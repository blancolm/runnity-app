import React from 'react';
import { Provider as PaperProvider, DarkTheme } from 'react-native-paper';
import BottomTabsNavigation from 'app/navigation/BottomTabsNavigation';
import CustomColors from 'app/shared/helpers/CustomColors';
import {
  NavigationContainer,
  Theme as NavigationTheme
} from '@react-navigation/native';
import { AuthContext, AuthProvider } from 'app/auth/services/useAuth';
import AuthNavigation from 'app/auth/navigation/AuthNavigation';
import {
  SnackbarProvider,
  MessageProvider,
  SpinnerProvider,
  VerifyActionProvider
} from '@vadiun/react-hooks';
import AppLoading from 'expo-app-loading';
import {
  useFonts,
  Poppins_500Medium,
  Poppins_700Bold
} from '@expo-google-fonts/poppins';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import {
  LoadingSpinner,
  Message,
  Provider as VadiunProvider,
  VerifyAction
} from '@vadiun/react-native-components';
import ErrorFallbackPage from 'app/shared/components/ErrorFallbackPage';
import ErrorBoundary from 'react-native-error-boundary';
import { StatusBar, LogBox } from 'react-native';
import {
  BackgroundLocationProvider,
  startBackgrounLocationTaskManager
} from 'app/shared/hooks/useBackgroundLocation';
import {
  LoggedRunnerContext,
  LoggedRunnerProvider
} from 'app/runner/services/useLoggedRunner';
import * as Sentry from 'sentry-expo';
import CompleteProfilePage from 'app/runner/profile/pages/CompleteProfilePage';
import CustomSnackbar from 'app/shared/components/CustomSnackbar';

LogBox.ignoreLogs(['Warning: ...']);
LogBox.ignoreAllLogs();

startBackgrounLocationTaskManager();

Sentry.init({
  dsn: 'https://04d3a28336d9463fab39b7e5b185795c@o961243.ingest.sentry.io/5909611',
  enableInExpoDevelopment: false,
  debug: true
});

declare global {
  namespace ReactNativePaper {
    interface ThemeColors {
      gray50: string;
      gray100: string;
      gray200: string;
      gray300: string;
      gray400: string;
      gray500: string;
      gray600: string;
      gray700: string;
      gray800: string;
      gray900: string;
    }
  }
}

const navigationTheme: NavigationTheme = {
  dark: true,
  colors: {
    primary: CustomColors.primary,
    background: CustomColors.dark900,
    card: CustomColors.dark600,
    text: CustomColors.primaryText,
    border: CustomColors.primaryText,
    notification: CustomColors.primaryText
  }
};

const theme: ReactNativePaper.Theme = {
  ...DarkTheme,
  roundness: 10,
  fonts: {
    regular: {
      fontFamily: 'poppins'
    },
    medium: {
      fontFamily: 'poppins'
    },
    light: {
      fontFamily: 'poppins'
    },
    thin: {
      fontFamily: 'poppins'
    }
  },
  colors: {
    ...DarkTheme.colors,
    ...{
      background: CustomColors.dark900,
      surface: CustomColors.dark600,
      text: CustomColors.primaryText
    },
    ...CustomColors,
    error: CustomColors.red500
  },
  dark: true
};

export default function App() {
  StatusBar.setBarStyle('light-content');
  StatusBar.setBackgroundColor(CustomColors.dark900);
  const [fontsLoaded] = useFonts({
    poppins: Poppins_500Medium,
    poppins_bold: Poppins_700Bold
  });

  function errorHandler() {}

  if (!fontsLoaded) {
    return <AppLoading />;
  }
  return (
    <VadiunProvider colors={CustomColors} googlePlacesApiKey="">
      <PaperProvider theme={theme}>
        <SafeAreaProvider>
          <NavigationContainer theme={navigationTheme}>
            <ErrorBoundary
              onError={errorHandler}
              FallbackComponent={ErrorFallbackPage}
            >
              <BackgroundLocationProvider>
                <SnackbarProvider component={CustomSnackbar}>
                  <MessageProvider component={Message}>
                    <SpinnerProvider component={LoadingSpinner}>
                      <VerifyActionProvider component={VerifyAction}>
                        <AuthProvider>
                          <LoggedRunnerProvider>
                            <AuthContext.Consumer>
                              {({ isAuthenticated }) =>
                                isAuthenticated ? (
                                  <LoggedRunnerContext.Consumer>
                                    {({ hasCompletedProfile, reload }) => {
                                      if (hasCompletedProfile === undefined) {
                                        return <AppLoading />;
                                      }
                                      if (hasCompletedProfile === false) {
                                        return (
                                          <CompleteProfilePage
                                            reloadRunner={reload}
                                          />
                                        );
                                      }
                                      return <BottomTabsNavigation />;
                                    }}
                                  </LoggedRunnerContext.Consumer>
                                ) : (
                                  <AuthNavigation />
                                )
                              }
                            </AuthContext.Consumer>
                          </LoggedRunnerProvider>
                        </AuthProvider>
                      </VerifyActionProvider>
                    </SpinnerProvider>
                  </MessageProvider>
                </SnackbarProvider>
              </BackgroundLocationProvider>
            </ErrorBoundary>
          </NavigationContainer>
        </SafeAreaProvider>
      </PaperProvider>
    </VadiunProvider>
  );
}
