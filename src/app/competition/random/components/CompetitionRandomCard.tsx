import { CompetitionModel } from 'app/competition/main/models/Competition';
import React from 'react';
import { Text } from 'react-native-paper';
import CompetitionCard from '../../main/components/CompetitionCard';

interface Props {
  competition: CompetitionModel;
  onParticipate: () => void;
}

const CompetitionRandomCard = ({ competition, onParticipate }: Props) => (
  <CompetitionCard image={{ uri: competition.participations[0].runner.photo }}>
    <CompetitionCard.Body>
      <CompetitionCard.Title>Competencia aleatoria</CompetitionCard.Title>
      <Text>{`${competition.category} K`}</Text>
      <Text>{`${competition.participations
        .slice(0, 2)
        .map((p) => p.runner.name)
        .join(', ')} ${
        competition.participations.length > 2
          ? ` y ${competition.participations.length - 2} más`
          : ''
      }`}</Text>
    </CompetitionCard.Body>
    <CompetitionCard.Button onPress={onParticipate}>
      Participar
    </CompetitionCard.Button>
  </CompetitionCard>
);

export default CompetitionRandomCard;
