import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useSuperMutation, useSuperQuery } from '@vadiun/react-hooks';
import { Button } from '@vadiun/react-native-components';
import RunnerListItem from 'app/runner/components/RunnerListItem';
import RunnerListItemLoading from 'app/runner/components/RunnerListItemLoading';
import RunnerModel from 'app/runner/models/Runner';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import CustomColors from 'app/shared/helpers/CustomColors';
import React, { useState } from 'react';
import { FlatList, StyleSheet, TouchableOpacity, View } from 'react-native';
import { Divider, Text } from 'react-native-paper';
import { CompetitionStackNavigationType } from '../../main/navigation/CompetitionNavigationType';
import useCompetitionRepository from '../../main/services/useCompetitionRepository';

const CompetitionRandomCreationPage = () => {
  const competitionRepository = useCompetitionRepository();
  const loggedRunner = useLoggedRunner();
  const [selectedCategory, setSelectedCategory] = useState<2 | 5 | 10>(2);

  const createMutation = useSuperMutation(
    competitionRepository.createRandomCompetition,
    {
      showSuccessMessage: false
    }
  );
  const navigation =
    useNavigation<
      StackNavigationProp<CompetitionStackNavigationType, 'randomCreation'>
    >();

  const createCompetition = async () => {
    await createMutation.mutate({
      runnerId: loggedRunner.runner!.id,
      category: selectedCategory
    });
    navigation.goBack();
    navigation.navigate('privateCompetitionCreated');
  };
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <Text style={{ fontSize: 16, textAlign: 'center', marginVertical: 10 }}>
          Elegí una categoría y buscaremos los mejores contrincantes para tu
          nivel
        </Text>
        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.category,
              selectedCategory === 2 ? styles.categorySelected : {}
            ]}
            onPress={() => setSelectedCategory(2)}
          >
            <Text style={{ fontFamily: 'poppins_bold' }}>2 K</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.category,
              selectedCategory === 5 ? styles.categorySelected : {}
            ]}
            onPress={() => setSelectedCategory(5)}
          >
            <Text style={{ fontFamily: 'poppins_bold' }}>5 K</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.category,
              selectedCategory === 10 ? styles.categorySelected : {}
            ]}
            onPress={() => setSelectedCategory(10)}
          >
            <Text style={{ fontFamily: 'poppins_bold' }}>10 K</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.footer}>
        <Button
          mode="contained"
          loading={createMutation.isLoading}
          disabled={createMutation.isLoading}
          onPress={() => createCompetition()}
        >
          Crear competencia
        </Button>
      </View>
    </View>
  );
};

export default CompetitionRandomCreationPage;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: CustomColors.dark600,
    padding: 20
  },
  category: {
    flex: 1,
    backgroundColor: CustomColors.dark300,
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 10
  },
  categorySelected: { borderColor: CustomColors.red500, borderWidth: 2 },
  body: {
    padding: 15
  }
});
