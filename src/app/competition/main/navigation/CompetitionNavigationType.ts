import { CompetitionRunningTeamModel } from 'app/competition/team/models/CompetitionRunningTeamModel';
import { RunningMeasure } from 'app/runningMeasures/models/RunningMeasure';
import { CompetitionModel } from '../models/Competition';

export type CompetitionStackNavigationType = {
  landing: undefined;
  privateCreation: undefined;
  privateDetail: { competition: CompetitionModel };
  teamDetail: { competition: CompetitionRunningTeamModel };
  teamCreation: undefined;
  randomCreation: undefined;
  privateCompetitionCreated: undefined;
  competitionPrivateExecution: {
    competition: CompetitionModel;
  };
  competitionRunningTeamExecution: {
    competition: CompetitionRunningTeamModel;
  };
  competitionPrivateFinished: {
    measures: RunningMeasure;
    competition: CompetitionModel;
  };
  competitionRunningTeamFinished: {
    measures: RunningMeasure;
    competition: CompetitionRunningTeamModel;
  };
  competitionPrivateResults: {
    competition: CompetitionModel;
  };
  competitionRunningTeamResults: {
    competition: CompetitionRunningTeamModel;
  };
};
