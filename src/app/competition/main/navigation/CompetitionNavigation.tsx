import CompetitionRunningteamExecutionPage from 'app/competition/team/pages/CompetitionRunningTeamExecutionPage';
import CompetitionRunningTeamFinishedPage from 'app/competition/team/pages/CompetitionRunningTeamFinishedPage';
import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import CompetitionPrivateResultsPage from 'app/competition/private/pages/CompetitionPrivateResultsPage';
import CompetitionPrivateFinishedPage from 'app/competition/private/pages/CompetitionPrivateFinishedPage';
import CompetitionRunningTeamResultsPage from 'app/competition/team/pages/CompetitionRunningTeamResultsPage';
import CompetitionPrivateExecutionPage from '../../private/pages/CompetitionPrivateExecutionPage';
import CompetitionCreatedPage from '../pages/CompetitionCreatedPage';
import CompetitionRunningTeamCreationPage from '../../team/pages/CompetitionRunningTeamCreationPage';
import CompetitionLandingPage from '../pages/CompetitionLandingPage';
import CompetitionPrivateCreationPage from '../../private/pages/CompetitionPrivateCreationPage';
import CompetitionRandomCreationPage from '../../random/pages/CompetitionRandomCreationPage';
import { CompetitionStackNavigationType } from './CompetitionNavigationType';
import CompetitionPrivateDetailPage from '../../private/pages/CompetitionPrivateDetailPage';
import CompetitionRunningTeamDetailPage from '../../team/pages/CompetitionRunningTeamDetailPage';

const Stack = createStackNavigator<CompetitionStackNavigationType>();

const CompetitionStackNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="landing"
      options={{
        headerShown: false
      }}
      component={CompetitionLandingPage}
    />
    <Stack.Screen
      name="privateCreation"
      options={{
        title: 'Competencia privada'
      }}
      component={CompetitionPrivateCreationPage}
    />
    <Stack.Screen
      name="teamCreation"
      options={{
        title: 'Competencia grupal'
      }}
      component={CompetitionRunningTeamCreationPage}
    />
    <Stack.Screen
      name="randomCreation"
      options={{
        title: 'Competencia aleatoria'
      }}
      component={CompetitionRandomCreationPage}
    />
    <Stack.Screen
      name="privateDetail"
      options={{
        title: 'Detalle competencia'
      }}
      component={CompetitionPrivateDetailPage}
    />
    <Stack.Screen
      name="teamDetail"
      options={{
        title: 'Detalle competencia'
      }}
      component={CompetitionRunningTeamDetailPage}
    />
    <Stack.Screen
      name="privateCompetitionCreated"
      options={{
        title: 'Competencia Creada'
      }}
      component={CompetitionCreatedPage}
    />
    <Stack.Screen
      name="competitionPrivateExecution"
      options={{
        headerShown: false
      }}
      component={CompetitionPrivateExecutionPage}
    />
    <Stack.Screen
      name="competitionPrivateFinished"
      options={{
        title: ''
      }}
      component={CompetitionPrivateFinishedPage}
    />
    <Stack.Screen
      name="competitionPrivateResults"
      options={{
        title: 'Resultados'
      }}
      component={CompetitionPrivateResultsPage}
    />
    <Stack.Screen
      name="competitionRunningTeamExecution"
      options={{
        headerShown: false
      }}
      component={CompetitionRunningteamExecutionPage}
    />
    <Stack.Screen
      name="competitionRunningTeamFinished"
      options={{
        title: ''
      }}
      component={CompetitionRunningTeamFinishedPage}
    />
    <Stack.Screen
      name="competitionRunningTeamResults"
      options={{
        title: 'Resultados'
      }}
      component={CompetitionRunningTeamResultsPage}
    />
  </Stack.Navigator>
);

export default CompetitionStackNavigation;
