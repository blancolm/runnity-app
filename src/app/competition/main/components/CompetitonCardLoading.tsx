import LoadingPlaceholder from 'app/shared/components/LoadingPlaceholder';
import React from 'react';

const CompetitonCardLoading = () => (
  <LoadingPlaceholder
    style={{
      height: 160,
      borderRadius: 20,
      margin: 10
    }}
  />
);

export default CompetitonCardLoading;
