import measureFormatter from 'app/runningMeasures/hooks/measureFormatter';
import { RunningMeasure } from 'app/runningMeasures/models/RunningMeasure';
import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { ImageSourcePropType, StyleSheet, View, Image } from 'react-native';
import { Text } from 'react-native-paper';

interface WithChildren {
  children: React.ReactNode;
}

const CompetitionResultListItem = (props: WithChildren) => (
  <View style={styles.container} {...props} />
);

export const CompetitionResultListItemBody = (props: WithChildren) => (
  <View style={styles.body} {...props} />
);

export const CompetitionResultListItemImage = ({
  source
}: {
  source: ImageSourcePropType;
}) => <Image source={source} style={styles.image} />;

export const CompetitionResultListItemTitle = (props: WithChildren) => (
  <Text style={styles.title} {...props} />
);

export const CompetitionResultListItemSubtitle = (props: WithChildren) => (
  <Text style={styles.subtitle} {...props} />
);

export const CompetitionResultListItemMeasures = ({
  measures
}: {
  measures: RunningMeasure;
}) => {
  const formattedMeasure = measureFormatter.format(measures);
  return (
    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
      <View style={{ alignItems: 'center' }}>
        <Text style={styles.subtitle}>{formattedMeasure.duration}</Text>
        <Text style={[styles.subtitle, { fontSize: 10 }]}>Duración</Text>
      </View>
      <View style={{ alignItems: 'center' }}>
        <Text style={styles.subtitle}>{formattedMeasure.mediumPace}</Text>
        <Text style={[styles.subtitle, { fontSize: 10 }]}>Ritmo medio</Text>
      </View>
      <View style={{ alignItems: 'center' }}>
        <Text style={styles.subtitle}>{formattedMeasure.calories}</Text>
        <Text style={[styles.subtitle, { fontSize: 10 }]}>Calorías</Text>
      </View>
    </View>
  );
};

export const CompetitionResultListItemProgressBar = ({
  totalDistance,
  distanceTraveled
}: {
  totalDistance: number;
  distanceTraveled: number;
}) => (
  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
    <View style={styles.progressBarContainer}>
      <View
        style={[
          styles.progressBar,
          { width: `${(distanceTraveled / totalDistance) * 100}%` }
        ]}
      />
    </View>
    <Text style={{ marginLeft: 10 }}>
      {(distanceTraveled / 1000).toFixed(2)} km.
    </Text>
  </View>
);

export const CompetitionResultListItemBadge = ({
  index
}: {
  index: number;
}) => {
  const colors = ['#FFD707', '#949494', '#CD7F32'];
  return (
    <View
      style={[
        styles.badge,
        {
          backgroundColor: colors[index] || CustomColors.dark300
        }
      ]}
    >
      <Text
        style={{
          fontFamily: 'poppins_bold',
          color: index === 0 ? CustomColors.dark900 : CustomColors.primaryText
        }}
      >
        {index + 1}
      </Text>
    </View>
  );
};

export default CompetitionResultListItem;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 20
  },
  image: {
    width: 70,
    height: 70,
    borderRadius: 20
  },
  body: {
    marginLeft: 20,
    flex: 1
  },
  badge: {
    position: 'absolute',
    top: 10,
    left: 10,
    height: 30,
    width: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100
  },
  title: {
    marginBottom: 10
  },
  subtitle: {
    color: CustomColors.secondaryText
  },
  progressBar: {
    height: 5,
    borderRadius: 5,
    backgroundColor: CustomColors.primary
  },
  progressBarContainer: {
    height: 5,
    borderRadius: 5,
    flex: 1,
    backgroundColor: CustomColors.dark300
  }
});
