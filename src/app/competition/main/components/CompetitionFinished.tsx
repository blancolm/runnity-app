import { MaterialCommunityIcons } from '@expo/vector-icons';
import RunningMeasures from 'app/runningMeasures/components/RunningMeasures';
import { RunningMeasure } from 'app/runningMeasures/models/RunningMeasure';
import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { StyleSheet } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Surface, Text } from 'react-native-paper';

const CompetitionFinished = ({
  measures,
  children
}: {
  measures: RunningMeasure;
  children: React.ReactNode;
}) => (
  <ScrollView contentContainerStyle={styles.container}>
    <MaterialCommunityIcons name="run" size={60} color="white" />
    <Text style={styles.title}>¡Competencia finalizada!</Text>
    <Text style={styles.subtitle}>Estos fueron tus resultados</Text>
    <Surface style={styles.body}>
      <RunningMeasures
        distance={measures.distance}
        duration={measures.seconds}
        mediumPace={measures.mediumPace}
        calories={measures.calories}
      />
    </Surface>
    {children}
  </ScrollView>
);

export default CompetitionFinished;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginTop: 30,
    flex: 1
  },
  title: {
    fontFamily: 'poppins_bold',
    fontSize: 24
  },
  subtitle: {
    fontSize: 20,
    marginTop: 10
  },
  body: {
    backgroundColor: CustomColors.dark600,
    padding: 20,
    borderRadius: 20,
    margin: 20
  }
});
