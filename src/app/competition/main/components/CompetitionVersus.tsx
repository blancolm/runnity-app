import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import {
  Dimensions,
  StyleSheet,
  View,
  Image,
  ImageSourcePropType
} from 'react-native';
import { Text } from 'react-native-paper';

interface Props {
  opponent1: React.ReactNode;
  opponent2: React.ReactNode;
}

const CompetitionVersus = ({ opponent1, opponent2 }: Props) => (
  <View style={styles.container}>
    <View style={styles.versus}>
      <Text style={{ fontFamily: 'poppins_bold', fontSize: 24 }}>VS</Text>
    </View>
    <View style={styles.bar} />
    <View style={styles.opponent1}>{opponent1}</View>
    <View style={styles.opponent2}>{opponent2}</View>
  </View>
);

CompetitionVersus.Subtitle = ({ children }: { children: React.ReactNode }) => (
  <Text style={styles.opponentDetails}>{children}</Text>
);

CompetitionVersus.Title = ({ children }: { children: React.ReactNode }) => (
  <Text style={styles.opponentName}>{children}</Text>
);

CompetitionVersus.Image = ({ source }: { source: ImageSourcePropType }) => (
  <View style={styles.opponentImage}>
    <View style={{ borderRadius: 15, overflow: 'hidden' }}>
      <Image
        source={source}
        resizeMode="cover"
        style={{ width: 130, height: 130 }}
      />
    </View>
  </View>
);

export default CompetitionVersus;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  opponent1: {
    position: 'absolute',
    top: 0,
    left: 0
  },
  opponentImage: {
    height: 130,
    width: 130,
    margin: 10
  },
  opponent2: {
    position: 'absolute',
    bottom: 0,
    right: 0
  },
  versus: {
    width: 80,
    height: 80,
    borderRadius: 15,
    backgroundColor: CustomColors.dark300,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 2
  },
  bar: {
    transform: [{ rotate: '-45deg' }],
    width: Dimensions.get('window').width,
    position: 'absolute',
    height: 4,
    backgroundColor: CustomColors.dark300
  },
  opponentDetails: {
    textAlign: 'center',
    color: CustomColors.secondaryText
  },
  opponentName: {
    fontFamily: 'poppins_bold',
    textAlign: 'center',
    fontSize: 18
  },
  badge: {
    position: 'absolute',
    top: -10,
    right: -10,
    padding: 10,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 2,
    borderRadius: 25
  },
  badgeCompleted: {
    backgroundColor: CustomColors.green500
  },
  badgePending: {
    backgroundColor: CustomColors.dark300
  }
});
