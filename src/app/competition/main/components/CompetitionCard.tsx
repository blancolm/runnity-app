import { Button } from '@vadiun/react-native-components';
import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { StyleSheet, View, Image, ImageSourcePropType } from 'react-native';
import { Text } from 'react-native-paper';

interface Props {
  image: ImageSourcePropType;
  children: React.ReactNode;
}

const CompetitionCard = ({ image, children }: Props) => (
  <View style={styles.card}>
    <Image source={image} style={styles.cardImage} resizeMode="cover" />
    <View style={styles.cardBody}>{children}</View>
  </View>
);

CompetitionCard.Title = ({ children }: { children: React.ReactNode }) => (
  <Text style={{ fontFamily: 'poppins_bold', fontSize: 16 }}>{children}</Text>
);

CompetitionCard.Body = ({ children }: { children: React.ReactNode }) => (
  <View style={{ margin: 15 }}>{children}</View>
);

CompetitionCard.Button = ({
  children,
  onPress
}: {
  children: React.ReactNode;
  onPress: () => void;
}) => (
  <Button
    style={{
      marginTop: 'auto',
      borderTopColor: CustomColors.dark300,
      borderWidth: 1
    }}
    onPress={onPress}
  >
    {children}
  </Button>
);

export default CompetitionCard;

const styles = StyleSheet.create({
  card: {
    backgroundColor: CustomColors.dark600,
    height: 160,
    overflow: 'hidden',
    borderRadius: 20,
    margin: 10,
    flexDirection: 'row'
  },
  cardImage: {
    width: 100
  },
  cardBody: {
    flex: 1
  }
});
