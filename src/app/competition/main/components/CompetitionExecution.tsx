import React, { useEffect } from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import { Marker } from 'react-native-maps';
import CustomBottomSheet from 'app/shared/components/CustomBottomSheet';
import useRunningMeasures, {
  RunningStatus
} from 'app/runningMeasures/hooks/useRunningMeasures';
import { FAB } from 'react-native-paper';
import { Button } from '@vadiun/react-native-components';
import { TouchableWithoutFeedback } from '@gorhom/bottom-sheet';
import useBackgroundLocation from 'app/shared/hooks/useBackgroundLocation';
import RunningMeasures from 'app/runningMeasures/components/RunningMeasures';

import CustomColors from 'app/shared/helpers/CustomColors';
import { SafeAreaView } from 'react-native-safe-area-context';

import Map from 'app/shared/components/Map/Map';

import { RunningMeasure } from 'app/runningMeasures/models/RunningMeasure';

const MarketIcon = require('assets/running.png');

interface Props {
  children: React.ReactNode;
  onGoBack: () => void;
  totalDistance: number;
  stopMutation: {
    isLoading: boolean;
    mutate: (measure: RunningMeasure) => any;
  };
}

const initialCameraPosition = {
  latitude: -34.60802433133401,
  longitude: -58.37039256425254
};

const CompetitionExecution = ({
  children,
  onGoBack,
  stopMutation,
  totalDistance
}: Props) => {
  const location = useBackgroundLocation();
  const runningMeasures = useRunningMeasures();
  const cameraPosition = location.location
    ? {
        latitude: location.location.lat,
        longitude: location.location.lng
      }
    : initialCameraPosition;
  const snapPoints = [
    3,
    runningMeasures.status === RunningStatus.RUNNING ? 320 : 250
  ];
  useEffect(() => {
    if (runningMeasures.distance >= totalDistance) {
      runningMeasures.stop();
      stopMutation.mutate(runningMeasures);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [runningMeasures.distance]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <FAB icon="chevron-left" style={styles.goBack} small onPress={onGoBack} />
      <Map
        style={styles.map}
        camera={{
          center: {
            latitude: cameraPosition.latitude - 0.0007,
            longitude: cameraPosition.longitude
          },
          zoom: 18,
          heading: 0,
          pitch: 0,
          altitude: 0
        }}
      >
        {location.location && (
          <Marker
            icon={MarketIcon}
            coordinate={{
              latitude: location.location.lat,
              longitude: location.location.lng
            }}
          />
        )}
      </Map>
      <CustomBottomSheet index={1} snapPoints={snapPoints}>
        {runningMeasures.status === RunningStatus.IDLE &&
        !stopMutation.isLoading ? (
          <View
            style={{
              padding: 30,
              flex: 1
            }}
          >
            {children}
            <View style={{ marginTop: 'auto' }}>
              <TouchableWithoutFeedback onPress={runningMeasures.start}>
                <Button onPress={() => {}} mode="contained">
                  Comenzar
                </Button>
              </TouchableWithoutFeedback>
            </View>
          </View>
        ) : (
          <View style={{ flex: 1, paddingVertical: 10, paddingHorizontal: 30 }}>
            <RunningMeasures
              duration={runningMeasures.seconds}
              distance={runningMeasures.distance}
              mediumPace={runningMeasures.mediumPace}
              calories={runningMeasures.calories}
            />
            <TouchableWithoutFeedback
              onPress={() => {
                runningMeasures.stop();
                stopMutation.mutate(runningMeasures);
              }}
            >
              <Button
                onPress={() => {}}
                mode="contained"
                loading={stopMutation.isLoading}
                disabled={stopMutation.isLoading}
              >
                Parar
              </Button>
            </TouchableWithoutFeedback>
          </View>
        )}
      </CustomBottomSheet>
    </SafeAreaView>
  );
};

export default CompetitionExecution;

const styles = StyleSheet.create({
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  },
  goBack: {
    position: 'absolute',
    margin: 20,
    marginTop: 40,
    top: 0,
    left: 0,
    backgroundColor: CustomColors.dark600,
    zIndex: 2
  }
});
