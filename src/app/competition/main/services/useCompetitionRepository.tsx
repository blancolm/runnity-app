import {
  competitionRunningTeamBuilder,
  CompetitionRunningTeamModel
} from 'app/competition/team/models/CompetitionRunningTeamModel';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import { httpClient } from 'app/shared/services/http/httpClient';
import { competitionBuilder, CompetitionModel } from '../models/Competition';

const useCompetitionRepository = () => {
  const loggedRunner = useLoggedRunner();
  const createPrivateCompetition = ({
    runnerIds,
    category
  }: {
    runnerIds: number[];
    category: 2 | 5 | 10;
  }) =>
    httpClient.post(`private-races`, {
      competitor_ids: [...runnerIds, loggedRunner.runner!.id],
      category: category === 2 ? 1 : category === 5 ? 2 : 3
    });

  const createRandomCompetition = ({
    runnerId,
    category
  }: {
    runnerId: number;
    category: 2 | 5 | 10;
  }) =>
    httpClient.post(`random-races`, {
      competitor_id: runnerId,
      category: category === 2 ? 1 : category === 5 ? 2 : 3
    });

  const createRunningTeamCompetition = ({
    runningTeamId,
    category
  }: {
    runningTeamId: number;
    category: 2 | 5 | 10;
  }) =>
    httpClient.post(`group-races`, {
      group_ids: [runningTeamId, loggedRunner.runner?.group_id],
      category: category === 2 ? 1 : category === 5 ? 2 : 3
    });

  const getPendingCompetitions = async (): Promise<
    (CompetitionModel | CompetitionRunningTeamModel)[]
  > => {
    const competitions = await httpClient.get(`runners/me/pending-races`);
    return competitions.map((comp: any) => {
      if (comp.type === 3) {
        return competitionRunningTeamBuilder.fromBackEnd(comp);
      }
      return competitionBuilder.fromBackEnd(comp);
    });
  };

  const getFinishedCompetitions = async (): Promise<
    (CompetitionModel | CompetitionRunningTeamModel)[]
  > => {
    const competitions = await httpClient.get(`runners/me/finished-races`);
    return competitions.map((comp: any) => {
      if (comp.type === 3) {
        return competitionRunningTeamBuilder.fromBackEnd(comp);
      }
      return competitionBuilder.fromBackEnd(comp);
    });
  };

  const getCompetition = async (
    id: number
  ): Promise<CompetitionModel | CompetitionRunningTeamModel> => {
    const competition = await httpClient.get(`races/${id}`);
    if (competition.type === 3) {
      return competitionRunningTeamBuilder.fromBackEnd(competition);
    }
    return competitionBuilder.fromBackEnd(competition);
  };

  const finishCompetition = (params: {
    seconds: number;
    distance: number;
    competitionId: number;
  }) =>
    httpClient.put(`races/${params.competitionId}/complete`, {
      seconds: params.seconds,
      distance: params.distance
    });

  return {
    createPrivateCompetition,
    createRunningTeamCompetition,
    createRandomCompetition,
    getPendingCompetitions,
    finishCompetition,
    getCompetition,
    getFinishedCompetitions
  };
};

export default useCompetitionRepository;
