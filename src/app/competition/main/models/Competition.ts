import RunnerModel, { runnerBuilder } from 'app/runner/models/Runner';
import moment, { Moment } from 'moment';

export type CompetitionCategory = 2 | 5 | 10;

export enum CompetitionParticipationState {
  FINISHED = 'Finalizado',
  PENDING = 'Pendiente'
}

export type CompetitionParticipationPending = {
  runner: RunnerModel;
  state: CompetitionParticipationState.PENDING;
};

export type CompetitionParticipationFinished = {
  runner: RunnerModel;
  state: CompetitionParticipationState.FINISHED;
  distance: number;
  seconds: number;
  datetime: Moment;
};

export type CompetitionParticipation =
  | CompetitionParticipationPending
  | CompetitionParticipationFinished;

export interface CompetitionModel {
  participations: CompetitionParticipation[];
  category: CompetitionCategory;
  endDateTime: Moment;
  initDateTime: Moment;
  type: CompetitionType;
  id: number;
}

export enum CompetitionType {
  PRIVATE = 'Privada',
  RANDOM = 'Aleatoria',
  GROUP = 'Grupo'
}

export const competitionBuilder = {
  fromBackEnd: (competition: any): CompetitionModel => {
    return {
      ...competition,
      endDateTime: moment(competition.end_datetime),
      initDateTime: moment(competition.init_datetime),
      type:
        competition.type === 1
          ? CompetitionType.PRIVATE
          : competition.type === 2
          ? CompetitionType.RANDOM
          : CompetitionType.GROUP,
      category:
        competition.category === 1 ? 2 : competition.category === 2 ? 5 : 10,
      participations: competition.competitors.map((p: any) => ({
        ...p,
        state:
          p.state === null
            ? CompetitionParticipationState.PENDING
            : CompetitionParticipationState.FINISHED,
        runner: runnerBuilder.fromBackEnd(p.runner)
      }))
    };
  }
};
