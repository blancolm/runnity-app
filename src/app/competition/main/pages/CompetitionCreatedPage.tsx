import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { Button } from '@vadiun/react-native-components';
import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-paper';
import { CompetitionStackNavigationType } from '../navigation/CompetitionNavigationType';

const CompetitionCreatedPage = () => {
  const navigation =
    useNavigation<
      StackNavigationProp<
        CompetitionStackNavigationType,
        'privateCompetitionCreated'
      >
    >();
  return (
    <View style={styles.container}>
      <MaterialCommunityIcons name="run" size={60} color="white" />
      <Text style={styles.title}>¡Excelente!</Text>
      <Text style={styles.subtitle}>Competencia creada</Text>
      <Text style={styles.body}>
        Esperamos que te diviertas compitiendo con tus amigos
      </Text>
      <Button
        mode="contained"
        style={{ marginTop: 'auto', width: '100%' }}
        onPress={() => navigation.goBack()}
      >
        Entendido
      </Button>
    </View>
  );
};

export default CompetitionCreatedPage;

const styles = StyleSheet.create({
  container: {
    marginTop: 80,
    alignItems: 'center',
    padding: 30,
    flex: 1
  },
  title: {
    fontFamily: 'poppins_bold',
    fontSize: 28
  },
  subtitle: {
    fontSize: 20,
    marginTop: 10
  },
  body: {
    marginTop: 20,
    color: CustomColors.secondaryText
  }
});
