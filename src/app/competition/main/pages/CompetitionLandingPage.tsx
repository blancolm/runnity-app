import { TouchableOpacity } from '@gorhom/bottom-sheet';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useSnackbar, useSuperQuery } from '@vadiun/react-hooks';
import CompetitionTeamCard from 'app/competition/team/components/CompetitionTeamCard';
import { isRunningTeamCompetition } from 'app/competition/team/models/CompetitionRunningTeamModel';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import CustomColors from 'app/shared/helpers/CustomColors';
import React, { useCallback } from 'react';
import { StyleSheet, ScrollView, View, RefreshControl } from 'react-native';
import { Text } from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';
import Swiper from 'react-native-swiper';
import CompetitionPrivateCard from '../../private/components/CompetitionPrivateCard';
import CompetitonCardLoading from '../components/CompetitonCardLoading';
import { CompetitionStackNavigationType } from '../navigation/CompetitionNavigationType';
import useCompetitionRepository from '../services/useCompetitionRepository';

const CompetitionLandingPage = () => {
  const competitionRepo = useCompetitionRepository();
  const showSnackbar = useSnackbar();
  const loggedRunner = useLoggedRunner();
  const navigation =
    useNavigation<StackNavigationProp<CompetitionStackNavigationType>>();

  const pendingCompetitionsQuery = useSuperQuery(
    competitionRepo.getPendingCompetitions
  );

  useFocusEffect(
    useCallback(() => {
      pendingCompetitionsQuery.reload();
      // teamCompetitionsQuery.reload();
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
  );

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView
        style={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={pendingCompetitionsQuery.isLoading}
            onRefresh={pendingCompetitionsQuery.reload}
          />
        }
      >
        <Text style={styles.title}>Nueva carrera</Text>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.competitionButton}
          onPress={() => navigation.push('randomCreation')}
        >
          <Text style={styles.title}>Aleatorio</Text>
          <Text style={{ fontSize: 12 }}>
            Encontraremos a tu mejor contrincante
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          style={styles.competitionButton}
          onPress={() => navigation.push('privateCreation')}
        >
          <Text style={styles.title}>Privado</Text>
          <Text style={{ fontSize: 12 }}>
            Competí sólo con los corredores que elijas
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          activeOpacity={0.7}
          style={[
            styles.competitionButton,
            {
              backgroundColor:
                loggedRunner.runner?.group_id === null
                  ? CustomColors.primary400
                  : CustomColors.primary500
            }
          ]}
          onPress={() => {
            if (loggedRunner.runner?.group_id === null) {
              showSnackbar({
                title: 'Aún no sos parte de ningun grupo',
                type: 'warning',
                message: 'Aún no sos parte de ningun grupo'
              });
              return;
            }
            navigation.push('teamCreation');
          }}
        >
          <Text style={styles.title}>Batalla de grupos</Text>
          <Text style={{ fontSize: 12 }}>
            Demostrá que sos parte del mejor grupo
          </Text>
        </TouchableOpacity>
        <Text style={[styles.title, { marginTop: 10 }]}>Pendientes</Text>
        {!pendingCompetitionsQuery.data ? (
          <Swiper style={styles.wrapper}>
            <CompetitonCardLoading />
            <CompetitonCardLoading />
            <CompetitonCardLoading />
          </Swiper>
        ) : pendingCompetitionsQuery.data.length === 0 ? (
          <Text
            style={{ color: CustomColors.secondaryText, marginVertical: 20 }}
          >
            No tienen niguna carrera pendiente
          </Text>
        ) : (
          <>
            <></>
            <Swiper style={styles.wrapper}>
              {pendingCompetitionsQuery.data.map((competition) => (
                <View style={styles.slide} key={competition.id}>
                  {isRunningTeamCompetition(competition) ? (
                    <CompetitionTeamCard
                      competition={competition}
                      onParticipate={() =>
                        navigation.navigate('teamDetail', { competition })
                      }
                    />
                  ) : (
                    <CompetitionPrivateCard
                      competition={competition}
                      onParticipate={() =>
                        navigation.navigate('privateDetail', { competition })
                      }
                    />
                  )}
                </View>
              ))}
              {/* {teamCompetitionsQuery.data.map((competition) => (
                <View style={styles.slide} key={competition.id}>
                  <CompetitionTeamCard
                    competition={competition}
                    onParticipate={() =>
                      navigation.navigate('teamDetail', { competition })
                    }
                  />
                </View>
              ))} */}
            </Swiper>
          </>
        )}

        <Text style={styles.title}>Eventos</Text>
        <Swiper style={styles.wrapper}>
          <View style={styles.slide}></View>
        </Swiper>
      </ScrollView>
    </SafeAreaView>
  );
};

export default CompetitionLandingPage;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    flex: 1
  },
  title: {
    fontFamily: 'poppins_bold',
    fontSize: 18
  },
  competitionButton: {
    backgroundColor: CustomColors.primary500,
    alignItems: 'center',
    marginVertical: 10,
    padding: 10,
    borderRadius: 20
  },
  wrapper: { height: 230 },
  slide: {}
});
