import {
  NavigationProp,
  RouteProp,
  useNavigation,
  useRoute
} from '@react-navigation/native';
import { Button } from '@vadiun/react-native-components';
import CompetitionFinished from 'app/competition/main/components/CompetitionFinished';
import { CompetitionStackNavigationType } from 'app/competition/main/navigation/CompetitionNavigationType';
import React from 'react';

const CompetitionPrivateFinishedPage = () => {
  const route =
    useRoute<
      RouteProp<CompetitionStackNavigationType, 'competitionPrivateFinished'>
    >();
  const navigation =
    useNavigation<
      NavigationProp<
        CompetitionStackNavigationType,
        'competitionPrivateFinished'
      >
    >();

  const { measures, competition } = route.params;
  return (
    <CompetitionFinished measures={measures}>
      <Button
        onPress={() =>
          navigation.navigate('competitionPrivateResults', { competition })
        }
      >
        Ver tabla de resultados
      </Button>
    </CompetitionFinished>
  );
};

export default CompetitionPrivateFinishedPage;
