import { RouteProp, useRoute } from '@react-navigation/native';
import { useSuperQuery } from '@vadiun/react-hooks';
import CompetitionResultListItem, {
  CompetitionResultListItemBadge,
  CompetitionResultListItemBody,
  CompetitionResultListItemImage,
  CompetitionResultListItemMeasures,
  CompetitionResultListItemProgressBar,
  CompetitionResultListItemTitle
} from 'app/competition/main/components/CompetitionResultListItem';
import {
  CompetitionParticipationFinished,
  CompetitionParticipationState
} from 'app/competition/main/models/Competition';
import { CompetitionStackNavigationType } from 'app/competition/main/navigation/CompetitionNavigationType';
import useCompetitionRepository from 'app/competition/main/services/useCompetitionRepository';
import caloriesCalculator from 'app/runningMeasures/hooks/caloriesCalculator';
import mediumPaceCalculator from 'app/runningMeasures/hooks/mediumPaceCalculator';
import { biggerDistanceFirst } from 'app/shared/helpers/biggerDistanceFirst';
import React from 'react';
import { ListRenderItemInfo, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { Divider } from 'react-native-paper';

const CompetitionPrivateResultsPage = () => {
  const competitionRepository = useCompetitionRepository();
  const route =
    useRoute<
      RouteProp<CompetitionStackNavigationType, 'competitionPrivateResults'>
    >();

  const { competition } = route.params;
  const competitionQuery = useSuperQuery(() =>
    competitionRepository.getCompetition(competition.id)
  );

  if (competitionQuery.data === undefined) {
    return null;
  }
  const participationsCompleted = competitionQuery.data.participations
    .filter((c) => c.state === CompetitionParticipationState.FINISHED)
    .sort(
      (
        a: CompetitionParticipationFinished,
        b: CompetitionParticipationFinished
      ) => biggerDistanceFirst(a, b)
    );

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        data={participationsCompleted}
        keyExtractor={(item) => item.id}
        renderItem={({
          item,
          index
        }: ListRenderItemInfo<CompetitionParticipationFinished>) => {
          const mediumPace = mediumPaceCalculator.calculate(
            item.distance,
            item.seconds
          );
          const calories = caloriesCalculator.calculate({
            minutesPerKm: mediumPace,
            time: item.seconds,
            weight: 70
          });
          return (
            <CompetitionResultListItem>
              <CompetitionResultListItemImage
                source={{ uri: item.runner.photo }}
              />
              <CompetitionResultListItemBadge index={index} />
              <CompetitionResultListItemBody>
                <CompetitionResultListItemTitle>
                  {item.runner.name}
                </CompetitionResultListItemTitle>
                <CompetitionResultListItemProgressBar
                  totalDistance={competition.category * 1000}
                  distanceTraveled={item.distance}
                />
                <CompetitionResultListItemMeasures
                  measures={{
                    seconds: item.seconds,
                    calories,
                    mediumPace,
                    distance: item.distance
                  }}
                />
              </CompetitionResultListItemBody>
            </CompetitionResultListItem>
          );
        }}
        ItemSeparatorComponent={Divider}
      />
    </View>
  );
};

export default CompetitionPrivateResultsPage;
