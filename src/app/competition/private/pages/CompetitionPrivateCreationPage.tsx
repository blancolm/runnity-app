import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useSuperMutation, useSuperQuery } from '@vadiun/react-hooks';
import { Button } from '@vadiun/react-native-components';
import RunnerListItem from 'app/runner/components/RunnerListItem';
import RunnerListItemLoading from 'app/runner/components/RunnerListItemLoading';
import RunnerModel from 'app/runner/models/Runner';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import useRunnerRepository from 'app/runner/services/useRunnerRepository';
import CustomColors from 'app/shared/helpers/CustomColors';
import React, { useState } from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import { Divider, Searchbar, Switch, Text } from 'react-native-paper';
import { CompetitionStackNavigationType } from '../../main/navigation/CompetitionNavigationType';
import useCompetitionRepository from '../../main/services/useCompetitionRepository';

const CompetitionPrivateCreationPage = () => {
  const runnerRepository = useRunnerRepository();
  const competitionRepository = useCompetitionRepository();
  const loggedRunner = useLoggedRunner();
  const runnersQuery = useSuperQuery(runnerRepository.getAll);
  const createMutation = useSuperMutation(
    competitionRepository.createPrivateCompetition,
    {
      showSuccessMessage: false
    }
  );
  const navigation =
    useNavigation<
      StackNavigationProp<CompetitionStackNavigationType, 'privateCreation'>
    >();

  const [searchBarText, setSearchBarText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState<2 | 5 | 10>(2);
  const [competitionRunners, setCompetitionRunners] = useState<RunnerModel[]>(
    []
  );

  const createCompetition = async () => {
    await createMutation.mutate({
      runnerIds: competitionRunners.map((x) => x.id),
      category: selectedCategory
    });
    navigation.goBack();
    navigation.navigate('privateCompetitionCreated');
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={{ marginTop: 15, marginRight: 15, marginLeft: 15 }}>
        <Searchbar
          placeholder="Buscar"
          value={searchBarText}
          onChangeText={(text) => {
            setSearchBarText(text);
          }}
        />
      </View>
      <ScrollView
        style={styles.container}
        contentContainerStyle={{ flexGrow: 1 }}
      >
        <View style={{ padding: 15 }}>
          {runnersQuery.data === undefined ? (
            <>
              <RunnerListItemLoading />
              <Divider />
              <RunnerListItemLoading />
              <Divider />
              <RunnerListItemLoading />
              <Divider />
            </>
          ) : (
            <FlatList
              data={runnersQuery.data
                .filter((runner) => runner.id !== loggedRunner.runner!.id)
                .filter((runner) =>
                  runner.name
                    .toLowerCase()
                    .includes(searchBarText.toLowerCase())
                )}
              keyExtractor={(item: RunnerModel) => String(item.id)}
              renderItem={({ item }) => (
                <RunnerListItem
                  runner={item}
                  right={
                    <Switch
                      value={competitionRunners.some((x) => x.id === item.id)}
                      onValueChange={() => {
                        if (competitionRunners.some((x) => x.id === item.id)) {
                          setCompetitionRunners((xs) =>
                            xs.filter((x) => x.id !== item.id)
                          );
                        } else {
                          setCompetitionRunners((xs) => [...xs, item]);
                        }
                      }}
                    />
                  }
                />
              )}
              ItemSeparatorComponent={Divider}
            />
          )}
        </View>
      </ScrollView>
      <View style={styles.footer}>
        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.category,
              selectedCategory === 2 ? styles.categorySelected : {}
            ]}
            onPress={() => setSelectedCategory(2)}
          >
            <Text style={{ fontFamily: 'poppins_bold' }}>2 K</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.category,
              selectedCategory === 5 ? styles.categorySelected : {}
            ]}
            onPress={() => setSelectedCategory(5)}
          >
            <Text style={{ fontFamily: 'poppins_bold' }}>5 K</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.category,
              selectedCategory === 10 ? styles.categorySelected : {}
            ]}
            onPress={() => setSelectedCategory(10)}
          >
            <Text style={{ fontFamily: 'poppins_bold' }}>10 K</Text>
          </TouchableOpacity>
        </View>
        <Button
          mode="contained"
          loading={createMutation.isLoading}
          disabled={createMutation.isLoading}
          onPress={createCompetition}
        >
          Crear competencia
        </Button>
      </View>
    </View>
  );
};

export default CompetitionPrivateCreationPage;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: CustomColors.dark600,
    padding: 20
  },
  category: {
    flex: 1,
    backgroundColor: CustomColors.dark300,
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 10
  },
  categorySelected: { borderColor: CustomColors.red500, borderWidth: 2 }
});
