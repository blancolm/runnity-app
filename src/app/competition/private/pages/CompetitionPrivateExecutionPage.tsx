import React from 'react';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { StyleSheet, View } from 'react-native';
import { useSuperMutation } from '@vadiun/react-hooks';
import CompetitionExecution from 'app/competition/main/components/CompetitionExecution';
import { RunningMeasure } from 'app/runningMeasures/models/RunningMeasure';
import { CompetitionType } from 'app/competition/main/models/Competition';
import { Text } from 'react-native-paper';
import { CompetitionStackNavigationType } from '../../main/navigation/CompetitionNavigationType';
import useCompetitionRepository from '../../main/services/useCompetitionRepository';

const CompetitionPrivateExecutionPage = () => {
  const competitionRepo = useCompetitionRepository();
  const navigation =
    useNavigation<
      StackNavigationProp<
        CompetitionStackNavigationType,
        'competitionPrivateExecution'
      >
    >();
  const route =
    useRoute<
      RouteProp<CompetitionStackNavigationType, 'competitionPrivateExecution'>
    >();
  const { competition } = route.params;

  const stopMutation = useSuperMutation(
    async (runningMeasures: RunningMeasure) => {
      await competitionRepo.finishCompetition({
        ...runningMeasures,
        competitionId: competition.id
      });
      navigation.popToTop();
      navigation.navigate('competitionPrivateFinished', {
        measures: runningMeasures,
        competition
      });
    },
    {
      showSuccessMessage: false
    }
  );

  return (
    <CompetitionExecution
      onGoBack={navigation.goBack}
      totalDistance={competition.category * 1000}
      stopMutation={stopMutation}
    >
      <View>
        <Text style={styles.title}>
          {competition.type === CompetitionType.RANDOM
            ? 'Competencia aleatoria'
            : 'Competencia privada'}
        </Text>
        <Text style={styles.competitor} numberOfLines={3}>
          {competition.participations.map((p) => p.runner.name).join(' VS ')}
        </Text>
      </View>
    </CompetitionExecution>
  );
};

export default CompetitionPrivateExecutionPage;

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'poppins_bold',
    marginBottom: 10
  },
  competitor: {
    textAlign: 'center',
    fontSize: 14
  }
});
