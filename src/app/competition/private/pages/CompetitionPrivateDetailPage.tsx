import {
  NavigationProp,
  RouteProp,
  useNavigation,
  useRoute
} from '@react-navigation/native';
import { Button } from '@vadiun/react-native-components';
import CompetitionPrivateWithMultipleOpponents from 'app/competition/private/components/CompetitionPrivateWithMultipleOpponents';
import CompetitionPrivateWithTwoOpponent from 'app/competition/private/components/CompetitionPrivateWithTwoOpponents';
import { CompetitionStackNavigationType } from 'app/competition/main/navigation/CompetitionNavigationType';
import React from 'react';
import { Text } from 'react-native-paper';
import { View } from 'react-native';
import CustomColors from 'app/shared/helpers/CustomColors';

const CompetitionPrivateDetailPage = () => {
  const route =
    useRoute<RouteProp<CompetitionStackNavigationType, 'privateDetail'>>();
  const navigation =
    useNavigation<
      NavigationProp<CompetitionStackNavigationType, 'privateDetail'>
    >();
  const { competition } = route.params;

  const CompetitionDetail =
    competition.participations.length === 2
      ? CompetitionPrivateWithTwoOpponent
      : CompetitionPrivateWithMultipleOpponents;
  return (
    <View style={{ flex: 1 }}>
      <CompetitionDetail participations={competition.participations} />
      <Text style={{ textAlign: 'center', color: CustomColors.secondaryText }}>
        Finalización {competition.endDateTime.format('DD/MM/YYYY HH:mm')} hs.
      </Text>
      <Button
        mode="contained"
        onPress={() =>
          navigation.navigate('competitionPrivateExecution', { competition })
        }
      >
        Competir
      </Button>
    </View>
  );
};

export default CompetitionPrivateDetailPage;
