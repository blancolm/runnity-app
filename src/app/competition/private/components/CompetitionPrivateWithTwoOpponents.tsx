import { Ionicons } from '@expo/vector-icons';
import CustomColors from 'app/shared/helpers/CustomColors';
import moment from 'moment';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import {
  CompetitionParticipation,
  CompetitionParticipationState
} from '../../main/models/Competition';
import CompetitionVersus from '../../main/components/CompetitionVersus';

interface Props {
  participations: CompetitionParticipation[];
}

const CompetitionStateBadge = ({
  state
}: {
  state: CompetitionParticipationState;
}) => {
  if (state === CompetitionParticipationState.PENDING) {
    return (
      <View style={[styles.badge, styles.badgePending]}>
        <Ionicons name="hourglass" color={CustomColors.primaryText} size={18} />
      </View>
    );
  }
  return (
    <View style={[styles.badge, styles.badgeCompleted]}>
      <Ionicons name="checkmark" color={CustomColors.primaryText} size={20} />
    </View>
  );
};

const CompetitionPrivateWithTwoOpponent = ({ participations }: Props) => (
  <CompetitionVersus
    opponent1={
      <>
        <CompetitionVersus.Subtitle>
          ORO 1 |{' '}
          {`${moment().diff(participations[0].runner.birthdate, 'years')} años`}
        </CompetitionVersus.Subtitle>
        <CompetitionVersus.Title>
          {participations[0].runner.name}
        </CompetitionVersus.Title>
        <View>
          <CompetitionVersus.Image
            source={{ uri: participations[0].runner.photo }}
          />
          <CompetitionStateBadge state={participations[0].state} />
        </View>
      </>
    }
    opponent2={
      <>
        <View>
          <CompetitionVersus.Image
            source={{ uri: participations[1].runner.photo }}
          />
          <CompetitionStateBadge state={participations[1].state} />
        </View>
        <CompetitionVersus.Title>
          {participations[1].runner.name}
        </CompetitionVersus.Title>
        <CompetitionVersus.Subtitle>
          ORO 1 |{' '}
          {`${moment().diff(participations[1].runner.birthdate, 'years')} años`}
        </CompetitionVersus.Subtitle>
      </>
    }
  />
);

export default CompetitionPrivateWithTwoOpponent;

const styles = StyleSheet.create({
  badge: {
    position: 'absolute',
    top: -0,
    right: -0,
    padding: 10,
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 2,
    borderRadius: 25
  },
  badgeCompleted: {
    backgroundColor: CustomColors.green500
  },
  badgePending: {
    backgroundColor: CustomColors.dark300
  }
});
