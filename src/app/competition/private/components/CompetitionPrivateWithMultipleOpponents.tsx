import RunnerListItem from 'app/runner/components/RunnerListItem';
import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { FlatList } from 'react-native-gesture-handler';
import { Badge, Divider } from 'react-native-paper';
import {
  CompetitionParticipation,
  CompetitionParticipationState
} from '../../main/models/Competition';

interface Props {
  participations: CompetitionParticipation[];
}

const CompetitionPrivateWithMultipleOpponents = ({ participations }: Props) => (
  <FlatList
    data={participations}
    keyExtractor={(item) => item.id}
    renderItem={({ item }) => (
      <RunnerListItem
        runner={item.runner}
        right={
          item.state === CompetitionParticipationState.PENDING ? (
            <Badge style={{ backgroundColor: CustomColors.dark300 }} size={22}>
              Pendiente
            </Badge>
          ) : (
            <Badge
              selectionColor={CustomColors.primary}
              style={{ backgroundColor: CustomColors.primary300 }}
              size={22}
            >
              Completo
            </Badge>
          )
        }
      />
    )}
    ItemSeparatorComponent={Divider}
  />
);

export default CompetitionPrivateWithMultipleOpponents;
