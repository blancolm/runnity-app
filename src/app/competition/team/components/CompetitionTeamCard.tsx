import React from 'react';
import { Text } from 'react-native-paper';
import CompetitionCard from '../../main/components/CompetitionCard';
import { CompetitionRunningTeamModel } from '../models/CompetitionRunningTeamModel';

interface Props {
  competition: CompetitionRunningTeamModel;
  onParticipate: () => void;
}

const CompetitionTeamCard = ({ competition, onParticipate }: Props) => (
  <CompetitionCard
    image={{ uri: competition.participationsPerTeam[0].runningTeam.photo }}
  >
    <CompetitionCard.Body>
      <CompetitionCard.Title>Batalla de grupos</CompetitionCard.Title>
      <Text>{`${competition.category} K`}</Text>
      <Text>{`${competition.participationsPerTeam[0].runningTeam.name} VS ${competition.participationsPerTeam[1].runningTeam.name}`}</Text>
    </CompetitionCard.Body>
    <CompetitionCard.Button onPress={onParticipate}>
      Participar
    </CompetitionCard.Button>
  </CompetitionCard>
);

export default CompetitionTeamCard;
