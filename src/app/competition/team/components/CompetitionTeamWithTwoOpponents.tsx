import React from 'react';
import RunningTeamModel from 'app/runningTeam/models/RunningTeam';
import CompetitionVersus from '../../main/components/CompetitionVersus';

interface Props {
  teams: RunningTeamModel[];
}

const CompetitionTeamWithTwoOpponent = ({ teams }: Props) => (
  <CompetitionVersus
    opponent1={
      <>
        <CompetitionVersus.Subtitle>
          {teams[0].members.length} integrantes
        </CompetitionVersus.Subtitle>
        <CompetitionVersus.Title>{teams[0].name}</CompetitionVersus.Title>
        <CompetitionVersus.Image source={{ uri: teams[0].photo }} />
      </>
    }
    opponent2={
      <>
        <CompetitionVersus.Image source={{ uri: teams[1].photo }} />
        <CompetitionVersus.Title>{teams[1].name}</CompetitionVersus.Title>
        <CompetitionVersus.Subtitle>
          {teams[1].members.length} integrantes
        </CompetitionVersus.Subtitle>
      </>
    }
  />
);

export default CompetitionTeamWithTwoOpponent;
