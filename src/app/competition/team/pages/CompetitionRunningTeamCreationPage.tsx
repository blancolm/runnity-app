import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useSuperMutation, useSuperQuery } from '@vadiun/react-hooks';
import { Button } from '@vadiun/react-native-components';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import RunningTeamListItem from 'app/runningTeam/components/runningTeamListItem';
import RunningTeamListItemLoading from 'app/runningTeam/components/RunningTeamListItemLoading';
import RunningTeamModel from 'app/runningTeam/models/RunningTeam';
import useRunningTeamRepository from 'app/runningTeam/services/useRunningTeamRepository';
import CustomColors from 'app/shared/helpers/CustomColors';
import React, { useState } from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View
} from 'react-native';
import { Divider, Searchbar, Text } from 'react-native-paper';
import { CompetitionStackNavigationType } from '../../main/navigation/CompetitionNavigationType';
import useCompetitionRepository from '../../main/services/useCompetitionRepository';

const CompetitionRunningTeamCreationPage = () => {
  const runningteamRepository = useRunningTeamRepository();
  const competitionRepository = useCompetitionRepository();
  const loggedRunner = useLoggedRunner();
  const runningTeamsQuery = useSuperQuery(runningteamRepository.getAll);
  const createMutation = useSuperMutation(
    competitionRepository.createRunningTeamCompetition,
    {
      showSuccessMessage: false
    }
  );
  const navigation =
    useNavigation<
      StackNavigationProp<CompetitionStackNavigationType, 'teamCreation'>
    >();

  const [searchBarText, setSearchBarText] = useState('');
  const [selectedCategory, setSelectedCategory] = useState<2 | 5 | 10>(2);
  const [competitionTeam, setCompetitionTeam] = useState<
    RunningTeamModel | undefined
  >(undefined);

  const createCompetition = async () => {
    await createMutation.mutate({
      runningTeamId: competitionTeam!.id,
      category: selectedCategory
    });
    navigation.goBack();
    navigation.navigate('privateCompetitionCreated');
  };

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={{ flexGrow: 1 }}
    >
      <View style={{ padding: 15 }}>
        <Searchbar
          placeholder="Buscar"
          value={searchBarText}
          onChangeText={(text) => {
            setSearchBarText(text);
          }}
          style={{ marginBottom: 15 }}
        />
        {runningTeamsQuery.data === undefined ? (
          <>
            <RunningTeamListItemLoading />
            <Divider />
            <RunningTeamListItemLoading />
            <Divider />
            <RunningTeamListItemLoading />
            <Divider />
          </>
        ) : (
          <FlatList
            data={runningTeamsQuery.data.filter(
              (team) =>
                team.name.toLowerCase().includes(searchBarText.toLowerCase()) &&
                team.id !== loggedRunner.runner?.group_id
            )}
            keyExtractor={(item) => String(item.id)}
            renderItem={({ item }) => (
              <TouchableOpacity
                activeOpacity={0.7}
                onPress={() => setCompetitionTeam(item)}
                style={{
                  borderRadius: 10,

                  backgroundColor:
                    competitionTeam?.id === item.id
                      ? CustomColors.dark300
                      : undefined
                }}
              >
                <RunningTeamListItem runningTeam={item} />
              </TouchableOpacity>
            )}
            ItemSeparatorComponent={Divider}
          />
        )}
      </View>
      <View style={styles.footer}>
        <Text style={{ fontSize: 16 }}>Categoría</Text>
        <View style={{ flexDirection: 'row', marginBottom: 10 }}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.category,
              selectedCategory === 2 ? styles.categorySelected : {}
            ]}
            onPress={() => setSelectedCategory(2)}
          >
            <Text style={{ fontFamily: 'poppins_bold' }}>2 K</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.category,
              selectedCategory === 5 ? styles.categorySelected : {}
            ]}
            onPress={() => setSelectedCategory(5)}
          >
            <Text style={{ fontFamily: 'poppins_bold' }}>5 K</Text>
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[
              styles.category,
              selectedCategory === 10 ? styles.categorySelected : {}
            ]}
            onPress={() => setSelectedCategory(10)}
          >
            <Text style={{ fontFamily: 'poppins_bold' }}>10 K</Text>
          </TouchableOpacity>
        </View>
        <Button
          mode="contained"
          loading={createMutation.isLoading}
          disabled={createMutation.isLoading}
          onPress={createCompetition}
        >
          Crear competencia
        </Button>
      </View>
    </ScrollView>
  );
};

export default CompetitionRunningTeamCreationPage;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: CustomColors.dark600,
    padding: 20
  },
  category: {
    flex: 1,
    backgroundColor: CustomColors.dark300,
    margin: 10,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 10
  },
  categorySelected: { borderColor: CustomColors.red500, borderWidth: 2 }
});
