import React from 'react';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useSuperMutation } from '@vadiun/react-hooks';
import CompetitionExecution from 'app/competition/main/components/CompetitionExecution';
import { RunningMeasure } from 'app/runningMeasures/models/RunningMeasure';
import { View, StyleSheet } from 'react-native';
import { Text } from 'react-native-paper';
import { CompetitionStackNavigationType } from '../../main/navigation/CompetitionNavigationType';
import useCompetitionRepository from '../../main/services/useCompetitionRepository';

const CompetitionRunningteamExecutionPage = () => {
  const competitionRepo = useCompetitionRepository();
  const navigation =
    useNavigation<
      StackNavigationProp<
        CompetitionStackNavigationType,
        'competitionRunningTeamExecution'
      >
    >();
  const route =
    useRoute<
      RouteProp<
        CompetitionStackNavigationType,
        'competitionRunningTeamExecution'
      >
    >();
  const { competition } = route.params;

  const stopMutation = useSuperMutation(
    async (runningMeasures: RunningMeasure) => {
      await competitionRepo.finishCompetition({
        ...runningMeasures,
        competitionId: competition.id
      });
      navigation.popToTop();
      navigation.navigate('competitionRunningTeamFinished', {
        measures: runningMeasures,
        competition
      });
    },
    {
      showSuccessMessage: false
    }
  );

  return (
    <CompetitionExecution
      onGoBack={navigation.goBack}
      stopMutation={stopMutation}
      totalDistance={competition.category * 1000}
    >
      <View>
        <Text style={styles.title}>Batalla de grupos</Text>
        <Text style={styles.competitor} numberOfLines={3}>
          {competition.participationsPerTeam
            .map((p) => p.runningTeam.name)
            .join(' VS ')}
        </Text>
      </View>
    </CompetitionExecution>
  );
};

export default CompetitionRunningteamExecutionPage;

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'poppins_bold',
    marginBottom: 10
  },
  competitor: {
    textAlign: 'center',
    fontSize: 14
  }
});
