import { RouteProp, useRoute } from '@react-navigation/native';
import { useSuperQuery } from '@vadiun/react-hooks';
import CompetitionResultListItem, {
  CompetitionResultListItemBadge,
  CompetitionResultListItemBody,
  CompetitionResultListItemImage,
  CompetitionResultListItemMeasures,
  CompetitionResultListItemProgressBar,
  CompetitionResultListItemTitle
} from 'app/competition/main/components/CompetitionResultListItem';
import {
  CompetitionParticipationFinished,
  CompetitionParticipationState
} from 'app/competition/main/models/Competition';
import { CompetitionStackNavigationType } from 'app/competition/main/navigation/CompetitionNavigationType';
import useCompetitionRepository from 'app/competition/main/services/useCompetitionRepository';
import caloriesCalculator from 'app/runningMeasures/hooks/caloriesCalculator';
import mediumPaceCalculator from 'app/runningMeasures/hooks/mediumPaceCalculator';
import React from 'react';
import { ListRenderItemInfo, View } from 'react-native';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import { Divider, Text } from 'react-native-paper';
import { CompetitionRunningTeamModel } from '../models/CompetitionRunningTeamModel';

const CompetitionRunningTeamResultsPage = () => {
  const route =
    useRoute<
      RouteProp<CompetitionStackNavigationType, 'competitionRunningTeamResults'>
    >();
  const competitionRepository = useCompetitionRepository();
  const { competition: competitionParam } = route.params;
  const competitionQuery = useSuperQuery(
    () =>
      competitionRepository.getCompetition(
        competitionParam.id
      ) as Promise<CompetitionRunningTeamModel>,
    { showSpinner: true }
  );

  const competition = competitionQuery.data;
  if (competition === undefined) {
    return null;
  }

  const resultsPerTeam = competition.participationsPerTeam.map((perTeam) => {
    const sum = perTeam.participations
      .filter((p) => p.state === CompetitionParticipationState.FINISHED)
      .map((p) => {
        const { distance, seconds } =
          p as any as CompetitionParticipationFinished;
        const mediumPace = mediumPaceCalculator.calculate(distance, seconds);
        const calories = caloriesCalculator.calculate({
          minutesPerKm: mediumPace,
          time: seconds,
          weight: 70
        });
        return {
          calories,
          mediumPace,
          distance,
          seconds
        };
      })
      .reduce(
        (acum, metrics) => ({
          calories: acum.calories + metrics.calories,
          mediumPace: acum.mediumPace + metrics.mediumPace,
          distance: acum.distance + metrics.distance,
          seconds: acum.seconds + metrics.seconds
        }),
        {
          calories: 0,
          mediumPace: 0,
          distance: 0,
          seconds: 0
        }
      );
    return {
      calories: sum.calories / perTeam.participations.length,
      mediumPace: sum.mediumPace / perTeam.participations.length,
      distance: sum.distance / perTeam.participations.length,
      seconds: sum.seconds / perTeam.participations.length
    };
  });

  return (
    <ScrollView style={{ flex: 1 }}>
      <Divider />
      <FlatList
        data={resultsPerTeam}
        keyExtractor={(item) => item.id}
        renderItem={({
          item,
          index
        }: ListRenderItemInfo<{
          calories: number;
          mediumPace: number;
          distance: number;
          seconds: number;
        }>) => (
          <CompetitionResultListItem>
            <CompetitionResultListItemImage
              source={{
                uri: competition.participationsPerTeam[index].runningTeam.photo
              }}
            />
            <CompetitionResultListItemBadge index={index} />
            <CompetitionResultListItemBody>
              <CompetitionResultListItemTitle>
                {competition.participationsPerTeam[index].runningTeam.name}
              </CompetitionResultListItemTitle>
              <CompetitionResultListItemProgressBar
                totalDistance={competition.category * 1000}
                distanceTraveled={item.distance}
              />
              <CompetitionResultListItemMeasures
                measures={{
                  seconds: item.seconds,
                  calories: item.calories,
                  mediumPace: item.mediumPace,
                  distance: item.distance
                }}
              />
            </CompetitionResultListItemBody>
          </CompetitionResultListItem>
        )}
        ItemSeparatorComponent={Divider}
      />
      <Divider />
      {competition.participationsPerTeam.map((perTeam) => (
        <View style={{ marginTop: 20 }}>
          <Text style={{ fontSize: 16, marginHorizontal: 20 }}>
            {perTeam.runningTeam.name}
          </Text>
          <FlatList
            data={perTeam.participations.filter(
              (p) => p.state === CompetitionParticipationState.FINISHED
            )}
            keyExtractor={(item) => item.id}
            renderItem={({
              item
            }: ListRenderItemInfo<CompetitionParticipationFinished>) => {
              const mediumPace = mediumPaceCalculator.calculate(
                item.distance,
                item.seconds
              );
              const calories = caloriesCalculator.calculate({
                minutesPerKm: mediumPace,
                time: item.seconds,
                weight: 70
              });
              return (
                <CompetitionResultListItem>
                  <CompetitionResultListItemImage
                    source={{ uri: item.runner.photo }}
                  />
                  <CompetitionResultListItemBody>
                    <CompetitionResultListItemTitle>
                      {item.runner.name}
                    </CompetitionResultListItemTitle>
                    <CompetitionResultListItemProgressBar
                      totalDistance={competition.category * 1000}
                      distanceTraveled={item.distance}
                    />
                    <CompetitionResultListItemMeasures
                      measures={{
                        seconds: item.seconds,
                        calories,
                        mediumPace,
                        distance: item.distance
                      }}
                    />
                  </CompetitionResultListItemBody>
                </CompetitionResultListItem>
              );
            }}
            ItemSeparatorComponent={Divider}
          />
        </View>
      ))}
    </ScrollView>
  );
};

export default CompetitionRunningTeamResultsPage;
