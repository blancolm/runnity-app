import {
  NavigationProp,
  RouteProp,
  useNavigation,
  useRoute
} from '@react-navigation/native';
import { Button } from '@vadiun/react-native-components';
import CompetitionFinished from 'app/competition/main/components/CompetitionFinished';
import { CompetitionStackNavigationType } from 'app/competition/main/navigation/CompetitionNavigationType';
import React from 'react';

const CompetitionRunningTeamFinishedPage = () => {
  const route =
    useRoute<
      RouteProp<
        CompetitionStackNavigationType,
        'competitionRunningTeamFinished'
      >
    >();
  const navigation =
    useNavigation<
      NavigationProp<
        CompetitionStackNavigationType,
        'competitionRunningTeamFinished'
      >
    >();

  const { measures, competition } = route.params;
  return (
    <CompetitionFinished measures={measures}>
      <Button
        onPress={() =>
          navigation.navigate('competitionRunningTeamResults', { competition })
        }
      >
        Ver tabla de resultados
      </Button>
    </CompetitionFinished>
  );
};

export default CompetitionRunningTeamFinishedPage;
