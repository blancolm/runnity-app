import {
  NavigationProp,
  RouteProp,
  useNavigation,
  useRoute
} from '@react-navigation/native';
import { Button } from '@vadiun/react-native-components';
import { CompetitionStackNavigationType } from 'app/competition/main/navigation/CompetitionNavigationType';
import React from 'react';
import { Divider, Text } from 'react-native-paper';
import { Dimensions, ScrollView, View } from 'react-native';
import CustomColors from 'app/shared/helpers/CustomColors';
import CompetitionPrivateWithMultipleOpponents from 'app/competition/private/components/CompetitionPrivateWithMultipleOpponents';
import CompetitionGroupWithTwoOpponents from '../components/CompetitionTeamWithTwoOpponents';

const CompetitionRunningTeamDetailPage = () => {
  const route =
    useRoute<RouteProp<CompetitionStackNavigationType, 'teamDetail'>>();
  const navigation =
    useNavigation<
      NavigationProp<CompetitionStackNavigationType, 'teamDetail'>
    >();
  const { competition } = route.params;

  return (
    <View style={{ flex: 1 }}>
      <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
        <View style={{ height: Dimensions.get('window').height - 200 }}>
          <CompetitionGroupWithTwoOpponents
            teams={[
              competition.participationsPerTeam[0].runningTeam,
              competition.participationsPerTeam[1].runningTeam
            ]}
          />
        </View>
        <Divider />
        <View style={{ margin: 20 }}>
          {competition.participationsPerTeam.map((perTeam) => (
            <View style={{ marginBottom: 15 }}>
              <Text style={{ fontSize: 18, fontFamily: 'poppins_bold' }}>
                {perTeam.runningTeam.name}
              </Text>
              <CompetitionPrivateWithMultipleOpponents
                participations={perTeam.participations}
              />
            </View>
          ))}
        </View>
      </ScrollView>
      <Text style={{ textAlign: 'center', color: CustomColors.secondaryText }}>
        Finalización {competition.endDateTime.format('DD/MM/YYYY HH:mm')} hs.
      </Text>
      <Button
        mode="contained"
        onPress={() =>
          navigation.navigate('competitionRunningTeamExecution', {
            competition
          })
        }
      >
        Competir
      </Button>
    </View>
  );
};

export default CompetitionRunningTeamDetailPage;
