import {
  CompetitionModel,
  CompetitionParticipation,
  CompetitionParticipationState,
  CompetitionType
} from 'app/competition/main/models/Competition';
import { runnerBuilder } from 'app/runner/models/Runner';
import RunningTeamModel, {
  runningTeamBuilder
} from 'app/runningTeam/models/RunningTeam';
import moment from 'moment';

export interface CompetitionRunningTeamModel
  extends Omit<CompetitionModel, 'participations'> {
  participationsPerTeam: [
    {
      runningTeam: RunningTeamModel;
      participations: CompetitionParticipation[];
    },
    {
      runningTeam: RunningTeamModel;
      participations: CompetitionParticipation[];
    }
  ];
}

export const competitionRunningTeamBuilder = {
  fromBackEnd: (competition: any): CompetitionRunningTeamModel => ({
    ...competition,
    endDateTime: moment(competition.end_datetime),
    initDateTime: moment(competition.init_datetime),
    type:
      competition.type === 1
        ? CompetitionType.PRIVATE
        : competition.type === 2
        ? CompetitionType.RANDOM
        : CompetitionType.GROUP,
    category:
      competition.category === 1 ? 2 : competition.category === 2 ? 5 : 10,
    participationsPerTeam: [
      {
        runningTeam: runningTeamBuilder.fromBackEnd(
          competition.groups[0].group
        ),
        participations: competition.groups[0].competitors.map((p: any) => ({
          ...p,
          state:
            p.state === null
              ? CompetitionParticipationState.PENDING
              : CompetitionParticipationState.FINISHED,
          runner: runnerBuilder.fromBackEnd(p.runner)
        }))
      },
      {
        runningTeam: runningTeamBuilder.fromBackEnd(
          competition.groups[1].group
        ),
        participations: competition.groups[1].competitors.map((p: any) => ({
          ...p,
          state:
            p.state === null
              ? CompetitionParticipationState.PENDING
              : CompetitionParticipationState.FINISHED,
          runner: runnerBuilder.fromBackEnd(p.runner)
        }))
      }
    ]
  })
};

export function isRunningTeamCompetition(
  comp: CompetitionModel | CompetitionRunningTeamModel
): comp is CompetitionRunningTeamModel {
  return (
    (comp as CompetitionRunningTeamModel).participationsPerTeam !== undefined
  );
}
