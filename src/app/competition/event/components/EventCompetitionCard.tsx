import { CompetitionModel } from 'app/competition/main/models/Competition';
import React from 'react';
import { Text } from 'react-native-paper';
import CompetitionCard from '../../main/components/CompetitionCard';

interface Props {
  competition: CompetitionModel;
  onParticipate: () => void;
}

const EventCompetitionCard = ({ competition, onParticipate }: Props) => (
  <CompetitionCard image={{ uri: competition.participations[0].runner.photo }}>
    <CompetitionCard.Body>
      <CompetitionCard.Title>Evento semanal</CompetitionCard.Title>
      <Text>{`${competition.category} K`}</Text>
      <Text>Categoría ORO 1</Text>
    </CompetitionCard.Body>
    <CompetitionCard.Button onPress={onParticipate}>
      Participar
    </CompetitionCard.Button>
  </CompetitionCard>
);

export default EventCompetitionCard;
