export function toTitleCase(str: string) {
  return str.replace(
    /\w\S*/g,
    (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
  );
}

export async function syncLoadingState(
  loadingSetter: (x: boolean) => void,
  callback: () => Promise<any>
) {
  loadingSetter(true);
  try {
    await callback();
    loadingSetter(false);
  } catch {
    loadingSetter(false);
  }
}
