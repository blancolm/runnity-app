import MockAdapter from 'axios-mock-adapter';
import faker from 'faker';
import ChallengeModel from 'app/challenge/models/ChallengeModel';
import RunningTeamModel from 'app/runningTeam/models/RunningTeam';
import {
  CompetitionModel,
  CompetitionParticipationState,
  CompetitionType
} from 'app/competition/main/models/Competition';
import RunnerModel from 'app/runner/models/Runner';
import moment from 'moment';
import { CompetitionRunningTeamModel } from 'app/competition/team/models/CompetitionRunningTeamModel';
import axiosInstance, { instance } from './axiosInstance';

const mock = new MockAdapter(axiosInstance, { delayResponse: 0 });

const createN = <T extends Object>(creationFn: () => T, amount: number) =>
  Array(amount)
    .fill(0)
    .map(creationFn)
    .map((elem, index) => ({ ...elem, id: index }));

const createChallenge = (): ChallengeModel => ({
  id: 0,
  name: faker.lorem.sentence(),
  description: faker.lorem.paragraphs(),
  photo: faker.image.abstract()
});

const createRunner = (): RunnerModel => ({
  id: 434,
  name: 'Prof. Travis Krajcik III',
  birthdate: moment(),
  sex: 'Dr. Brett Prosacco',
  weight: 1159.99,
  photo: 'http://localhost/storage/tmp/e6bbcffc27607ad7f23421fe8efcf9fc.png',
  user_id: 823,
  group_id: null,
  level_2k: 1,
  level_5k: 1,
  level_10k: 1,
  seconds_2k: null,
  seconds_5k: null,
  seconds_10k: null
});

// const createRunningTeam = (): RunningTeamModel => ({
//   id: 0,
//   name: faker.lorem.words(),
//   photo: faker.image.sports(),
//   members: createN(createRunner, 10)
// });

const challenges = createN(createChallenge, 10);
// const runners = createN(createRunner, 10);

const runningMoron: RunningTeamModel = {
  id: 1,
  name: 'Running Moron',
  photo: faker.image.sports(),
  members: createN(createRunner, 10)
};
const losMagios: RunningTeamModel = {
  id: 2,
  name: 'Los Magios',
  photo: faker.image.sports(),
  members: createN(createRunner, 10)
};
const lucas: RunnerModel = {
  id: 1,
  photo:
    'https://inspirationalife.com/wp-content/uploads/2021/03/what-are-the-signs-of-a-lazy-person.jpg',
  name: 'Lucas Blanco',
  group_id: null,
  birthdate: moment('1995-10-27'),
  weight: 80,
  sex: 'male',
  user_id: 823,
  level_2k: 1,
  level_5k: 1,
  level_10k: 1,
  seconds_2k: null,
  seconds_5k: null,
  seconds_10k: null
};
const joaquin: RunnerModel = {
  id: 2,
  name: 'Joaquin Mazoud',
  photo:
    'https://inspirationalife.com/wp-content/uploads/2021/03/what-are-the-signs-of-a-lazy-person.jpg',
  birthdate: moment('1995-10-27'),
  weight: 80,
  sex: 'male',
  user_id: 823,
  group_id: 1,
  level_2k: 1,
  level_5k: 1,
  level_10k: 1,
  seconds_2k: null,
  seconds_5k: null,
  seconds_10k: null
};
const gonzalo: RunnerModel = {
  id: 3,
  name: 'Gonzalo',
  photo: faker.image.people(),
  birthdate: moment('1995-10-27'),
  weight: 80,
  sex: 'male',
  user_id: 823,
  group_id: 1,
  level_2k: 1,
  level_5k: 1,
  level_10k: 1,
  seconds_2k: null,
  seconds_5k: null,
  seconds_10k: null
};
const fede: RunnerModel = {
  id: 4,
  name: 'Federico Hombre',
  photo:
    'https://st.depositphotos.com/1424188/3074/i/600/depositphotos_30741729-stock-photo-young-man-sprinter-runner-running.jpg',
  birthdate: moment('1995-10-27'),
  weight: 80,
  sex: 'male',
  user_id: 823,
  group_id: 1,
  level_2k: 1,
  level_5k: 1,
  level_10k: 1,
  seconds_2k: null,
  seconds_5k: null,
  seconds_10k: null
};

const runningTeams = [runningMoron, losMagios];

const privateCompetition1: CompetitionModel = {
  id: 3,
  participations: [
    {
      runner: joaquin,
      state: CompetitionParticipationState.FINISHED,
      distance: 2000,
      seconds: 600
    },
    {
      runner: fede,
      state: CompetitionParticipationState.FINISHED,
      distance: 2000,
      seconds: 300
    }
  ],
  category: 5,
  endDateTime: moment().add(3, 'days'),
  type: CompetitionType.PRIVATE
};
const privateCompetition2: CompetitionModel = {
  id: 2,
  participations: [
    {
      runner: lucas,
      state: CompetitionParticipationState.PENDING
    },
    {
      runner: gonzalo,
      state: CompetitionParticipationState.FINISHED,
      distance: 3000,
      seconds: 6000
    },
    {
      runner: joaquin,
      state: CompetitionParticipationState.FINISHED,
      distance: 5000,
      seconds: 6000
    }
  ],
  category: 5,
  endDateTime: moment().add(3, 'days'),
  type: CompetitionType.PRIVATE
};

const groupCompetition: CompetitionRunningTeamModel = {
  category: 5,
  id: 1,
  endDateTime: moment().add(3, 'days'),
  type: CompetitionType.GROUP,
  participationsPerTeam: [
    {
      runningTeam: runningMoron,
      participations: [
        {
          runner: lucas,
          state: CompetitionParticipationState.FINISHED,
          distance: 3000,
          seconds: 7000
        },
        {
          runner: gonzalo,
          state: CompetitionParticipationState.FINISHED,
          distance: 4000,
          seconds: 7000
        }
      ]
    },
    {
      runningTeam: losMagios,
      participations: [
        {
          runner: joaquin,
          state: CompetitionParticipationState.FINISHED,
          distance: 5000,
          seconds: 7000
        }
      ]
    }
  ]
};

mock.onGet('/challenges').reply(200, challenges);
mock.onGet('/running-teams').reply(200, runningTeams);
mock.onPost('/running-teams').reply(200);
mock.onPost(/\/running-teams\/\d+\/members/).reply(200);
mock.onPost('competitions/private').reply(200);
mock.onGet('competitions/private').reply(200, [privateCompetition1]);
mock.onGet('competitions/random').reply(200, [privateCompetition1]);
mock.onGet('competitions/team').reply(200, []);
mock.onPost('competitions/random').reply(200);
mock.onPost('competitions/team').reply(200);
mock.onGet('/competitions/findOpponent').reply(200, [lucas, joaquin, gonzalo]);
mock.onPut(/\/competitions\/\d+\/finish/).reply(200);
mock.onGet(/\/running-teams\/\d+/).reply(200, runningMoron);
mock.onPut(/\/running-teams\/\d+/).reply(200);
mock.onGet(/\/runners\/\d+/).reply(200, lucas);
mock.onGet('runners').reply(200, [lucas, joaquin, gonzalo]);

export default axiosInstance;
export const httpClient = instance;
