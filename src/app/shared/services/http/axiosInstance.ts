// @ts-nocheck
import axios, { AxiosError } from 'axios';
import environment from 'environment/environment';
import AsyncStorage from '@react-native-async-storage/async-storage';

const mockedInstance = axios.create({
  baseURL: environment.backEnd
});

const realInstance = axios.create({
  baseURL: environment.backEnd
});

mockedInstance.interceptors.request.use(async (configParam) => {
  const config = configParam;
  const token = await AsyncStorage.getItem('token');
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  // if (config.url) {
  //   if (config.url.includes('?') && !config.url.includes('/?')) {
  //     config.url = config.url.replace('?', '/?');
  //   }
  //   if (
  //     !config.url.includes('?') &&
  //     config.url[config.url!.length - 1] !== '/'
  //   ) {
  //     config.url += '/';
  //   }
  // }
  return config;
});

realInstance.interceptors.request.use(async (configParam) => {
  const config = configParam;
  const token = await AsyncStorage.getItem('token');
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

mockedInstance.interceptors.response.use(
  (response) =>
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    response.data,
  (error: AxiosError) =>
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    {
      debugger;
      return Promise.reject(
        new Error(error?.response?.data.messages.join(' '))
      );
    }
);

realInstance.interceptors.response.use(
  (response) =>
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    response.data,
  (error: AxiosError) =>
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    // debugger;
    Promise.reject(new Error(error?.response?.data.messages.join(' ')))
);

export default mockedInstance;
export const instance = realInstance;
