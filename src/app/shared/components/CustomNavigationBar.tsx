import { StackHeaderProps } from '@react-navigation/stack';
import React from 'react';
import { Appbar } from 'react-native-paper';

interface Props extends StackHeaderProps {}

function CustomNavigationBar(props: Props) {
  return (
    <Appbar.Header>
      {props.previous ? (
        <Appbar.BackAction onPress={props.navigation.goBack} />
      ) : null}
      <Appbar.Content title={props.scene.descriptor.options.title} />
    </Appbar.Header>
  );
}

export default CustomNavigationBar;
