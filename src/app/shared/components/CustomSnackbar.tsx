import React, { useEffect, useState } from 'react';
import { Dimensions, View } from 'react-native';
import { Text, Snackbar } from 'react-native-paper';
import { Ionicons } from '@expo/vector-icons';
import CustomColors from '../helpers/CustomColors';

declare type Props = {
  message: string;
  type: 'error' | 'success' | 'info' | 'warning';
  // eslint-disable-next-line react/no-unused-prop-types
  title?: string;
  isVisible: boolean;
};

const CustomSnackbar = (props: Props) => {
  const [isVisible, setIsVisible] = useState(false);

  useEffect(() => {
    setIsVisible(props.isVisible);
  }, [props.isVisible, setIsVisible]);

  const icons = {
    success: (
      <Ionicons
        name="checkmark-circle-outline"
        size={24}
        color={CustomColors.green500}
      />
    ),
    error: (
      <Ionicons
        name="close-circle-outline"
        size={24}
        color={CustomColors.red500}
      />
    ),
    info: (
      <Ionicons
        name="information-circle-outline"
        size={24}
        color={CustomColors.blue500}
      />
    ),
    warning: (
      <Ionicons
        name="alert-circle-outline"
        size={24}
        color={CustomColors.yellow500}
      />
    )
  };
  return (
    <Snackbar
      visible={isVisible}
      onDismiss={() => {}}
      style={{ backgroundColor: CustomColors.dark300 }}
    >
      <View
        style={{
          flexDirection: 'row',
          width: Dimensions.get('window').width - 40,
          alignItems: 'center'
        }}
      >
        {icons[props.type]}
        <Text style={{ marginLeft: 10 }}>{props.message}</Text>
      </View>
    </Snackbar>
  );
};

export default CustomSnackbar;
