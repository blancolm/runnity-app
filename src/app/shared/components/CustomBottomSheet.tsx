import BottomSheet, { BottomSheetBackgroundProps } from '@gorhom/bottom-sheet';
import React, { ComponentProps, useMemo } from 'react';
import Animated from 'react-native-reanimated';
import CustomColors from '../helpers/CustomColors';

const CustomBackground = ({ style }: BottomSheetBackgroundProps) => {
  const containerStyle = useMemo(
    () => [
      style,
      {
        backgroundColor: CustomColors.dark600,
        borderRadius: 20
      }
    ],
    [style]
  );

  return <Animated.View style={containerStyle} />;
};

const CustomBottomSheet = (props: ComponentProps<typeof BottomSheet>) => (
  <BottomSheet {...props} backgroundComponent={CustomBackground} />
);

export default CustomBottomSheet;
