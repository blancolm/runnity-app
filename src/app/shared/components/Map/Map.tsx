import React, { ComponentProps } from 'react';
import MapView from 'react-native-maps';
import styles from './MapStyles';

const Map = (
  props: ComponentProps<typeof MapView> & { children: React.ReactNode }
) => (
  <MapView
    zoomEnabled={false}
    zoomTapEnabled={false}
    zoomControlEnabled={false}
    rotateEnabled={false}
    cacheEnabled={false}
    loadingEnabled={false}
    scrollEnabled={false}
    pitchEnabled={false}
    customMapStyle={styles}
    toolbarEnabled={false}
    mapType="standard"
    {...props}
  />
);

export default Map;
