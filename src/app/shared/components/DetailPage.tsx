import CustomColors from 'app/shared/helpers/CustomColors';
import React, { FC } from 'react';
import {
  Dimensions,
  StyleSheet,
  View,
  Image,
  ImageSourcePropType,
  ScrollView
} from 'react-native';

interface Props {
  imageSrc: ImageSourcePropType;
}

const DetailPage: FC<Props> = ({ imageSrc, children }) => (
  <ScrollView style={styles.container} contentContainerStyle={{ flexGrow: 1 }}>
    <Image source={imageSrc} style={styles.image} />
    <View style={styles.body}>{children}</View>
  </ScrollView>
);

export default DetailPage;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  body: {
    padding: 30,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    marginTop: -20,
    elevation: 1,
    backgroundColor: CustomColors.dark900,
    flex: 1
  },
  image: {
    width: '100%',
    height: Dimensions.get('window').height / 4
  }
});
