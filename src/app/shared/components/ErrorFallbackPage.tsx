import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { List, Paragraph, Title } from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';
import { Button } from '@vadiun/react-native-components';
import CustomColors from '../helpers/CustomColors';

interface Props {
  error: Error;
  resetError: () => void;
}

const ErrorFallbackPage = (props: Props) => (
  <SafeAreaView style={styles.container}>
    <ScrollView style={{ flex: 1 }}>
      <Title style={{ textAlign: 'center' }}>Algo ha salido mal</Title>
      <List.Accordion
        title="Detalles"
        style={{ width: '100%' }}
        left={(_props) => <List.Icon {..._props} icon="menu" />}
      >
        <Paragraph style={styles.body}>{props.error.toString()}</Paragraph>
      </List.Accordion>

      <Button onPress={props.resetError} mode="light">
        Reiniciar
      </Button>
    </ScrollView>
  </SafeAreaView>
);

export default ErrorFallbackPage;

const styles = StyleSheet.create({
  container: {
    backgroundColor: CustomColors.dark900,
    flex: 1,
    justifyContent: 'center',
    padding: 20
  },
  body: {
    marginVertical: 20
  }
});
