import React from 'react';
import {
  GestureResponderEvent,
  StyleProp,
  StyleSheet,
  View,
  ViewStyle
} from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import CustomColors from 'app/shared/helpers/CustomColors';

interface Props {
  children: React.ReactNode;
  onPress?: (e: GestureResponderEvent) => void;
  textAvatar?: React.ReactNode;
  bottom?: React.ReactNode;
  style?: StyleProp<ViewStyle>;
  right?: React.ReactNode;
}

const ListItem = (props: Props) => {
  const textAvatar = props.textAvatar && (
    <View style={styles.textAvatar}>{props.textAvatar}</View>
  );
  return (
    <TouchableRipple style={styles.item} onPress={props.onPress}>
      <View style={[props.style]}>
        <View style={styles.itemBody}>
          {textAvatar}
          <View style={styles.childrenContainer}>{props.children}</View>

          {props.right && (
            <View style={{ marginLeft: 'auto' }}>{props.right}</View>
          )}
        </View>
        {props.bottom && <View style={{ marginTop: 15 }}>{props.bottom}</View>}
      </View>
    </TouchableRipple>
  );
};

export default ListItem;

const styles = StyleSheet.create({
  item: {
    paddingHorizontal: 15,
    paddingVertical: 15
  },
  itemBody: {
    flexDirection: 'row'
  },
  avatar: {
    marginRight: 20
  },
  textAvatar: {
    backgroundColor: CustomColors.primary,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    marginRight: 20
  },
  childrenContainer: {
    flexGrow: 1
  }
});
