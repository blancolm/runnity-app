import React, { useEffect, useMemo, useRef } from 'react';
import { Animated, StyleSheet, ViewStyle } from 'react-native';
import CustomColors from '../helpers/CustomColors';

interface Props {
  style?: ViewStyle;
}

const LoadingPlaceholder = ({ style }: Props) => {
  const fadeAnim = useRef(new Animated.Value(0)).current;

  const animation = useMemo(
    () =>
      Animated.loop(
        Animated.sequence([
          Animated.timing(fadeAnim, {
            toValue: 1,
            duration: 700,
            useNativeDriver: true
          }),
          Animated.timing(fadeAnim, {
            toValue: 0.4,
            duration: 700,
            useNativeDriver: true
          })
        ])
      ),
    [fadeAnim]
  );

  useEffect(() => {
    animation.start();
  }, [animation]);

  return (
    <Animated.View
      style={[
        styles.base,
        { opacity: fadeAnim, backgroundColor: CustomColors.dark600 },
        style,
        style?.width ? { minWidth: 0 } : { minWidth: '100%' }
      ]}
    />
  );
};

export default LoadingPlaceholder;

const styles = StyleSheet.create({
  base: {
    borderRadius: 10,
    height: 60
  }
});
