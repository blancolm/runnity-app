import React from 'react';
import { View } from 'react-native';
import { Divider } from 'react-native-paper';

interface Props {
  children: React.ReactNode;
}

const List = (props: Props) => (
  <View>
    {React.Children.map(props.children, (child) => (
      <>
        <Divider />
        {child}
      </>
    ))}
    <Divider />
  </View>
);

export default List;
