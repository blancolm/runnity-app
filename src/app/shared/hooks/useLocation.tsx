import { useEffect, useState } from 'react';
import * as Location from 'expo-location';

const useLocation = () => {
  const [location, setLocation] =
    useState<{ lat: number; lng: number } | undefined>(undefined);
  const [permissionGranted, setPermissionGranted] = useState(false);

  async function requestPermission() {
    try {
      const { status } = await Location.requestForegroundPermissionsAsync();
      setPermissionGranted(status === 'granted');
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(error);
      setPermissionGranted(false);
    }
  }

  useEffect(() => {
    Location.getForegroundPermissionsAsync().then((response) => {
      setPermissionGranted(response.granted);
    });
  }, []);

  useEffect(() => {
    if (!permissionGranted) return;
    Location.getCurrentPositionAsync({
      accuracy: Location.LocationAccuracy.Highest
    })
      .then((_location) => {
        setLocation({
          lat: _location.coords.latitude,
          lng: _location.coords.longitude
        });
      })
      .catch((error) => {
        // eslint-disable-next-line no-console
        console.error(error);
      });
  }, [permissionGranted]);

  return { location, permissionGranted, requestPermission };
};

export default useLocation;
