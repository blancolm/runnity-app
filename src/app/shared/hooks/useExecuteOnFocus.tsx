import { useIsFocused } from '@react-navigation/native';
import { useEffect } from 'react';

const useExecuteOnFocus = (callback: () => void) => {
  const isFocused = useIsFocused();

  useEffect(() => {
    if (isFocused) {
      callback();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);
};

export default useExecuteOnFocus;
