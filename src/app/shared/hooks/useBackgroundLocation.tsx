import React, {
  createContext,
  FC,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState
} from 'react';
import * as Location from 'expo-location';
import * as TaskManager from 'expo-task-manager';

type PermissionStatus = 'pending' | 'granted' | 'denied';

type BackgroundLocationType = {
  location: Coordenates | undefined;
  permissionStatus: {
    foreground: PermissionStatus;
    background: PermissionStatus;
  };
  requestPermissions: () => void;
};

const LOCATION_TRACKING = 'location-tracking';

type Coordenates = {
  lat: number;
  lng: number;
};

type Callback = (coords: Coordenates) => void;

let onLocationChangeCallback: Callback;

export const startBackgrounLocationTaskManager = () =>
  TaskManager.defineTask(LOCATION_TRACKING, async ({ data, error }) => {
    if (error) {
      // eslint-disable-next-line no-console
      console.error('LOCATION_TRACKING task ERROR:', error);
      return;
    }

    if (data) {
      const { locations } = data as {
        locations: { coords: { latitude: number; longitude: number } }[];
      };
      const lat = locations[0].coords.latitude;
      const lng = locations[0].coords.longitude;
      if (onLocationChangeCallback) {
        onLocationChangeCallback({ lat, lng });
      }
    }
  });

const BackgroundLocationContext = createContext({} as BackgroundLocationType);

export const BackgroundLocationProvider: FC = ({ children }) => {
  const [location, setLocation] = useState<Coordenates | undefined>(undefined);
  const [foregroundPermissionsStatus, setForegroundPermissionStatus] =
    useState<PermissionStatus>('pending');
  const [backgroundPermissionsStatus, setBackgroundPermissionStatus] =
    useState<PermissionStatus>('pending');

  const startLocalization = () => {
    Location.startLocationUpdatesAsync(LOCATION_TRACKING, {
      accuracy: Location.Accuracy.Highest,
      timeInterval: 1000,
      distanceInterval: 50, // minimum change (in meters) betweens updates
      deferredUpdatesInterval: 2000, // minimum interval (in milliseconds) between updates
      // foregroundService is how you get the task to be updated as often as would be if the app was open
      foregroundService: {
        notificationTitle: 'Runnity',
        notificationBody: 'Estamos midiendo tu desempeño. ¡Sigue así!'
      }
    }).catch((error) => {
      // eslint-disable-next-line no-console
      console.error(`startLocationUpdatesAsync failed: ${error.message}`);
    });
  };

  const stopLocalization = () => {
    Location.stopLocationUpdatesAsync(LOCATION_TRACKING);
  };

  const getForegroundPermission = async () => {
    try {
      const foregroundPermission =
        await Location.requestForegroundPermissionsAsync();

      setForegroundPermissionStatus(
        foregroundPermission.granted ? 'granted' : 'denied'
      );
    } catch (error) {
      setForegroundPermissionStatus('denied');
      // eslint-disable-next-line no-console
      console.error(`getForegroundPermission failed: ${error.message}`);
    }
  };

  const getBackgroundPermission = async () => {
    try {
      const backgroundPermission =
        await Location.requestBackgroundPermissionsAsync();
      setBackgroundPermissionStatus(
        backgroundPermission.granted ? 'granted' : 'denied'
      );
    } catch (error) {
      setBackgroundPermissionStatus('denied');
      // eslint-disable-next-line no-console
      console.error(`getBackgroundPermission failed: ${error.message}`);
    }
  };

  const requestPermissions = useCallback(() => {
    if (foregroundPermissionsStatus !== 'granted') {
      getForegroundPermission();
      return;
    }
    if (backgroundPermissionsStatus !== 'granted') {
      getBackgroundPermission();
    }
  }, [foregroundPermissionsStatus, backgroundPermissionsStatus]);

  useEffect(() => {
    onLocationChangeCallback = setLocation;
    return stopLocalization;
  }, []);

  useEffect(() => {
    if (foregroundPermissionsStatus === 'denied') return;
    getBackgroundPermission();
  }, [foregroundPermissionsStatus]);

  useEffect(() => {
    Location.getForegroundPermissionsAsync().then((permission) => {
      setForegroundPermissionStatus(permission.granted ? 'granted' : 'denied');
    });
    Location.getBackgroundPermissionsAsync().then((permission) => {
      setBackgroundPermissionStatus(permission.granted ? 'granted' : 'denied');
    });
  }, []);

  useEffect(() => {
    if (backgroundPermissionsStatus === 'denied') return;
    Location.hasStartedLocationUpdatesAsync(LOCATION_TRACKING)
      .then((hasStarted) => {
        if (!hasStarted) {
          startLocalization();
        }
      })
      .catch((error) => {
        setBackgroundPermissionStatus('denied');
        // eslint-disable-next-line no-console
        console.error(
          `hasStartedLocationUpdatesAsync failed: ${error.message}`
        );
      });
  }, [backgroundPermissionsStatus]);

  const value = useMemo(
    () => ({
      location,
      permissionStatus: {
        foreground: foregroundPermissionsStatus,
        background: backgroundPermissionsStatus
      },
      requestPermissions
    }),
    [
      location,
      foregroundPermissionsStatus,
      backgroundPermissionsStatus,
      requestPermissions
    ]
  );

  return (
    <BackgroundLocationContext.Provider value={value}>
      {children}
    </BackgroundLocationContext.Provider>
  );
};

const useBackgroundLocation = () => {
  const context = useContext(BackgroundLocationContext);
  if (context.requestPermissions === undefined) {
    throw new Error(
      'useBackgroundLocation must be used inside BackgroundLocationProvider'
    );
  }
  return context;
};

export default useBackgroundLocation;
