import { Linking } from 'react-native';

const useRedirectToPhoneCall = () => {
  function call(phone: string) {
    Linking.openURL(`tel:${phone}`);
  }

  return call;
};

export default useRedirectToPhoneCall;
