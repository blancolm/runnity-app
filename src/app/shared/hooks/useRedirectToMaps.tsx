import { Linking, Platform } from 'react-native';

const useRedirectToMaps = () => {
  function selectNavigationApp(point: { lat: number; lng: number }) {
    const scheme = Platform.select({
      ios: 'maps:0,0?q=',
      android: 'geo:0,0?q='
    });
    const latLng = `${point.lat},${point.lng}`;
    const label = 'Proximo destino';
    const url = Platform.select({
      ios: `${scheme}${label}@${latLng}`,
      android: `${scheme}${latLng}(${label})`
    });
    Linking.openURL(url!);
  }

  return selectNavigationApp;
};

export default useRedirectToMaps;
