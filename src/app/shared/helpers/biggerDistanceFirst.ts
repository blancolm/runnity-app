export const biggerDistanceFirst = (
  a: { distance: number; seconds: number },
  b: { distance: number; seconds: number }
) => {
  if (a.distance > b.distance) {
    return -1;
  }
  if (a.distance < b.distance) {
    return 1;
  }
  if (a.seconds < b.seconds) {
    return -1;
  }
  if (a.seconds > b.seconds) {
    return 1;
  }
  return 0;
};
