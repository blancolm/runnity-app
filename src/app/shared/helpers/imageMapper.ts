import environment from 'environment/environment';

export const imageMapper = (image: string) => {
  if (environment.production) {
    return image.replace('localhost', environment.localIp);
  }
  return image.replace('localhost', environment.localIp);
};
