import { createStackNavigator } from '@react-navigation/stack';
import ForgotPasswordPage from 'app/auth/pages/ForgotPasswordPage';
import LoginPage from 'app/auth/pages/LoginPage';
import RegisterPage from 'app/auth/pages/RegisterPage';
import React from 'react';

export type AuthNavigationType = {
  login: undefined;
  register: undefined;
  forgotPassword: undefined;
};

const Stack = createStackNavigator<AuthNavigationType>();

const AuthNavigation = () => (
  <Stack.Navigator initialRouteName="login">
    <Stack.Screen
      name="login"
      options={{
        title: 'Login',
        headerShown: false
      }}
      component={LoginPage}
    />
    <Stack.Screen
      name="register"
      options={{
        title: 'Registro'
      }}
      component={RegisterPage}
    />
    <Stack.Screen
      name="forgotPassword"
      options={{
        title: 'Olvidé mi contraseña'
      }}
      component={ForgotPasswordPage}
    />
  </Stack.Navigator>
);

export default AuthNavigation;
