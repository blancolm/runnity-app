import React, { createContext, useContext, useEffect, useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LoginCredentials } from 'app/auth/models/User';
import { httpClient } from 'app/shared/services/http/httpClient';
import jwtDecode from 'jwt-decode';

export interface AuthContextType {
  isAuthenticated: boolean;
  login: (x: LoginCredentials) => Promise<any>;
  logout: () => void;
  loggedUserID: number | undefined;
  forgotPassword: (x: { email: string }) => Promise<void>;
  register: (x: {
    name: string;
    password: string;
    passwordRepeat: string;
    email: string;
  }) => Promise<void>;
}

export const AuthContext = createContext({} as AuthContextType);

export const AuthProvider = (props: any) => {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  const [loggedUserID, setLoggedUserID] = useState<number | undefined>(
    undefined
  );

  useEffect(() => {
    AsyncStorage.getItem('token').then((token) => setIsAuthenticated(!!token));
  }, []);

  useEffect(() => {
    if (isAuthenticated) {
      AsyncStorage.getItem('token').then((token) => {
        const { user_id } = jwtDecode<{ user_id: number }>(token!);
        setLoggedUserID(user_id);
      });
    }
  }, [isAuthenticated]);

  async function login(args: { email: string; password: string }) {
    const { token } = await httpClient.post<{ token: string }>('login', args);
    AsyncStorage.setItem('token', token);
    setIsAuthenticated(true);
    return token;
  }

  function logout() {
    AsyncStorage.removeItem('token');
    setIsAuthenticated(false);
    setLoggedUserID(undefined);
  }

  function forgotPassword({ email }: { email: string }) {
    return httpClient.post('forgot-password', { email });
  }

  function register(x: {
    name: string;
    password: string;
    passwordRepeat: string;
    email: string;
  }) {
    return httpClient.post('register', {
      password: x.password,
      password_confirmation: x.passwordRepeat,
      name: x.name,
      email: x.email
    });
  }

  const value: AuthContextType = {
    isAuthenticated,
    login,
    logout,
    loggedUserID,
    forgotPassword,
    register
  };
  return <AuthContext.Provider value={value} {...props} />;
};

export const useAuth = () => {
  const authContext = useContext(AuthContext);
  if (!authContext.login) {
    throw new Error('useAuth debe estar dentro del proveedor AuthContext');
  }
  return authContext;
};
