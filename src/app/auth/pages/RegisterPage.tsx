import { useSuperMutation } from '@vadiun/react-hooks';
import { Button, FormikTextInput } from '@vadiun/react-native-components';
import { useAuth } from 'app/auth/services/useAuth';
import { Field, Formik } from 'formik';
import React from 'react';
import { KeyboardAvoidingView, Platform, ScrollView, View } from 'react-native';
import * as Yup from 'yup';

const Schema = Yup.object().shape({
  name: Yup.string().required('El nombre es requerido'),
  email: Yup.string().required('El email es requerido').required('El email es inválido'),
  password: Yup.string()
    .required('La contraseña es requerida')
    .min(4, 'La contraseña debe tener como minimo 4 caracteres'),
  passwordRepeat: Yup.string()
    .required('La contraseña es requerida')
    .min(4, 'La contraseña debe tener como mínimo 4 caracteres')
    .test(
      'match',
      'Las contraseñas no coinciden',
      (value, context) => context.parent.password === value
    )
});

const RegisterPage = () => {
  const { register } = useAuth();
  const registerMutation = useSuperMutation(register, {
    showSpinner: false,
    successMessageType: 'message',
    messages: {
      success: {
        title: 'Usuario creado',
        message:
          'Su usuario fue creado. Podrá ingresar al sistema con su email y contraseña'
      }
    }
  });

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{ flex: 1 }}
    >
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          padding: 20
        }}
      >
        <Formik
          initialValues={{
            password: '',
            passwordRepeat: '',
            name: '',
            email: ''
          }}
          validationSchema={Schema}
          onSubmit={registerMutation.mutate}
        >
          {({ submitForm }) => (
            <View style={{ flex: 1 }}>
              <Field
                name="name"
                label="Nombre"
                mode="flat"
                component={FormikTextInput}
              />
              <Field
                name="email"
                label="Email"
                mode="flat"
                component={FormikTextInput}
              />
              <Field
                name="password"
                label="Contraseña"
                mode="flat"
                secureTextEntry
                component={FormikTextInput}
              />
              <Field
                name="passwordRepeat"
                label="Repita la contraseña"
                mode="flat"
                secureTextEntry
                component={FormikTextInput}
              />

              <View style={{ marginTop: 'auto' }}>
                <Button
                  onPress={submitForm}
                  mode="contained"
                  style={{ padding: 5 }}
                  loading={registerMutation.isLoading}
                  disabled={registerMutation.isLoading}
                >
                  Registrar
                </Button>
              </View>
            </View>
          )}
        </Formik>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default RegisterPage;
