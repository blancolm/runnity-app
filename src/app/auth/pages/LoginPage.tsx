import { Link } from '@react-navigation/native';
import { useMutation } from '@vadiun/react-hooks';
import { Button, FormikTextInput } from '@vadiun/react-native-components';
import { useAuth } from 'app/auth/services/useAuth';
import { Field, Formik } from 'formik';
import React from 'react';
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  Platform,
  ScrollView
} from 'react-native';
import { Paragraph, Text } from 'react-native-paper';
import CustomColors from 'app/shared/helpers/CustomColors';
import * as Yup from 'yup';

const Schema = Yup.object().shape({
  email: Yup.string()
    .email('El email es invalido')
    .required('El email es requerido'),
  password: Yup.string()
    .required('La contraseña es requerida')
    .min(4, 'La contraseña debe tener como minimo 4 caracteres')
});
const LoginPage = () => {
  const { login } = useAuth();
  const loginMutation = useMutation(login);

  const initialValues = {
    email: '',
    password: ''
  };

  async function submit(values: { email: string; password: string }) {
    loginMutation.mutate(values);
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{ flex: 1 }}
    >
      <ScrollView contentContainerStyle={styles.container}>
        <Text style={styles.title}>Runnity</Text>
        <View style={styles.welcome}>
          <Text style={styles.welcomeTitle}>Bienvenido</Text>
          <Text style={styles.welcomeSubtitle}>Ingrese con su cuenta</Text>
        </View>
        <Paragraph>{loginMutation.error?.message}</Paragraph>
        <Formik
          initialValues={initialValues}
          validationSchema={Schema}
          onSubmit={submit}
        >
          {({ submitForm }) => (
            <View style={{ flex: 1 }}>
              <Field
                name="email"
                label="Email"
                mode="flat"
                component={FormikTextInput}
                keyboardType="email-address"
              />
              <Field
                name="password"
                label="Contraseña"
                mode="flat"
                secureTextEntry
                component={FormikTextInput}
              />

              {/* <Link to="/forgotPassword" style={{ marginLeft: 'auto' }}>
                <Text style={{ color: CustomColors.primary }}>
                  Olvidé mi contraseña
                </Text>
              </Link> */}
              <View style={styles.buttons}>
                <Button
                  onPress={submitForm}
                  mode="contained"
                  style={{ padding: 5 }}
                  loading={loginMutation.isLoading}
                  disabled={loginMutation.isLoading}
                >
                  Login
                </Button>

                <Paragraph
                  style={[
                    styles.softText,
                    { textAlign: 'center', marginTop: 20 }
                  ]}
                >
                  ¿Aún no tienes una cuenta?{' '}
                  <Link to="/register">
                    {' '}
                    <Text style={{ color: CustomColors.primary }}>
                      Registrate aqui
                    </Text>
                  </Link>
                </Paragraph>
              </View>
            </View>
          )}
        </Formik>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default LoginPage;

const styles = StyleSheet.create({
  container: {
    padding: 20,
    flexGrow: 1
  },
  title: {
    fontFamily: 'poppins_bold',
    fontSize: 36,
    marginTop: 20,
    textAlign: 'center'
  },
  buttons: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  welcomeTitle: { fontSize: 26 },
  welcomeSubtitle: {
    marginTop: 50,
    fontSize: 18
  },
  welcome: {
    alignItems: 'center'
  },
  softText: {}
});
