import { useSuperMutation } from '@vadiun/react-hooks';
import { useAuth } from 'app/auth/services/useAuth';
import { Field, Formik } from 'formik';
import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Platform
} from 'react-native';
import { Paragraph } from 'react-native-paper';
import * as Yup from 'yup';
import { Button, FormikTextInput } from '@vadiun/react-native-components';
import CustomColors from 'app/shared/helpers/CustomColors';

const Logo = require('../../../assets/MARCA.png');

const Schema = Yup.object().shape({
  email: Yup.string()
    .email('El email es invalido')
    .required('El email es requerido')
});

const ForgotPasswordPage = () => {
  const { forgotPassword } = useAuth();

  const forgotPasswordMutations = useSuperMutation(forgotPassword, {
    showSpinner: false,
    successMessageType: 'message',
    messages: {
      success: {
        message:
          'Se ha enviado un mail a su casilla de correo con las instrucciones.'
      }
    }
  });
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{ flex: 1 }}
    >
      <ScrollView contentContainerStyle={styles.container}>
        <View style={{ alignItems: 'center' }}>
          <Image source={Logo} style={styles.logo} />
        </View>
        <Paragraph style={styles.welcomeSubtitle}>
          Ingrese con su email y le enviaremos las instrucciones para modificar
          su contraseña
        </Paragraph>
        <Paragraph>{forgotPasswordMutations.error?.message}</Paragraph>
        <Formik
          initialValues={{ email: '' }}
          validationSchema={Schema}
          onSubmit={forgotPasswordMutations.mutate}
        >
          {({ submitForm }) => (
            <View style={{ flex: 1 }}>
              <Field
                name="email"
                label="Email"
                mode="flat"
                component={FormikTextInput}
                keyboardType="email-address"
              />

              <View style={{ marginTop: 'auto' }}>
                <Button
                  onPress={submitForm}
                  mode="contained"
                  style={{ padding: 5 }}
                  loading={forgotPasswordMutations.isLoading}
                  disabled={forgotPasswordMutations.isLoading}
                >
                  Continuar
                </Button>
              </View>
            </View>
          )}
        </Formik>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default ForgotPasswordPage;

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: CustomColors.white,
    padding: 20
  },
  logo: { width: 200, height: 200 },
  welcomeSubtitle: { color: CustomColors.gray500, fontSize: 16 }
});
