import { httpClient } from 'app/shared/services/http/httpClient';
import { trainingBuilder } from '../models/TrainingModel';

export const useTrainingRepository = () => {
  const saveResults = (measures: { distance: number; seconds: number }) =>
    httpClient.post('runners/me/trainings', {
      distance: measures.distance,
      seconds: measures.seconds
    });

  const getLoggedRunnerResults = async () => {
    const res = await httpClient.get(`runners/me/trainings`);
    return res.map(trainingBuilder.fromBackEnd);
  };

  return {
    saveResults,
    getLoggedRunnerResults
  };
};
