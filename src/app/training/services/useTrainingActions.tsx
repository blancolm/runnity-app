import { useTrainingRepository } from './useTrainingRepository';

export const useTrainingActions = () => {
  const trainingRepo = useTrainingRepository();

  return {
    ...trainingRepo
  };
};
