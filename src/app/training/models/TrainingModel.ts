import moment, { Moment } from 'moment';

export interface TrainingModel {
  id: number;
  runner_id: number;
  datetime: Moment;
  distance: number;
  seconds: number;
}

export const trainingBuilder = {
  fromBackEnd: (t: any) => ({
    ...t,
    datetime: moment(t.datetime)
  })
};
