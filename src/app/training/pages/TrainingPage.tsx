import React from 'react';
import { Dimensions, StyleSheet, View } from 'react-native';
import { Marker } from 'react-native-maps';
import CustomBottomSheet from 'app/shared/components/CustomBottomSheet';
import useRunningMeasures, {
  RunningStatus
} from 'app/runningMeasures/hooks/useRunningMeasures';
import { Text } from 'react-native-paper';
import { Button } from '@vadiun/react-native-components';
import { TouchableWithoutFeedback } from '@gorhom/bottom-sheet';
import useBackgroundLocation from 'app/shared/hooks/useBackgroundLocation';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import Map from 'app/shared/components/Map/Map';
import RunningMeasures from 'app/runningMeasures/components/RunningMeasures';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import { useSuperMutation } from '@vadiun/react-hooks';
import { useTrainingActions } from '../services/useTrainingActions';
import { TrainingStackNavigationType } from '../navigation/TrainingNavigationType';

const MarketIcon = require('assets/running.png');

const initialCameraPosition = {
  latitude: -34.60802433133401,
  longitude: -58.37039256425254
};

const TrainingPage = () => {
  const trainingActions = useTrainingActions();
  const runner = useLoggedRunner();
  const location = useBackgroundLocation();
  const navigation =
    useNavigation<StackNavigationProp<TrainingStackNavigationType, 'main'>>();
  const runningMeasures = useRunningMeasures();

  const saveTrainingMutation = useSuperMutation(trainingActions.saveResults, {
    showSpinner: false,
    showSuccessMessage: false
  });

  const cameraPosition = location.location
    ? {
        latitude: location.location.lat,
        longitude: location.location.lng
      }
    : initialCameraPosition;

  const snapPoints = [
    3,
    runningMeasures.status === RunningStatus.RUNNING ? 320 : 250
  ];

  const stopTraining = async () => {
    runningMeasures.stop();
    await saveTrainingMutation.mutate(runningMeasures);
    navigation.navigate('trainingFinished', runningMeasures);
  };

  return (
    <>
      <Map
        style={styles.map}
        camera={{
          center: {
            latitude: cameraPosition.latitude - 0.0007,
            longitude: cameraPosition.longitude
          },
          zoom: 18,
          heading: 0,
          pitch: 0,
          altitude: 0
        }}
      >
        {location.location && (
          <Marker
            coordinate={{
              latitude: location.location.lat,
              longitude: location.location.lng
            }}
            icon={MarketIcon}
          />
        )}
      </Map>
      <CustomBottomSheet index={1} snapPoints={snapPoints}>
        {runningMeasures.status === RunningStatus.IDLE ? (
          <View
            style={{
              padding: 30,
              flex: 1
            }}
          >
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'poppins_bold',
                fontSize: 20
              }}
            >
              Hola {runner.runner?.name}
            </Text>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'poppins_bold',
                fontSize: 16
              }}
            >
              ¡Bienvenido de vuelta!
            </Text>
            <View style={{ marginTop: 'auto' }}>
              <TouchableWithoutFeedback onPress={runningMeasures.start}>
                <Button onPress={() => {}} mode="contained">
                  Comenzar
                </Button>
              </TouchableWithoutFeedback>
            </View>
          </View>
        ) : (
          <View style={{ flex: 1, paddingVertical: 10, paddingHorizontal: 30 }}>
            <RunningMeasures
              duration={runningMeasures.seconds}
              distance={runningMeasures.distance}
              mediumPace={runningMeasures.mediumPace}
              calories={runningMeasures.calories}
            />
            <TouchableWithoutFeedback onPress={stopTraining}>
              <Button
                onPress={() => {}}
                mode="contained"
                loading={saveTrainingMutation.isLoading}
                disabled={saveTrainingMutation.isLoading}
              >
                Parar
              </Button>
            </TouchableWithoutFeedback>
          </View>
        )}
      </CustomBottomSheet>
    </>
  );
};

export default TrainingPage;

const styles = StyleSheet.create({
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height
  }
});
