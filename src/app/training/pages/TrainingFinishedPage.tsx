import { MaterialCommunityIcons } from '@expo/vector-icons';
import { RouteProp, useRoute } from '@react-navigation/native';
import RunningMeasures from 'app/runningMeasures/components/RunningMeasures';
import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { StyleSheet } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Surface, Text } from 'react-native-paper';
import { TrainingStackNavigationType } from '../navigation/TrainingNavigationType';

const TrainingFinishedPage = () => {
  const route =
    useRoute<RouteProp<TrainingStackNavigationType, 'trainingFinished'>>();

  const { distance, seconds, mediumPace, calories } = route.params;
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <MaterialCommunityIcons name="arm-flex" size={60} color="white" />
      <Text style={styles.title}>¡Excelente!</Text>
      <Text style={styles.subtitle}>Estos fueron tus resultados</Text>
      <Surface style={styles.body}>
        <RunningMeasures
          distance={distance}
          duration={seconds}
          mediumPace={mediumPace}
          calories={calories}
        />
      </Surface>
    </ScrollView>
  );
};

export default TrainingFinishedPage;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    marginTop: 30,
    flex: 1
  },
  title: {
    fontFamily: 'poppins_bold',
    fontSize: 28
  },
  subtitle: {
    fontSize: 20,
    marginTop: 10
  },
  body: {
    backgroundColor: CustomColors.dark600,
    padding: 20,
    borderRadius: 20,
    margin: 20
  }
});
