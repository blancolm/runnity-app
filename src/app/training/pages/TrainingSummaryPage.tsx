import { useSuperQuery } from '@vadiun/react-hooks';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { Divider } from 'react-native-paper';
import { TrainingListItem } from '../components/TrainingListItem';
import { TrainingModel } from '../models/TrainingModel';
import { useTrainingActions } from '../services/useTrainingActions';

export const TrainingSummaryPage = () => {
  const trainingActions = useTrainingActions();
  const trainingQuery = useSuperQuery(trainingActions.getLoggedRunnerResults, {
    showSpinner: true
  });
  return (
    <View style={styles.container}>
      <FlatList
        data={trainingQuery.data ?? []}
        keyExtractor={(item: TrainingModel) => String(item.id)}
        renderItem={({ item }) => <TrainingListItem training={item} />}
        ItemSeparatorComponent={Divider}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
