import { Button } from '@vadiun/react-native-components';
import CustomColors from 'app/shared/helpers/CustomColors';
import useBackgroundLocation from 'app/shared/hooks/useBackgroundLocation';

import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

const RequestLocationPage = () => {
  const { requestPermissions } = useBackgroundLocation();
  return (
    <SafeAreaView style={styles.container}>
      <Text
        style={{
          color: CustomColors.primaryText,
          fontSize: 18,
          textAlign: 'center'
        }}
      >
        Necesitamos su posición para poder medir sus entrenamientos
      </Text>
      <Text style={{ color: CustomColors.secondaryText, marginVertical: 20 }}>
        Por favor habilite los permisos de localización para continuar
      </Text>
      <Button
        onPress={requestPermissions}
        mode="contained"
        style={{ marginTop: 'auto' }}
      >
        Habilitar
      </Button>
    </SafeAreaView>
  );
};

export default RequestLocationPage;

const styles = StyleSheet.create({
  container: { flex: 1, margin: 20 }
});
