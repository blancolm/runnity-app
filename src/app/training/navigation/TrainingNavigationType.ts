import ChallengeModel from 'app/challenge/models/ChallengeModel';
import { RunningMeasure } from 'app/runningMeasures/models/RunningMeasure';

export type TrainingStackNavigationType = {
  challengeDetails: { challenge: ChallengeModel };
  main: undefined;
  trainingFinished: RunningMeasure;
};

export type TrainingNavigationTabsType = {
  training: undefined;
  challenges: undefined;
  requestPermission: undefined;
};
