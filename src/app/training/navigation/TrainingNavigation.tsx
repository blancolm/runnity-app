import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import CustomColors from 'app/shared/helpers/CustomColors';
import useBackgroundLocation from 'app/shared/hooks/useBackgroundLocation';
import { createStackNavigator } from '@react-navigation/stack';
import ChallengeDetailsPage from 'app/challenge/pages/ChallengeDetailsPage';
import ChallengePage from 'app/challenge/pages/ChallengePage';
import { SafeAreaView } from 'react-native-safe-area-context';
import RequestLocationPage from '../pages/RequestLocationPage';
import TrainingPage from '../pages/TrainingPage';
import TrainingFinishedPage from '../pages/TrainingFinishedPage';
import {
  TrainingNavigationTabsType,
  TrainingStackNavigationType
} from './TrainingNavigationType';

const Stack = createStackNavigator<TrainingStackNavigationType>();

const TrainingStackNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="main"
      options={{
        headerShown: false
      }}
      component={TrainingTabsNavigation}
    />
    <Stack.Screen
      name="challengeDetails"
      options={{
        title: 'Detalle desafío'
      }}
      component={ChallengeDetailsPage}
    />
    <Stack.Screen
      name="trainingFinished"
      options={{
        title: 'Resultados'
      }}
      component={TrainingFinishedPage}
    />
  </Stack.Navigator>
);

const Tab = createMaterialTopTabNavigator<TrainingNavigationTabsType>();

const TrainingTabsNavigation = () => {
  const { permissionStatus } = useBackgroundLocation();

  if (
    permissionStatus.foreground !== 'granted' ||
    permissionStatus.background !== 'granted'
  ) {
    return <RequestLocationPage />;
  }
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Tab.Navigator
        tabBarOptions={{
          activeTintColor: CustomColors.primary,
          pressColor: CustomColors.gray100,
          indicatorStyle: {
            backgroundColor: CustomColors.primary
          }
        }}
      >
        <Tab.Screen
          name="training"
          options={{ title: 'Entrenamiento' }}
          component={TrainingPage}
        />
        <Tab.Screen
          name="challenges"
          options={{ title: 'Desafíos' }}
          component={ChallengePage}
        />
      </Tab.Navigator>
    </SafeAreaView>
  );
};

export default TrainingStackNavigation;
