import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import caloriesCalculator from 'app/runningMeasures/hooks/caloriesCalculator';
import measureFormatter from 'app/runningMeasures/hooks/measureFormatter';
import mediumPaceCalculator from 'app/runningMeasures/hooks/mediumPaceCalculator';
import CustomColors from 'app/shared/helpers/CustomColors';
import moment from 'moment';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-paper';
import { TrainingModel } from '../models/TrainingModel';

interface Props {
  training: TrainingModel;
}

export const TrainingListItem = ({ training }: Props) => {
  const loggedRunner = useLoggedRunner();
  const mediumPace = mediumPaceCalculator.calculate(
    training.distance,
    training.seconds
  );
  const calories = caloriesCalculator.calculate({
    minutesPerKm: mediumPace,
    time: training.seconds,
    weight: loggedRunner.runner!.weight
  });

  const measures = measureFormatter.format({
    distance: training.distance,
    seconds: training.seconds,
    mediumPace,
    calories
  });

  return (
    <View style={styles.container}>
      <View style={styles.avatar}>
        <Text style={{ fontFamily: 'poppins_bold', fontSize: 18 }}>
          {measures.distance}
        </Text>
        <Text>km.</Text>
      </View>
      <View style={{ marginLeft: 20, flex: 1 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.measureLabel}>
            Duracion:{' '}
            <Text style={styles.measure}>{measures.duration} hs.</Text>
          </Text>
          <Text style={styles.date}>
            {training.datetime.format('DD/MM/YYYY')}
          </Text>
        </View>
        <Text style={styles.measureLabel}>
          Calorías: <Text style={styles.measure}>{measures.calories}</Text>
        </Text>
        <Text style={styles.measureLabel}>
          Ritmo medio:{' '}
          <Text style={styles.measure}>{measures.mediumPace} min/km</Text>
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  date: {
    fontSize: 10,
    marginLeft: 'auto',
    color: CustomColors.secondaryText
  },
  measureLabel: {
    color: CustomColors.secondaryText
  },
  measure: {
    fontSize: 16
  },
  container: {
    flexDirection: 'row',
    padding: 20,
    alignItems: 'flex-start'
  },
  avatar: {
    backgroundColor: CustomColors.primary500,
    padding: 10,
    width: 80,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20
  }
});
