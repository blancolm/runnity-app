import React, { useState } from 'react';
import { View } from 'react-native';
import { LineChart } from 'react-native-chart-kit';
import { takeRight } from 'lodash';
import { LineChartData } from 'react-native-chart-kit/dist/line-chart/LineChart';
import { TrainingModel } from '../models/TrainingModel';
import CustomColors from 'app/shared/helpers/CustomColors';

const chartConfig = {
  backgroundGradientFrom: '#1E2923',
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: '#08130D',
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(176, 179, 184,  ${opacity})`,
  strokeWidth: 2, // optional, default 3
  barPercentage: 0.5,
  useShadowColorFromDataset: false // optional
};

interface Props {
  trainings: TrainingModel[];
}
const TrainingChart = ({ trainings }: Props) => {
  const [width, setWidth] = useState(0);
  if (trainings.length === 0) {
    return null;
  }
  const last7 = trainings.length > 7 ? takeRight(trainings, 7) : trainings;
  const data: LineChartData = {
    labels: last7.map((t) => t.datetime.format('DD/MM')),
    datasets: [
      {
        data: last7.map((t) => Number(t.distance) / 1000),
        color: () => CustomColors.primary, // optional
        strokeWidth: 2 // optional
      }
    ]
  };
  return (
    <View onLayout={(ev) => setWidth(ev.nativeEvent.layout.width)}>
      <LineChart
        data={data}
        width={width}
        height={220}
        chartConfig={chartConfig}
      />
    </View>
  );
};

export default TrainingChart;
