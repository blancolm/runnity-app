import httpClient from 'app/shared/services/http/httpClient';
import ChallengeModel, { challengeBuilder } from '../models/ChallengeModel';

const useChallengeRepository = () => {
  const getAll = async (): Promise<ChallengeModel[]> => {
    const res = await httpClient.get('challenges');
    return res.map(challengeBuilder.fromBackEnd);
  };

  return {
    getAll
  };
};

export default useChallengeRepository;
