interface ChallengeModel {
  id: number;
  photo: string;
  name: string;
  description: string;
}

export default ChallengeModel;

export const challengeBuilder = {
  fromBackEnd: (challenge: any): ChallengeModel => ({
    ...challenge
  })
};
