import LoadingPlaceholder from 'app/shared/components/LoadingPlaceholder';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Surface } from 'react-native-paper';

const ChallengeCardLoading = () => (
  <Surface style={styles.container}>
    <LoadingPlaceholder style={styles.image} />
    <View style={styles.cardBody}>
      <LoadingPlaceholder style={{ height: 25, width: 150 }} />
      <View style={{ marginTop: 10 }}>
        <LoadingPlaceholder style={{ height: 15, width: 200, marginTop: 5 }} />
        <LoadingPlaceholder style={{ height: 15, width: 200, marginTop: 5 }} />
        <LoadingPlaceholder style={{ height: 15, width: 200, marginTop: 5 }} />
      </View>
    </View>
  </Surface>
);

export default ChallengeCardLoading;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    overflow: 'hidden',
    borderRadius: 20
  },
  image: {
    width: '25%',
    height: '100%',
    borderRadius: 0
  },
  cardBody: { padding: 20, flex: 1 }
});
