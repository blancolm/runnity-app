import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Surface, Text } from 'react-native-paper';
import ChallengeModel from '../models/ChallengeModel';

interface Props {
  challenge: ChallengeModel;
}

const ChallengeCard = ({ challenge }: Props) => (
  <Surface style={styles.container}>
    <Image
      source={{ uri: challenge.photo }}
      resizeMode="cover"
      style={styles.image}
    />
    <View style={styles.cardBody}>
      <Text style={styles.title}>{challenge.name}</Text>
      <Text style={styles.description} numberOfLines={4}>
        {challenge.description}
      </Text>
    </View>
  </Surface>
);

export default ChallengeCard;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    overflow: 'hidden',
    borderRadius: 20
  },
  image: {
    width: '25%',
    height: '100%'
  },
  title: {
    fontFamily: 'poppins_bold',
    fontSize: 16
  },
  description: {
    color: CustomColors.secondaryText
  },
  cardBody: { padding: 20, flex: 1 }
});
