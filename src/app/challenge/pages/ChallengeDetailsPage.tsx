import { RouteProp, useRoute } from '@react-navigation/native';
import DetailPage from 'app/shared/components/DetailPage';
import CustomColors from 'app/shared/helpers/CustomColors';
import { TrainingStackNavigationType } from 'app/training/navigation/TrainingNavigationType';
import React from 'react';
import { StyleSheet } from 'react-native';
import { Text } from 'react-native-paper';

const ChallengeDetailsPage = () => {
  const route =
    useRoute<RouteProp<TrainingStackNavigationType, 'challengeDetails'>>();
  const { challenge } = route.params;
  return (
    <DetailPage imageSrc={{ uri: challenge.photo }}>
      <Text style={styles.title}>{challenge.name}</Text>
      <Text style={styles.descriptionLabel}>Descripción</Text>
      <Text style={styles.description}>{challenge.description}</Text>
    </DetailPage>
  );
};

export default ChallengeDetailsPage;

const styles = StyleSheet.create({
  title: {
    fontFamily: 'poppins_bold',
    fontSize: 20,
    textAlign: 'center'
  },
  descriptionLabel: {
    fontFamily: 'poppins_bold',
    fontSize: 16,
    marginVertical: 10
  },
  description: {
    color: CustomColors.secondaryText
  }
});
