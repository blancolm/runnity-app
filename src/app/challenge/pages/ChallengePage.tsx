import { TouchableOpacity } from '@gorhom/bottom-sheet';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useSuperQuery } from '@vadiun/react-hooks';
import { TrainingStackNavigationType } from 'app/training/navigation/TrainingNavigationType';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import ChallengeCard from '../components/ChallengeCard';
import ChallengeCardLoading from '../components/ChallengeCardLoading';
import useChallengeRepository from '../services/useChallengeRepository';

const ChallengePage = () => {
  const challengeService = useChallengeRepository();
  const challengesQuery = useSuperQuery(challengeService.getAll);
  const navigation =
    useNavigation<StackNavigationProp<TrainingStackNavigationType, 'main'>>();

  if (!challengesQuery.data) {
    return (
      <ScrollView style={styles.container}>
        <View style={styles.cardContainer}>
          <ChallengeCardLoading />
        </View>
        <View style={styles.cardContainer}>
          <ChallengeCardLoading />
        </View>
        <View style={styles.cardContainer}>
          <ChallengeCardLoading />
        </View>
      </ScrollView>
    );
  }
  return (
    <ScrollView style={styles.container}>
      {challengesQuery.data.map((challenge) => (
        <View style={styles.cardContainer} key={challenge.id}>
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() =>
              navigation.navigate('challengeDetails', { challenge })
            }
          >
            <ChallengeCard challenge={challenge} />
          </TouchableOpacity>
        </View>
      ))}
    </ScrollView>
  );
};

export default ChallengePage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  },
  cardContainer: { marginVertical: 10, borderRadius: 20, overflow: 'hidden' }
});
