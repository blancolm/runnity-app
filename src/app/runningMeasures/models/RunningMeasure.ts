export interface RunningMeasure {
  seconds: number;
  distance: number;
  mediumPace: number;
  calories: number;
}
