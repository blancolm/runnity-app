const calculateCaloriesBurned = ({
  weight,
  time,
  minutesPerKm
}: {
  weight: number; // kilograms
  time: number; // seconds
  minutesPerKm: number;
}) => (time / 60) * getAproximateMet(minutesPerKm) * weight * 0.0175;

const getAproximateMet = (minutesPerKm: number): number => {
  const seed = { minutesPerKm: 12, met: 0 };
  return MET_TABLE.reduce((a, b) => {
    const differenceA = Math.abs(a.minutesPerKm - minutesPerKm);
    const differenceB = Math.abs(b.minutesPerKm - minutesPerKm);
    return differenceA < differenceB ? a : b;
  }, seed).met;
};

const MET_TABLE = [
  {
    minutesPerKm: 9.32057,
    met: 6
  },
  {
    minutesPerKm: 7.45645,
    met: 8.3
  },
  {
    minutesPerKm: 7.145769,
    met: 9
  },
  {
    minutesPerKm: 6.21371,
    met: 9.8
  },
  {
    minutesPerKm: 5.59234,
    met: 10.5
  },
  {
    minutesPerKm: 5.28166,
    met: 11
  },
  {
    minutesPerKm: 4.97097,
    met: 11.8
  },
  {
    minutesPerKm: 4.66028,
    met: 12
  },
  {
    minutesPerKm: 4.3496,
    met: 12.3
  },
  {
    minutesPerKm: 4.03891,
    met: 12.8
  },
  {
    minutesPerKm: 3.72823,
    met: 14.5
  },
  {
    minutesPerKm: 3.41754,
    met: 16
  },
  {
    minutesPerKm: 3.10686,
    met: 19
  },
  {
    minutesPerKm: 2.85831,
    met: 19.8
  },
  {
    minutesPerKm: 2.6719,
    met: 23
  }
];

const caloriesCalculator = {
  calculate: calculateCaloriesBurned
};

export default caloriesCalculator;
