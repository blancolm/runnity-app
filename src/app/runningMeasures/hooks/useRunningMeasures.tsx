import { useEffect, useRef, useState } from 'react';
import { AppState, AppStateStatus } from 'react-native';
import dayjs, { Dayjs } from 'dayjs';
import useBackgroundLocation from 'app/shared/hooks/useBackgroundLocation';
import haversine from 'haversine-distance';
import caloriesCalculator from './caloriesCalculator';
import mediumPaceCalculator from './mediumPaceCalculator';

export enum RunningStatus {
  RUNNING = 'running',
  IDLE = 'idle'
}

const useRunningMeasures = () => {
  const [status, setStatus] = useState<RunningStatus>(RunningStatus.IDLE);
  const lastTimeActive = useRef<Dayjs | null>(null);
  const [seconds, setSeconds] = useState(0);
  const [distance, setDistance] = useState(0);
  const interval = useRef<NodeJS.Timeout | null>(null);
  const prevPosition = useRef<{ lat: number; lng: number } | null>(null);
  const { location } = useBackgroundLocation();
  const mediumPace = mediumPaceCalculator.calculate(distance, seconds);
  const calories = caloriesCalculator.calculate({
    minutesPerKm: mediumPace,
    time: seconds,
    weight: 70
  });

  function start() {
    setStatus(RunningStatus.RUNNING);
  }

  function stop() {
    setStatus(RunningStatus.IDLE);
  }

  // Add the time that the app was on background
  useEffect(() => {
    const handleAppStateChange = (nextAppState: AppStateStatus) => {
      if (nextAppState === 'background' || nextAppState === 'inactive') {
        lastTimeActive.current = dayjs();
      }
      if (nextAppState === 'active' && status === RunningStatus.RUNNING) {
        setSeconds(
          (_seconds) =>
            _seconds + dayjs().diff(lastTimeActive.current!, 'seconds')
        );
      }
    };
    AppState.addEventListener('change', handleAppStateChange);
    return () => {
      AppState.removeEventListener('change', handleAppStateChange);
    };
  }, [status]);

  // Create the timer
  useEffect(() => {
    if (status === RunningStatus.RUNNING) {
      interval.current = setInterval(() => {
        setSeconds((x) => x + 1);
      }, 1000);
    } else {
      interval.current && clearInterval(interval.current);
    }
  }, [status]);

  // Calculate the distance
  useEffect(() => {
    if (!location) return;
    if (status !== RunningStatus.RUNNING) return;
    if (prevPosition.current !== null) {
      const newDistance = haversine(location, prevPosition.current);
      setDistance((_distance) => _distance + newDistance);
    }
    prevPosition.current = location;
  }, [location, status]);

  // Clean state
  useEffect(() => {
    if (status === RunningStatus.IDLE) {
      prevPosition.current = null;
      setSeconds(0);
      setDistance(0);
    }
  }, [status]);

  return {
    status,
    start,
    stop,
    seconds,
    distance,
    mediumPace,
    calories
  };
};

export default useRunningMeasures;
