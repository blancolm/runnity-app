import dayjs from 'dayjs';
import { RunningMeasure } from '../models/RunningMeasure';

const formatDuration = (seconds: number) =>
  dayjs('2015-01-01').startOf('day').second(seconds).format('H:mm:ss');

const formatDistance = (distance: number) =>
  Number((distance / 1000).toFixed(2));

const formatCalories = (calories: number) => Number(calories.toFixed(2));

const formatMediumPace = (mediumPace: number) => Number(mediumPace.toFixed(2));

const measureFormatter = {
  format: (measure: RunningMeasure) => ({
    duration: formatDuration(measure.seconds),
    distance: formatDistance(measure.distance),
    calories: formatCalories(measure.calories),
    mediumPace: formatMediumPace(measure.mediumPace)
  })
};

export default measureFormatter;
