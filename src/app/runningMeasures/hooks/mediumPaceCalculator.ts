const calculateMediumPace = (distance: number, seconds: number) =>
  distance === 0 ? 0 : seconds / 60 / (distance / 1000); // min per km

const mediumPaceCalculator = { calculate: calculateMediumPace };

export default mediumPaceCalculator;
