import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-paper';
import measureFormatter from '../hooks/measureFormatter';

interface Props {
  duration: number;
  distance: number;
  mediumPace: number;
  calories: number;
}

const RunningMeasures = (props: Props) => {
  const formattedMeasures = measureFormatter.format({
    seconds: props.duration,
    distance: props.distance,
    mediumPace: props.mediumPace,
    calories: props.calories
  });
  return (
    <View>
      <View>
        <Text style={styles.mainMetricValue}>{formattedMeasures.duration}</Text>
        <Text style={styles.mainMetricLabel}>Duración</Text>
      </View>
      <View>
        <Text style={styles.mainMetricValue}>{formattedMeasures.distance}</Text>
        <Text style={styles.mainMetricLabel}>Distancia (km)</Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 20
        }}
      >
        <View style={{ width: '50%' }}>
          <Text style={styles.secondaryMetricValue}>
            {formattedMeasures.mediumPace}
          </Text>
          <Text style={styles.secondaryMetricLabel}>Ritmo medio (min/km)</Text>
        </View>
        <View style={{ width: '50%' }}>
          <Text style={styles.secondaryMetricValue}>
            {formattedMeasures.calories}
          </Text>
          <Text style={styles.secondaryMetricLabel}>Calorías</Text>
        </View>
      </View>
    </View>
  );
};

export default RunningMeasures;

const styles = StyleSheet.create({
  mainMetricValue: {
    fontFamily: 'poppins_bold',
    fontSize: 32,
    textAlign: 'center'
  },
  mainMetricLabel: {
    fontSize: 12,
    color: CustomColors.secondaryText,
    textAlign: 'center'
  },
  secondaryMetricValue: {
    fontFamily: 'poppins_bold',
    fontSize: 24,
    textAlign: 'center'
  },
  secondaryMetricLabel: {
    fontSize: 12,
    color: CustomColors.secondaryText,
    textAlign: 'center'
  }
});
