import React from 'react';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import CustomColors from 'app/shared/helpers/CustomColors';
import TrainingNavigation from 'app/training/navigation/TrainingNavigation';
import CompetitionNavigation from 'app/competition/main/navigation/CompetitionNavigation';
import RunningTeamNavigation from 'app/runningTeam/navigation/RunningTeamNavigation';
import { ProfileStackNavigation } from 'app/runner/navigation/ProfileNavigation';

export type RootNavigationType = {
  training: undefined;
  competition: undefined;
  runningTeam: undefined;
  profile: undefined;
};

const Tab = createMaterialBottomTabNavigator<RootNavigationType>();

const BottomTabsNavigation = () => (
  <Tab.Navigator activeColor={CustomColors.primary} shifting>
    <Tab.Screen
      name="training"
      component={TrainingNavigation}
      options={{ tabBarLabel: 'Entrenamiento', tabBarIcon: 'arm-flex' }}
    />
    <Tab.Screen
      name="competition"
      component={CompetitionNavigation}
      options={{ tabBarLabel: 'Competencias', tabBarIcon: 'run' }}
    />
    <Tab.Screen
      name="runningTeam"
      component={RunningTeamNavigation}
      options={{ tabBarLabel: 'Grupos', tabBarIcon: 'account-group' }}
    />
    <Tab.Screen
      name="profile"
      component={ProfileStackNavigation}
      options={{ tabBarLabel: 'Perfil', tabBarIcon: 'account-circle' }}
    />
  </Tab.Navigator>
);

export default BottomTabsNavigation;
