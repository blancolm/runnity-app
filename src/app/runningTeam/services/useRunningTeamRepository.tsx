import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import { jsonToFormData } from 'app/shared/helpers/jsonToFormData';
import { httpClient } from 'app/shared/services/http/httpClient';
import RunningTeamModel, {
  runningTeamBuilder,
  RunningTeamCrud
} from '../models/RunningTeam';

const useRunningTeamRepository = () => {
  const loggedRunner = useLoggedRunner();

  const getAll = async (): Promise<RunningTeamModel[]> => {
    const res = await httpClient.get('groups');
    return res.map(runningTeamBuilder.fromBackEnd);
  };

  const getLoggedUserRunningTeam = async () => {
    const res = await httpClient.get('groups');
    const mapped: RunningTeamModel[] = res.map(runningTeamBuilder.fromBackEnd);
    const team = mapped.find((x) => x.id === loggedRunner.runner?.group_id);
    return team ?? null;
  };

  const get = async (id: number): Promise<RunningTeamModel> => {
    const res = await httpClient.get('groups');
    const mapped: RunningTeamModel[] = res.map(runningTeamBuilder.fromBackEnd);
    return mapped.find((x) => x.id === id)!;
  };

  const signIn = (idRunningTeam: number, password: string) =>
    httpClient.post(`groups/${idRunningTeam}/members`, {
      id: loggedRunner.runner?.id,
      password
    });

  const create = (team: RunningTeamCrud) =>
    httpClient.post(`runners/me/groups`, jsonToFormData(team));

  const edit = (team: RunningTeamCrud) => {
    // const { photo, ...restTeam } = team;
    // const toBack =
    //   typeof photo === 'string' ? restTeam : { photo, ...restTeam };
    return httpClient.post(
      `groups/${team.id}?_method=PUT`,
      jsonToFormData(team)
    );
  };

  const remove = (id: number) => httpClient.delete(`groups/${id}`);

  const removeMember = (teamId: number, memberId: number) =>
    httpClient.delete(`members/${memberId}`);

  return {
    getAll,
    signIn,
    get,
    create,
    edit,
    remove,
    removeMember,
    getLoggedUserRunningTeam
  };
};

export default useRunningTeamRepository;
