import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import CustomColors from 'app/shared/helpers/CustomColors';
import { SafeAreaView } from 'react-native-safe-area-context';
import React from 'react';
import RunningTeamSignIn from '../pages/RunningTeamSignIn';
import MyRunningTeamPage from '../pages/MyRunningTeamPage';
import RunningTeamSearchPage from '../pages/RunningTeamSearchPage';
import {
  RunningTeamNavigationTabsType,
  RunningTeamStackNavigationType
} from './RunningTeamNavigationTypes';
import RunningTeamCreation from '../pages/RunningTeamCreation';
import RunningTeamCreationSuccess from '../pages/RunningTeamCreationSuccess';
import RunningTeamEdit from '../pages/RunningTeamEdit';

const Stack = createStackNavigator<RunningTeamStackNavigationType>();

const TrainingStackNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="main"
      options={{
        headerShown: false
      }}
      component={RunningTeamTabsNavigation}
    />
    <Stack.Screen
      name="signIn"
      options={{
        title: 'Inscripción'
      }}
      component={RunningTeamSignIn}
    />
    <Stack.Screen
      name="create"
      options={{
        title: 'Creación'
      }}
      component={RunningTeamCreation}
    />
    <Stack.Screen
      name="edit"
      options={{
        title: 'Edición'
      }}
      component={RunningTeamEdit}
    />
    <Stack.Screen
      name="creationSuccess"
      options={{
        title: '',
        headerTransparent: true
      }}
      component={RunningTeamCreationSuccess}
    />
  </Stack.Navigator>
);

const Tab = createMaterialTopTabNavigator<RunningTeamNavigationTabsType>();

const RunningTeamTabsNavigation = () => (
  <SafeAreaView style={{ flex: 1 }}>
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: CustomColors.primary,
        pressColor: CustomColors.gray100,
        indicatorStyle: {
          backgroundColor: CustomColors.primary
        }
      }}
    >
      <Tab.Screen
        name="myTeam"
        options={{ title: 'Mi grupo' }}
        component={MyRunningTeamPage}
      />
      <Tab.Screen
        name="search"
        options={{ title: 'Buscar Grupo' }}
        component={RunningTeamSearchPage}
      />
    </Tab.Navigator>
  </SafeAreaView>
);

export default TrainingStackNavigation;
