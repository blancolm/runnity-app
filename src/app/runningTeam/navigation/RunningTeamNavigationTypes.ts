import { NavigatorScreenParams } from '@react-navigation/native';
import RunningTeamModel from '../models/RunningTeam';

export type RunningTeamStackNavigationType = {
  signIn: { runningTeam: RunningTeamModel };
  main: NavigatorScreenParams<RunningTeamNavigationTabsType>;
  create: undefined;
  edit: { runningTeam: RunningTeamModel };
  creationSuccess: undefined;
};

export type RunningTeamNavigationTabsType = {
  search: undefined;
  myTeam: undefined;
};
