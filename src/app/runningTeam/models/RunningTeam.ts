import RunnerModel, { runnerBuilder } from 'app/runner/models/Runner';
import { imageMapper } from 'app/shared/helpers/imageMapper';

interface RunningTeamModel {
  id: number;
  photo: string;
  name: string;
  members: RunnerModel[];
  owner: RunnerModel;
}

export interface RunningTeamCrud {
  id?: number;
  name: string;
  password?: string;
  passwordRepetition?: string;
  photo: string | { uri: string } | null;
}

export const runningTeamBuilder = {
  fromBackEnd: (team: any): RunningTeamModel => ({
    ...team,
    photo: imageMapper(team.photo),
    members: team.members.map(runnerBuilder.fromBackEnd),
    owner: runnerBuilder.fromBackEnd(team.owner)
  })
};

export default RunningTeamModel;
