import { useSuperMutation } from '@vadiun/react-hooks';
import {
  Button,
  FormikTextInput,
  IconButton
} from '@vadiun/react-native-components';
import DetailPage from 'app/shared/components/DetailPage';
import { ErrorMessage, Field, Formik, FormikProps } from 'formik';
import React, { useRef, useState } from 'react';
import { View } from 'react-native';
import * as Yup from 'yup';
import * as ImagePicker from 'expo-image-picker';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { Text } from 'react-native-paper';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import { RunningTeamStackNavigationType } from '../navigation/RunningTeamNavigationTypes';
import useRunningTeamRepository from '../services/useRunningTeamRepository';
import { RunningTeamCrud } from '../models/RunningTeam';

const RunningTeamImage = require('../../../assets/runningTeam.png');

const Schema = Yup.object({
  name: Yup.string().required('El nombre es requerido'),
  password: Yup.string()
    .required('La contraseña es requerida')
    .min(8, 'La contraseña debe tener mas de 8 caracteres'),
  passwordRepetition: Yup.string()
    .required('La contraseña es requerida')
    .min(8, 'La contraseña debe tener mas de 8 caracteres')
    .oneOf([Yup.ref('password')], 'Las contraseñas deben coincidir'),
  photo: Yup.mixed().required('La imagen es requerida')
});

const RunningTeamCreation = () => {
  const navigation =
    useNavigation<
      StackNavigationProp<RunningTeamStackNavigationType, 'create'>
    >();
  const formRef = useRef<FormikProps<RunningTeamCrud>>(null);
  const runningTeamRepository = useRunningTeamRepository();
  const loggedUserService = useLoggedRunner();
  const createMutation = useSuperMutation(runningTeamRepository.create, {
    showSuccessMessage: false
  });
  const [photo, setPhoto] = useState(RunningTeamImage);

  async function takePhoto() {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 0.7
    });
    if (!result.cancelled) {
      formRef.current?.setFieldValue('photo', result);
      setPhoto({ uri: result.uri });
    }
  }

  async function createRunningTeam(team: RunningTeamCrud) {
    await createMutation.mutate(team);
    loggedUserService.reload();
    navigation.goBack();
    navigation.navigate('creationSuccess');
  }

  return (
    <DetailPage imageSrc={photo}>
      <IconButton
        icon="camera"
        mode="light"
        shape="circle"
        onPress={takePhoto}
        size={30}
        style={{
          position: 'absolute',
          top: -25,
          right: 25
        }}
      />
      <Formik<RunningTeamCrud>
        innerRef={formRef}
        initialValues={{
          name: '',
          password: '',
          passwordRepetition: '',
          photo
        }}
        validationSchema={Schema}
        onSubmit={createRunningTeam}
      >
        {({ submitForm }) => (
          <View style={{ flex: 1, marginTop: 40 }}>
            <Field
              name="name"
              label="Nombre"
              mode="flat"
              component={FormikTextInput}
            />
            <Field
              name="password"
              label="Contraseña"
              mode="flat"
              secureTextEntry
              component={FormikTextInput}
            />
            <Field
              name="passwordRepetition"
              label="Repetir Contraseña"
              mode="flat"
              secureTextEntry
              component={FormikTextInput}
            />
            <ErrorMessage name="photo">
              {(message) => <Text>{message}</Text>}
            </ErrorMessage>

            <Button
              onPress={submitForm}
              mode="contained"
              style={{ marginTop: 'auto' }}
              loading={createMutation.isLoading}
              disabled={createMutation.isLoading}
            >
              Crear
            </Button>
          </View>
        )}
      </Formik>
    </DetailPage>
  );
};

export default RunningTeamCreation;
