import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-paper';
import { Field, Formik } from 'formik';
import * as Yup from 'yup';
import { Button, FormikTextInput } from '@vadiun/react-native-components';
import { useSuperMutation } from '@vadiun/react-hooks';
import { StackNavigationProp } from '@react-navigation/stack';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import DetailPage from 'app/shared/components/DetailPage';
import useRunningTeamRepository from '../services/useRunningTeamRepository';
import { RunningTeamStackNavigationType } from '../navigation/RunningTeamNavigationTypes';

const Schema = Yup.object({
  password: Yup.string().required('La contraseña es requerida')
});

const RunningTeamSignIn = () => {
  const runningTeamRepository = useRunningTeamRepository();
  const loggedRunner = useLoggedRunner();
  const route = useRoute<RouteProp<RunningTeamStackNavigationType, 'signIn'>>();
  const navigation =
    useNavigation<
      StackNavigationProp<RunningTeamStackNavigationType, 'signIn'>
    >();
  const signInMutation = useSuperMutation(runningTeamRepository.signIn, {
    showSuccessMessage: false
  });

  const signIn = async ({ password }: { password: string }) => {
    await signInMutation.mutate(route.params.runningTeam.id, password);
    loggedRunner.reload();
    navigation.navigate('main', { screen: 'myTeam' });
  };

  const { runningTeam } = route.params;
  return (
    <DetailPage imageSrc={{ uri: runningTeam.photo }}>
      <Text style={styles.title}>{runningTeam.name}</Text>
      <Formik
        initialValues={{
          password: ''
        }}
        validationSchema={Schema}
        onSubmit={signIn}
      >
        {({ submitForm }) => (
          <View style={{ flex: 1, marginTop: 40 }}>
            <Field
              name="password"
              label="Contraseña"
              mode="flat"
              secureTextEntry
              component={FormikTextInput}
            />

            <Button
              onPress={submitForm}
              mode="contained"
              style={{ marginTop: 'auto' }}
              loading={signInMutation.isLoading}
              disabled={signInMutation.isLoading}
            >
              Ingresar
            </Button>
          </View>
        )}
      </Formik>
    </DetailPage>
  );
};

export default RunningTeamSignIn;

const styles = StyleSheet.create({
  title: {
    fontFamily: 'poppins_bold',
    fontSize: 20,
    textAlign: 'center'
  }
});
