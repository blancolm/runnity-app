import { useSuperQuery } from '@vadiun/react-hooks';
import React, { useState } from 'react';
import { RefreshControl, StyleSheet } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { Searchbar } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import RunningTeamList from '../components/RunningTeamList';
import useRunningTeamRepository from '../services/useRunningTeamRepository';
import { RunningTeamStackNavigationType } from '../navigation/RunningTeamNavigationTypes';
import RunningTeamModel from '../models/RunningTeam';

const RunningTeamSearchPage = () => {
  const runningTeamRepository = useRunningTeamRepository();
  const loggedRunner = useLoggedRunner();
  const [searchBarText, setSearchBarText] = useState('');
  const navigation =
    useNavigation<
      StackNavigationProp<RunningTeamStackNavigationType, 'main'>
    >();
  const runningTeamsQuery = useSuperQuery(() => runningTeamRepository.getAll());

  const runningTeamsFiltered = (runningTeamsQuery.data ?? []).filter(
    (team: RunningTeamModel) =>
      team.name
        .toLocaleLowerCase()
        .includes(searchBarText.toLocaleLowerCase()) &&
      team.id !== loggedRunner.runner?.group_id
  );

  return (
    <ScrollView
      style={styles.container}
      refreshControl={
        <RefreshControl
          refreshing={runningTeamsQuery.isLoading}
          onRefresh={runningTeamsQuery.reload}
        />
      }
    >
      <Searchbar
        placeholder="Buscar"
        value={searchBarText}
        onChangeText={(text) => {
          setSearchBarText(text);
        }}
      />
      <RunningTeamList
        isLoading={runningTeamsQuery.isLoading}
        runningTeams={runningTeamsFiltered}
        onRunningTeamPress={(runningTeam) =>
          navigation.navigate('signIn', { runningTeam })
        }
      />
    </ScrollView>
  );
};

export default RunningTeamSearchPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  }
});
