import {
  useSuperMutation,
  useSuperQuery,
  useVerifyAction
} from '@vadiun/react-hooks';
import {
  Button,
  FormikTextInput,
  IconButton
} from '@vadiun/react-native-components';
import DetailPage from 'app/shared/components/DetailPage';
import { ErrorMessage, Field, Formik, FormikProps } from 'formik';
import React, { useRef, useState } from 'react';
import { View, StyleSheet, Animated, FlatList } from 'react-native';
import * as Yup from 'yup';
import * as ImagePicker from 'expo-image-picker';
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { Divider, Text } from 'react-native-paper';
import RunnerListItem from 'app/runner/components/RunnerListItem';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import RunnerModel from 'app/runner/models/Runner';
import CustomColors from 'app/shared/helpers/CustomColors';
import useRunningTeamRepository from '../services/useRunningTeamRepository';
import { RunningTeamStackNavigationType } from '../navigation/RunningTeamNavigationTypes';
import RunningTeamModel, { RunningTeamCrud } from '../models/RunningTeam';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';

const Schema = Yup.object({
  name: Yup.string().required('El nombre es requerido'),
  photo: Yup.mixed().required('La imagen es requerida')
});

const RunningTeamEdit = () => {
  const navigation =
    useNavigation<
      StackNavigationProp<RunningTeamStackNavigationType, 'edit'>
    >();
  navigation.setOptions({
    headerRight: () => (
      <Button onPress={formRef.current?.submitForm}>Guardar</Button>
    )
  });
  const verifyAction = useVerifyAction();
  const loggedRunner = useLoggedRunner();
  const route = useRoute<RouteProp<RunningTeamStackNavigationType, 'edit'>>();
  const formRef = useRef<FormikProps<RunningTeamCrud>>(null);
  const runningTeamRepository = useRunningTeamRepository();
  const editMutation = useSuperMutation(runningTeamRepository.edit, {
    showSuccessMessage: false
  });
  const removeRunnerMutation = useSuperMutation(
    runningTeamRepository.removeMember,
    {
      showSuccessMessage: false,
      showSpinner: true
    }
  );

  const runningTeamQuery = useSuperQuery(() =>
    runningTeamRepository.get(route.params.runningTeam.id)
  );

  const runningTeam: RunningTeamModel =
    runningTeamQuery.data || route.params.runningTeam;

  const [photo, setPhoto] = useState({ uri: runningTeam.photo });

  async function takePhoto() {
    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 0.7
    });
    if (!result.cancelled) {
      formRef.current?.setFieldValue('photo', result.uri);
      setPhoto({ uri: result.uri });
    }
  }

  async function editRunningTeam(team: RunningTeamCrud) {
    await editMutation.mutate({
      ...team,
      id: runningTeam.id,
      photo
    });
    navigation.goBack();
  }

  async function deleteRunner(runner: RunnerModel) {
    await removeRunnerMutation.mutate(runningTeam.id, runner.id);
    runningTeamQuery.reload();
  }

  const renderDelete =
    (runner: RunnerModel) =>
    (
      progress: Animated.AnimatedInterpolation,
      dragX: Animated.AnimatedInterpolation
    ) => {
      const scale = dragX.interpolate({
        inputRange: [0, 60],
        outputRange: [-60, 0],
        extrapolate: 'clamp'
      });
      return (
        <Animated.View
          style={{
            transform: [{ translateX: scale }],
            width: 60,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: CustomColors.dark300,
            borderTopLeftRadius: 5,
            borderBottomLeftRadius: 5
          }}
        >
          <IconButton
            icon="delete"
            onPress={() =>
              verifyAction({
                title: 'Alerta',
                body: '¿Está seguro que desea borrar este miembro?',
                onAccept: () => deleteRunner(runner)
              })
            }
          />
        </Animated.View>
      );
    };

  return (
    <Formik<RunningTeamCrud>
      innerRef={formRef}
      initialValues={{
        name: runningTeam.name,
        photo: runningTeam.photo
      }}
      validationSchema={Schema}
      onSubmit={editRunningTeam}
    >
      {() => (
        <DetailPage imageSrc={photo}>
          <IconButton
            icon="camera"
            mode="light"
            shape="circle"
            onPress={takePhoto}
            size={30}
            style={{
              position: 'absolute',
              top: -25,
              right: 25
            }}
          />

          <View style={{ flex: 1, marginTop: 40 }}>
            <Field
              name="name"
              label="Nombre"
              mode="flat"
              component={FormikTextInput}
            />
            <ErrorMessage name="photo">
              {(message) => <Text>{message}</Text>}
            </ErrorMessage>
            <Text style={styles.descriptionLabel}>Integrantes</Text>
            <FlatList
              data={runningTeam.members}
              keyExtractor={(item: RunnerModel) => String(item.id)}
              renderItem={({ item }) =>
                item.id !== loggedRunner.runner?.id ? (
                  <Swipeable
                    renderLeftActions={renderDelete(item)}
                    overshootLeft={false}
                  >
                    <RunnerListItem runner={item} />
                  </Swipeable>
                ) : (
                  <RunnerListItem runner={item} />
                )
              }
              ItemSeparatorComponent={() => <Divider />}
            />
          </View>
        </DetailPage>
      )}
    </Formik>
  );
};

const styles = StyleSheet.create({
  descriptionLabel: {
    fontFamily: 'poppins_bold',
    fontSize: 16,
    marginVertical: 10
  }
});

export default RunningTeamEdit;
