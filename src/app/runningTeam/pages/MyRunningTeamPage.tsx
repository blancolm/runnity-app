import { MaterialTopTabNavigationProp } from '@react-navigation/material-top-tabs';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import {
  useSuperMutation,
  useSuperQuery,
  useVerifyAction
} from '@vadiun/react-hooks';
import { Button, IconButton } from '@vadiun/react-native-components';
import RunnerListItem from 'app/runner/components/RunnerListItem';
import RunnerListItemLoading from 'app/runner/components/RunnerListItemLoading';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import DetailPage from 'app/shared/components/DetailPage';
import LoadingPlaceholder from 'app/shared/components/LoadingPlaceholder';
import CustomColors from 'app/shared/helpers/CustomColors';
import React, { useCallback, useState } from 'react';
import {
  Dimensions,
  StyleSheet,
  View,
  Image,
  ScrollView,
  RefreshControl
} from 'react-native';
import { Divider, Menu, Text } from 'react-native-paper';
import {
  RunningTeamNavigationTabsType,
  RunningTeamStackNavigationType
} from '../navigation/RunningTeamNavigationTypes';
import useRunningTeamRepository from '../services/useRunningTeamRepository';

const TeamWorkImage = require('assets/team-work.png');

const MyRunningTeamPage = () => {
  const loggedRunner = useLoggedRunner();
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const runningTeamRepository = useRunningTeamRepository();
  const verifyAction = useVerifyAction();
  const navigation =
    useNavigation<
      StackNavigationProp<RunningTeamStackNavigationType, 'main'>
    >();
  const navigationTabs =
    useNavigation<
      MaterialTopTabNavigationProp<RunningTeamNavigationTabsType, 'myTeam'>
    >();
  const deleteTeamMutation = useSuperMutation(runningTeamRepository.remove, {
    showSuccessMessage: false
  });
  const deleteTeamMemberMutation = useSuperMutation(
    runningTeamRepository.removeMember,
    { showSuccessMessage: false }
  );

  const teamQuery = useSuperQuery(
    runningTeamRepository.getLoggedUserRunningTeam,
    { showSpinner: false },
    [loggedRunner.runner?.group_id]
  );

  useFocusEffect(
    useCallback(() => {
      loggedRunner.reload();
      teamQuery.reload();
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
  );

  const deleteTeam = async () => {
    await deleteTeamMutation.mutate(teamQuery.data!.id);
    loggedRunner.reload();
  };

  const abandoneTeam = async () => {
    await deleteTeamMemberMutation.mutate(
      teamQuery.data!.id,
      loggedRunner.runner!.id
    );
    loggedRunner.reload();
  };

  if (teamQuery.data === undefined) {
    return (
      <View style={styles.container}>
        <LoadingPlaceholder style={styles.image} />
        <View style={styles.body}>
          <View style={{ alignItems: 'center' }}>
            <LoadingPlaceholder style={{ height: 30, width: 200 }} />
          </View>
          <LoadingPlaceholder
            style={{ height: 20, width: 150, marginVertical: 20 }}
          />
          <RunnerListItemLoading />
          <Divider />
          <RunnerListItemLoading />
          <Divider />
          <RunnerListItemLoading />
          <Divider />
        </View>
      </View>
    );
  }
  console.log('id', loggedRunner.runner?.group_id);
  console.log('temaquery', teamQuery.data);
  if (loggedRunner.runner?.group_id === null || teamQuery.data === null) {
    return (
      <ScrollView
        style={[styles.container, { padding: 30 }]}
        contentContainerStyle={{ flexGrow: 1 }}
        refreshControl={
          <RefreshControl
            refreshing={teamQuery.isLoading}
            onRefresh={teamQuery.reload}
          />
        }
      >
        <Image
          source={TeamWorkImage}
          style={{
            width: 150,
            height: 150,
            borderRadius: 20,
            marginBottom: 20,
            alignSelf: 'center'
          }}
        />
        <Text style={{ textAlign: 'center', fontFamily: 'poppins_bold' }}>
          Aún no sos parte de ningún grupo de running
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: CustomColors.secondaryText,
            marginTop: 20
          }}
        >
          ¡Correr se disfruta más en compañía!
        </Text>
        <Text
          style={{ textAlign: 'center', color: CustomColors.secondaryText }}
        >
          Buscá un grupo y unite, o creá tu propio grupo de running para
          participar con tus amigos
        </Text>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginTop: 'auto'
          }}
        >
          <Button
            mode="outline"
            style={{ width: '48%' }}
            onPress={() => navigationTabs.navigate('search')}
          >
            Buscar grupo
          </Button>
          <Button
            mode="outline"
            style={{ width: '48%' }}
            onPress={() => navigation.navigate('create')}
          >
            Crear grupo
          </Button>
        </View>
      </ScrollView>
    );
  }

  return (
    <ScrollView
      style={styles.container}
      refreshControl={
        <RefreshControl
          refreshing={teamQuery.isLoading}
          onRefresh={teamQuery.reload}
        />
      }
    >
      <View style={styles.moreButton}>
        <Menu
          visible={isMenuOpen}
          onDismiss={() => setIsMenuOpen(false)}
          anchor={
            <View style={{ backgroundColor: '#ffffff00' }}>
              <IconButton
                mode="light"
                icon="dots-horizontal"
                shape="circle"
                onPress={() => setIsMenuOpen(true)}
              />
            </View>
          }
        >
          {teamQuery.data?.owner?.id === loggedRunner.runner!.id ? (
            <>
              <Menu.Item
                onPress={() => {
                  setIsMenuOpen(false);
                  navigation.navigate('edit', { runningTeam: teamQuery.data! });
                }}
                title="Editar"
              />
              <Menu.Item
                onPress={() => {
                  setIsMenuOpen(false);
                  verifyAction({
                    title: 'Alerta',
                    body: '¿Está seguro que desea borrar este grupo?',
                    onAccept: deleteTeam
                  });
                }}
                title="Eliminar"
              />
            </>
          ) : null}
          {teamQuery.data?.owner?.id !== loggedRunner.runner!.id ? (
            <Menu.Item
              onPress={() => {
                setIsMenuOpen(false);
                verifyAction({
                  title: 'Alerta',
                  body: '¿Está seguro que desea abandonar este grupo?',
                  onAccept: abandoneTeam
                });
              }}
              title="Abandonar"
            />
          ) : null}
        </Menu>
      </View>
      <DetailPage imageSrc={{ uri: teamQuery.data.photo }}>
        <Text style={styles.title}>{teamQuery.data.name}</Text>
        <Text style={styles.descriptionLabel}>Integrantes</Text>
        {teamQuery.data.members.map((runner) => (
          <View key={runner.id}>
            <Divider />
            <RunnerListItem runner={runner} />
          </View>
        ))}
      </DetailPage>
    </ScrollView>
  );
};

export default MyRunningTeamPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative'
  },
  body: {
    padding: 30,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    marginTop: -20,
    elevation: 1,
    backgroundColor: CustomColors.dark900,
    flex: 1
  },
  image: {
    width: '100%',
    height: Dimensions.get('window').height / 4
  },
  title: {
    fontFamily: 'poppins_bold',
    fontSize: 20,
    textAlign: 'center'
  },
  descriptionLabel: {
    fontFamily: 'poppins_bold',
    fontSize: 16,
    marginVertical: 10
  },
  moreButton: {
    position: 'absolute',
    margin: 10,
    right: 0,
    top: 0,
    zIndex: 2
  }
});
