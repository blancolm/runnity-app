import { TouchableOpacity } from '@gorhom/bottom-sheet';
import React from 'react';
import { View } from 'react-native';
import { Divider } from 'react-native-paper';
import RunningTeamModel from '../models/RunningTeam';
import RunningTeamListItem from './runningTeamListItem';
import RunningTeamListItemLoading from './RunningTeamListItemLoading';

interface Props {
  isLoading: boolean;
  runningTeams: RunningTeamModel[];
  onRunningTeamPress: (team: RunningTeamModel) => void;
}

const RunningTeamList = ({
  isLoading,
  runningTeams,
  onRunningTeamPress
}: Props) => {
  if (isLoading) {
    return (
      <View>
        <Divider />
        <RunningTeamListItemLoading />
        <Divider />
        <RunningTeamListItemLoading />
        <Divider />
        <RunningTeamListItemLoading />
        <Divider />
      </View>
    );
  }
  return (
    <View>
      {runningTeams.map((team) => (
        <TouchableOpacity
          key={team.id}
          activeOpacity={0.7}
          onPress={() => onRunningTeamPress(team)}
        >
          <RunningTeamListItem runningTeam={team} />
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default RunningTeamList;
