import React from 'react';
import { StyleSheet, View } from 'react-native';
import LoadingPlaceholder from 'app/shared/components/LoadingPlaceholder';

const RunningTeamListItemLoading = () => (
  <View style={styles.container}>
    <LoadingPlaceholder style={styles.image} />
    <View style={styles.body}>
      <LoadingPlaceholder style={{ height: 30, width: 200 }} />
      <LoadingPlaceholder style={{ height: 20, width: 100, marginTop: 10 }} />
    </View>
  </View>
);

export default RunningTeamListItemLoading;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  image: {
    height: 55,
    width: 55,
    borderRadius: 15
  },
  name: {
    fontSize: 18
  },
  body: {
    flex: 1,
    marginLeft: 20
  },
  members: { flexDirection: 'row', alignItems: 'center' }
});
