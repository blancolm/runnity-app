import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Text } from 'react-native-paper';
import MaterialCommunityIcons from '@expo/vector-icons/MaterialCommunityIcons';
import RunningTeamModel from '../models/RunningTeam';

interface Props {
  runningTeam: RunningTeamModel;
  right?: React.ReactNode;
}

const RunningTeamListItem = ({ runningTeam, right }: Props) => (
  <View style={styles.container}>
    <Image source={{ uri: runningTeam.photo }} style={styles.image} />
    <View style={styles.body}>
      <Text style={styles.name}>{runningTeam.name}</Text>
      <View style={styles.members}>
        <MaterialCommunityIcons
          name="account-group"
          style={{ color: CustomColors.secondaryText, marginRight: 10 }}
          size={20}
        />
        <Text style={{ color: CustomColors.secondaryText }}>
          {runningTeam.members.length} integrantes
        </Text>
      </View>
    </View>
    <View style={{ marginLeft: 'auto' }}>{right}</View>
  </View>
);

export default RunningTeamListItem;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  image: {
    height: 55,
    width: 55,
    borderRadius: 15
  },
  name: {
    fontSize: 18
  },
  body: {
    flex: 1,
    marginLeft: 20
  },
  members: { flexDirection: 'row', alignItems: 'center' }
});
