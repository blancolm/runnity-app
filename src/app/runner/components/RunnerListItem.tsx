import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Text } from 'react-native-paper';
import RunnerModel from '../models/Runner';

interface Props {
  runner: RunnerModel;
  right?: JSX.Element;
}

const RunnerListItem = ({ runner, right }: Props) => (
  <View style={styles.container}>
    <Image source={{ uri: runner.photo }} style={styles.image} />
    <Text style={styles.name}>{runner.name}</Text>
    <View style={{ marginLeft: 'auto' }}>{right}</View>
  </View>
);

export default RunnerListItem;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  image: {
    height: 40,
    width: 40,
    borderRadius: 50
  },
  name: {
    fontSize: 16,
    marginLeft: 30,
    color: CustomColors.secondaryText
  }
});
