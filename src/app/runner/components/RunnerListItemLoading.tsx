import LoadingPlaceholder from 'app/shared/components/LoadingPlaceholder';
import React from 'react';
import { StyleSheet, View } from 'react-native';

const RunnerListItemLoading = () => (
  <View style={styles.container}>
    <LoadingPlaceholder style={styles.image} />
    <LoadingPlaceholder style={{ width: 150, height: 20, marginLeft: 30 }} />
  </View>
);

export default RunnerListItemLoading;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  image: {
    height: 40,
    width: 40,
    borderRadius: 50
  }
});
