import { ImageInfo } from '@vadiun/react-native-components/lib/typescript/src/EditablePhoto';
import { Moment } from 'moment';

export interface ProfileFormType {
  birthdate: Moment;
  name: string;
  weight: string;
  sex: string;
  photo: ImageInfo | string | { uri: string };
}
