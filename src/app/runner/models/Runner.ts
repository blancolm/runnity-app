import { imageMapper } from 'app/shared/helpers/imageMapper';
import moment, { Moment } from 'moment';

interface RunnerModel {
  id: number;
  name: string;
  birthdate: Moment;
  sex: string;
  weight: number;
  photo: string;
  user_id: number;
  group_id: number | null;
  level_2k: number;
  level_5k: number;
  level_10k: number;
  seconds_2k: number | null;
  seconds_5k: number | null;
  seconds_10k: number | null;
}

export const runnerBuilder = {
  fromBackEnd: (runner: any): RunnerModel => ({
    ...runner,
    birthdate: moment(runner.birthdate),
    photo: imageMapper(runner.photo)
  })
};

export default RunnerModel;
