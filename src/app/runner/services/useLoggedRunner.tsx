import { useQuery } from '@vadiun/react-hooks';
import { useAuth } from 'app/auth/services/useAuth';
import React, {
  createContext,
  FC,
  useContext,
  useEffect,
  useState
} from 'react';
import RunnerModel from '../models/Runner';
import useRunnerRepository from './useRunnerRepository';

export const LoggedRunnerContext = createContext(
  {} as {
    runner: RunnerModel | undefined;
    reload: () => void;
    isLoading: boolean;
    hasCompletedProfile: boolean | undefined;
  }
);

export const LoggedRunnerProvider: FC = (props) => {
  const authService = useAuth();
  const runnerRepository = useRunnerRepository();
  const runnerQuery = useQuery(async () => {
    if (authService.loggedUserID !== undefined) {
      const res = await runnerRepository.getLoggedUser();
      return res;
    }
    return undefined;
  }, [authService.loggedUserID]);

  const hasCompletedProfile = runnerQuery.isError
    ? false
    : runnerQuery.data === undefined
    ? undefined
    : runnerQuery.data !== undefined &&
      runnerQuery.data.birthdate !== undefined &&
      runnerQuery.data.sex !== undefined &&
      runnerQuery.data.photo !== undefined &&
      runnerQuery.data.weight !== undefined;

  const value = {
    runner: runnerQuery.data,
    reload: runnerQuery.reload,
    isLoading: runnerQuery.isLoading,
    hasCompletedProfile
  };
  return (
    <LoggedRunnerContext.Provider value={value}>
      {props.children}
    </LoggedRunnerContext.Provider>
  );
};

const useLoggedRunner = () => {
  const context = useContext(LoggedRunnerContext);
  if (context?.isLoading === undefined) {
    throw new Error(
      'useLoggedRunner must be surrounded by LoggedRunnerProvider'
    );
  }
  return context;
};

export default useLoggedRunner;
