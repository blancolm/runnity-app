import { jsonToFormData } from 'app/shared/helpers/jsonToFormData';
import { httpClient } from 'app/shared/services/http/httpClient';
import { ProfileFormType } from '../models/ProfileFormType';
import RunnerModel, { runnerBuilder } from '../models/Runner';

const useRunnerRepository = () => {
  const get = async (id: number) => {
    const res = await httpClient.get(`runners/${id}`);
    return runnerBuilder.fromBackEnd(res);
  };

  const getLoggedUser = async () => {
    const res = await httpClient.get('runners/me');
    return runnerBuilder.fromBackEnd(res);
  };

  const getAll = async (): Promise<RunnerModel[]> => {
    const res = await httpClient.get(`runners`);
    return res.map(runnerBuilder.fromBackEnd);
  };

  const complete = async (profile: ProfileFormType): Promise<void> => {
    await httpClient.post(
      `users/me/runners`,
      jsonToFormData({
        name: profile.name,
        birthdate: profile.birthdate.format('YYYY-MM-DD'),
        sex: profile.sex,
        weight: profile.weight,
        photo: profile.photo
      })
    );
  };

  const edit = async (profile: ProfileFormType): Promise<void> => {
    await httpClient.post(
      `runners/me?_method=PUT`,
      jsonToFormData({
        name: profile.name,
        birthdate: profile.birthdate.format('YYYY-MM-DD'),
        sex: profile.sex,
        weight: profile.weight,
        photo: profile.photo
      })
    );
  };

  return {
    get,
    getAll,
    complete,
    getLoggedUser,
    edit
  };
};

export default useRunnerRepository;
