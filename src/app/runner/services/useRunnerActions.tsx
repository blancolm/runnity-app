import { ProfileFormType } from '../models/ProfileFormType';
import useLoggedRunner from './useLoggedRunner';
import useRunnerRepository from './useRunnerRepository';

export const useRunnerActions = () => {
  const runnerRepository = useRunnerRepository();
  const loggedRunner = useLoggedRunner();

  const complete = async (profile: ProfileFormType) => {
    await runnerRepository.complete(profile);
    loggedRunner.reload();
  };

  const edit = async (profile: ProfileFormType) => {
    await runnerRepository.edit(profile);
    loggedRunner.reload();
  };

  return { ...runnerRepository, complete, edit };
};
