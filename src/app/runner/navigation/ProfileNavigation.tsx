import React from 'react';
import { CompetitionModel } from 'app/competition/main/models/Competition';
import { createStackNavigator } from '@react-navigation/stack';
import { CompetitionRunningTeamModel } from 'app/competition/team/models/CompetitionRunningTeamModel';
import { TrainingSummaryPage } from 'app/training/pages/TrainingSummaryPage';
import CompetitionPrivateResultsPage from 'app/competition/private/pages/CompetitionPrivateResultsPage';
import CompetitionRunningTeamResultsPage from 'app/competition/team/pages/CompetitionRunningTeamResultsPage';
import ProfilePage from '../profile/pages/ProfilePage';
import { EditProfilePage } from '../profile/pages/EditProfilePage';
import { ProfileCompetitionsPage } from '../profile/pages/ProfileCompetitionsPage';

export type RunnerStackNavigationType = {
  profile: undefined;
  trainingSummary: undefined;
  editProfile: undefined;
  allCompetitions: undefined;
  competitionPrivateResults: {
    competition: CompetitionModel;
  };
  competitionRunningTeamResults: {
    competition: CompetitionRunningTeamModel;
  };
};

const Stack = createStackNavigator<RunnerStackNavigationType>();

export const ProfileStackNavigation = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="profile"
      options={{
        headerShown: false
      }}
      component={ProfilePage}
    />
    <Stack.Screen
      name="editProfile"
      options={{
        title: 'Edición'
      }}
      component={EditProfilePage}
    />
    <Stack.Screen
      name="trainingSummary"
      options={{
        title: 'Entrenamientos'
      }}
      component={TrainingSummaryPage}
    />
    <Stack.Screen
      name="allCompetitions"
      options={{
        title: 'Competiciones'
      }}
      component={ProfileCompetitionsPage}
    />
    <Stack.Screen
      name="competitionPrivateResults"
      options={{
        title: 'Resultados'
      }}
      component={CompetitionPrivateResultsPage}
    />
    <Stack.Screen
      name="competitionRunningTeamResults"
      options={{
        title: 'Resultados'
      }}
      component={CompetitionRunningTeamResultsPage}
    />
  </Stack.Navigator>
);
