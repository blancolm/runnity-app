import { useSuperMutation } from '@vadiun/react-hooks';
import React from 'react';
import { KeyboardAvoidingView, Platform, ScrollView } from 'react-native';
import { Text } from 'react-native-paper';
import CustomColors from 'app/shared/helpers/CustomColors';
import useRunnerRepository from '../../services/useRunnerRepository';
import { RunnerProfileForm } from '../components/RunnerProfileForm';

const CompleteProfilePage = ({
  reloadRunner
}: {
  reloadRunner: () => void;
}) => {
  const profileActions = useRunnerRepository();
  const completeMutation = useSuperMutation(profileActions.complete, {
    showSpinner: false,
    showSuccessMessage: false
  });

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{ flex: 1 }}
    >
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          padding: 20,
          paddingTop: 40,
          backgroundColor: CustomColors.dark900
        }}
      >
        <Text
          style={{
            textAlign: 'center',
            fontFamily: 'poppins_bold',
            fontSize: 32
          }}
        >
          Bienvenido
        </Text>
        <Text
          style={{
            textAlign: 'center',
            color: CustomColors.secondaryText,
            marginBottom: 10
          }}
        >
          Complete su perfil para continuar
        </Text>
        <RunnerProfileForm
          onSubmit={async (values) => {
            await completeMutation.mutate(values);
            reloadRunner();
          }}
        />
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default CompleteProfilePage;
