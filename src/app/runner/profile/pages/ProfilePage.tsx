import React, { useCallback } from 'react';
import {
  NavigationProp,
  useFocusEffect,
  useNavigation
} from '@react-navigation/native';
import { RunnerStackNavigationType } from 'app/runner/navigation/ProfileNavigation';
import { CompetitionModel } from 'app/competition/main/models/Competition';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTrainingActions } from 'app/training/services/useTrainingActions';
import { useSuperQuery } from '@vadiun/react-hooks';
import { useAuth } from 'app/auth/services/useAuth';
import useCompetitionRepository from 'app/competition/main/services/useCompetitionRepository';
import { ScrollView } from 'react-native-gesture-handler';
import {
  CompetitionRunningTeamModel,
  isRunningTeamCompetition
} from 'app/competition/team/models/CompetitionRunningTeamModel';
import { RefreshControl } from 'react-native';
import { RunnerProfile } from '../components';
import useLoggedRunner from '../../services/useLoggedRunner';

const ProfilePage = () => {
  const authService = useAuth();
  const loggedRunner = useLoggedRunner();
  const navigation = useNavigation<NavigationProp<RunnerStackNavigationType>>();
  const competitionRepo = useCompetitionRepository();
  const trainingActions = useTrainingActions();
  const trainingQuery = useSuperQuery(trainingActions.getLoggedRunnerResults);
  const competitionQuery = useSuperQuery(
    competitionRepo.getFinishedCompetitions
  );
  useFocusEffect(
    useCallback(() => {
      trainingQuery.reload();
      competitionQuery.reload();
      loggedRunner.reload();
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
  );
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={
              trainingQuery.isLoading ||
              loggedRunner.isLoading ||
              competitionQuery.isLoading
            }
            onRefresh={() => {
              trainingQuery.reload();
              competitionQuery.reload();
              loggedRunner.reload();
            }}
          />
        }
      >
        <RunnerProfile
          competitions={competitionQuery.data?.slice(0, 5) ?? []}
          runner={loggedRunner.runner!}
          onShowTrainingDetails={() => navigation.navigate('trainingSummary')}
          trainings={trainingQuery.data ?? []}
          onLogout={authService.logout}
          onShowAllCompetitions={() => navigation.navigate('allCompetitions')}
          onShowCompetition={(
            competition: CompetitionModel | CompetitionRunningTeamModel
          ) => {
            if (isRunningTeamCompetition(competition)) {
              navigation.navigate('competitionRunningTeamResults', {
                competition
              });
              return;
            }
            navigation.navigate('competitionPrivateResults', { competition });
          }}
          onEdit={() => navigation.navigate('editProfile')}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

export default ProfilePage;
