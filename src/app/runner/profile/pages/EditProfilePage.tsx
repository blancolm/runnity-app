import { NavigationProp, useNavigation } from '@react-navigation/native';
import { useSuperMutation } from '@vadiun/react-hooks';
import { ProfileFormType } from 'app/runner/models/ProfileFormType';
import { RunnerStackNavigationType } from 'app/runner/navigation/ProfileNavigation';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import { useRunnerActions } from 'app/runner/services/useRunnerActions';
import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { KeyboardAvoidingView, Platform, ScrollView } from 'react-native';
import { RunnerProfileForm } from '../components/RunnerProfileForm';

export const EditProfilePage = () => {
  const navigation =
    useNavigation<NavigationProp<RunnerStackNavigationType, 'editProfile'>>();
  const loggedRunner = useLoggedRunner();
  const runnerActions = useRunnerActions();
  const editMutation = useSuperMutation(runnerActions.edit, {
    showSuccessMessage: false
  });

  const edit = async (x: ProfileFormType) => {
    await editMutation.mutate(x);
    navigation.navigate('profile');
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{ flex: 1 }}
    >
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          padding: 20,
          paddingTop: 40,
          backgroundColor: CustomColors.dark900
        }}
      >
        <RunnerProfileForm
          onSubmit={edit}
          initialValues={{
            ...loggedRunner.runner!,
            photo: { uri: loggedRunner.runner!.photo },
            weight: String(loggedRunner.runner!.weight)
          }}
        />
      </ScrollView>
    </KeyboardAvoidingView>
  );
};
