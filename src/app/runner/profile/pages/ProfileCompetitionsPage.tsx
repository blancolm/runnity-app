import { NavigationProp, useNavigation } from '@react-navigation/native';
import { useSuperQuery } from '@vadiun/react-hooks';
import {
  CompetitionModel,
  CompetitionParticipationFinished
} from 'app/competition/main/models/Competition';
import useCompetitionRepository from 'app/competition/main/services/useCompetitionRepository';
import {
  CompetitionRunningTeamModel,
  isRunningTeamCompetition
} from 'app/competition/team/models/CompetitionRunningTeamModel';
import { RunnerStackNavigationType } from 'app/runner/navigation/ProfileNavigation';
import useLoggedRunner from 'app/runner/services/useLoggedRunner';
import { biggerDistanceFirst } from 'app/shared/helpers/biggerDistanceFirst';
import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { FlatList, ListRenderItemInfo, StyleSheet, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Divider } from 'react-native-paper';
import { RunnerProfileCompetitionListItem } from '../components/RunnerProfileCompetitions/RunnerProfileCompetitionListItem';

export const ProfileCompetitionsPage = () => {
  const navigation =
    useNavigation<
      NavigationProp<RunnerStackNavigationType, 'allCompetitions'>
    >();
  const competitionRepo = useCompetitionRepository();
  const competitionQuery = useSuperQuery(
    competitionRepo.getFinishedCompetitions
  );
  const loggedRunner = useLoggedRunner();
  return (
    <View style={styles.container}>
      <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
        <FlatList
          data={competitionQuery.data ?? []}
          keyExtractor={(
            item: CompetitionModel | CompetitionRunningTeamModel
          ) => String(item.id)}
          renderItem={({
            item
          }: ListRenderItemInfo<
            CompetitionModel | CompetitionRunningTeamModel
          >) =>
            isRunningTeamCompetition(item) ? (
              <TouchableOpacity
                onPress={() =>
                  navigation.navigate('competitionRunningTeamResults', {
                    competition: item
                  })
                }
              >
                <RunnerProfileCompetitionListItem
                  participants={item.participationsPerTeam.map(
                    (rt) => rt.runningTeam.name
                  )}
                  competitionType={item.type}
                  myPlacement={
                    item.participationsPerTeam
                      .map((t) => t.participations)
                      .flat()
                      .sort(
                        (
                          a: CompetitionParticipationFinished,
                          b: CompetitionParticipationFinished
                        ) => biggerDistanceFirst(a, b)
                      )
                      .findIndex(
                        (p) => p.runner.id === loggedRunner.runner?.id!
                      ) + 1
                  }
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity>
                <RunnerProfileCompetitionListItem
                  participants={item.participations.map((p) => p.runner.name)}
                  competitionType={item.type}
                  myPlacement={
                    item.participations
                      .sort(
                        (
                          a: CompetitionParticipationFinished,
                          b: CompetitionParticipationFinished
                        ) => biggerDistanceFirst(a, b)
                      )
                      .findIndex(
                        (p) => p.runner.id === loggedRunner.runner?.id!
                      ) + 1
                  }
                />
              </TouchableOpacity>
            )
          }
          ItemSeparatorComponent={Divider}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
    paddingTop: 10,
    backgroundColor: CustomColors.dark600
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  title: { fontSize: 16, marginLeft: 20 }
});
