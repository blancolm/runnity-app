import RunnerModel from 'app/runner/models/Runner';
import CustomColors from 'app/shared/helpers/CustomColors';
import moment from 'moment';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-paper';
import RunnerProfileResultMedal from './RunnerProfileResultMedal';

interface Props {
  runner: RunnerModel;
}

export const RunnerProfileResults = ({ runner }: Props) => {
  const hasAnyResult =
    runner.level_10k !== null &&
    runner.level_5k !== null &&
    runner.level_10k !== null;

  const level10k =
    runner.level_10k <= 3
      ? 'gold'
      : runner.level_10k <= 6
      ? 'silver'
      : 'bronce';
  const level5k =
    runner.level_5k <= 3 ? 'gold' : runner.level_5k <= 6 ? 'silver' : 'bronce';
  const level2k =
    runner.level_2k <= 3 ? 'gold' : runner.level_2k <= 6 ? 'silver' : 'bronce';

  return (
    <View style={{ backgroundColor: CustomColors.dark600, padding: 20 }}>
      <Text style={{ fontSize: 16 }}>Mis resultados</Text>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          marginTop: 20
        }}
      >
        {hasAnyResult ? (
          <>
            <View style={styles.result}>
              <Text style={{ fontFamily: 'poppins_bold' }}>2K</Text>
              <RunnerProfileResultMedal
                type={level2k}
                subtype={runner.level_2k ? runner.level_2k % 3 : 0}
              />
              <Text style={{ color: CustomColors.secondaryText }}>
                {moment('2020-01-01')
                  .seconds(runner.seconds_2k ?? 0)
                  .format('HH:mm:ss')}
              </Text>
            </View>
            <View style={styles.result}>
              <Text style={{ fontFamily: 'poppins_bold' }}>5K</Text>
              <RunnerProfileResultMedal
                type={level5k}
                subtype={runner.level_5k ? runner.level_5k % 3 : 0}
              />
              <Text style={{ color: CustomColors.secondaryText }}>
                {moment('2020-01-01')
                  .seconds(runner.seconds_5k ?? 0)
                  .format('HH:mm:ss')}
              </Text>
            </View>
            <View style={styles.result}>
              <Text style={{ fontFamily: 'poppins_bold' }}>10K</Text>
              <RunnerProfileResultMedal
                type={level10k}
                subtype={runner.level_10k ? runner.level_10k % 3 : 0}
              />
              <Text style={{ color: CustomColors.secondaryText }}>
                {moment('2020-01-01')
                  .seconds(runner.seconds_10k ?? 0)
                  .format('HH:mm:ss')}
              </Text>
            </View>
          </>
        ) : (
          <Text style={{ color: CustomColors.secondaryText }}>
            Participa en entrenamientos y competencias para ver tus resultados
          </Text>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  result: {
    alignItems: 'center'
  }
});
