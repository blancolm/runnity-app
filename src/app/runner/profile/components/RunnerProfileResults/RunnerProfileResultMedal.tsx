import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { Text } from 'react-native-paper';

const GoldMedal = require('assets/gold-medal.png');
const SilverMedal = require('assets/silver-medal.png');
const BronceMedal = require('assets/bronce-medal.png');

interface Props {
  type: 'gold' | 'silver' | 'bronce';
  subtype: number;
}

const RunnerProfileResultMedal = ({ type, subtype }: Props) => {
  let Medal = GoldMedal;
  if (type === 'silver') {
    Medal = SilverMedal;
  }
  if (type === 'bronce') {
    Medal = BronceMedal;
  }

  return (
    <View>
      <Image
        source={Medal}
        style={{ height: 50, width: 50 }}
        height={50}
        resizeMode="contain"
      />
      <View style={styles.subtype}>
        <Text>{subtype}</Text>
      </View>
    </View>
  );
};

export default RunnerProfileResultMedal;

const styles = StyleSheet.create({
  subtype: {
    position: 'absolute',
    right: -5,
    top: -5,
    borderRadius: 20,
    width: 25,
    height: 25,
    backgroundColor: CustomColors.dark300,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: CustomColors.dark600
  }
});
