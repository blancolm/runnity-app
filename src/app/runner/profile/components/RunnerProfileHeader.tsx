import React from 'react';
import { StyleSheet, View, Image, Dimensions } from 'react-native';
import { Text } from 'react-native-paper';
import CustomColors from 'app/shared/helpers/CustomColors';
import moment from 'moment';
import { Button, IconButton } from '@vadiun/react-native-components';
import RunnerModel from '../../models/Runner';

interface Props {
  runner: RunnerModel;
  onLogout: () => void;
  onEdit: () => void;
}

export const RunnerProfileHeader = ({ runner, onLogout, onEdit }: Props) => (
  <>
    <View style={styles.header}>
      <IconButton icon="logout" style={styles.exit} onPress={onLogout} />
      <Button mode="outline" style={styles.edit} onPress={onEdit}>
        Editar
      </Button>
      <Image source={{ uri: runner.photo }} style={styles.image} />
    </View>
    <View style={styles.body}>
      <Text
        style={{
          fontFamily: 'poppins_bold',
          fontSize: 24
        }}
      >
        {runner.name}
      </Text>
      <View style={{ flexDirection: 'row' }}>
        <Text style={{ color: CustomColors.secondaryText }}>
          {moment().diff(runner.birthdate, 'years')} años
        </Text>
        <View
          style={{
            marginHorizontal: 20,
            width: 1,
            backgroundColor: CustomColors.dark300
          }}
        />
        <Text style={{ color: CustomColors.secondaryText }}>
          {runner.weight} kg.
        </Text>
        <View
          style={{
            marginHorizontal: 20,
            width: 1,
            backgroundColor: CustomColors.dark300
          }}
        />
        <Text style={{ color: CustomColors.secondaryText }}>
          {runner.sex === 'male'
            ? 'Masculino'
            : runner.sex === 'female'
            ? 'Femenino'
            : 'Otro'}
        </Text>
      </View>
    </View>
  </>
);

const styles = StyleSheet.create({
  header: {
    height: 110,
    backgroundColor: CustomColors.dark600
  },
  image: {
    height: 150,
    width: 150,
    borderRadius: 200,
    position: 'absolute',
    top: 30,
    right: (Dimensions.get('window').width - 150) / 2
  },
  body: {
    marginTop: 60,
    padding: 20,
    alignItems: 'center'
  },
  exit: {
    margin: 10,
    position: 'absolute',
    top: 0,
    left: 0
  },
  edit: {
    margin: 10,
    position: 'absolute',
    top: 0,
    right: 0
  }
});
