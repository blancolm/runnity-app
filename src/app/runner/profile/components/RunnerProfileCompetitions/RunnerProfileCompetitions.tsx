import { Button } from '@vadiun/react-native-components';
import {
  CompetitionModel,
  CompetitionParticipationFinished
} from 'app/competition/main/models/Competition';
import {
  CompetitionRunningTeamModel,
  isRunningTeamCompetition
} from 'app/competition/team/models/CompetitionRunningTeamModel';
import RunnerModel from 'app/runner/models/Runner';
import { biggerDistanceFirst } from 'app/shared/helpers/biggerDistanceFirst';
import CustomColors from 'app/shared/helpers/CustomColors';
import React from 'react';
import { ListRenderItemInfo, StyleSheet, View } from 'react-native';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { Divider, Text } from 'react-native-paper';
import { RunnerProfileCompetitionListItem } from './RunnerProfileCompetitionListItem';

interface Props {
  competitions: (CompetitionModel | CompetitionRunningTeamModel)[];
  runner: RunnerModel;
  onShowAllCompetitions: () => void;
  onShowCompetition: (
    competition: CompetitionModel | CompetitionRunningTeamModel
  ) => void;
}

export const RunnerProfileCompetitions = ({
  competitions,
  runner,
  onShowAllCompetitions,
  onShowCompetition
}: Props) => (
  <View style={styles.container}>
    <View style={styles.row}>
      <Text style={styles.title}>Mis competiciones</Text>
      <Button onPress={onShowAllCompetitions}>Ver todos</Button>
    </View>
    <View style={{ marginHorizontal: 20, marginBottom: 20 }}>
      <FlatList
        data={competitions}
        keyExtractor={(item: CompetitionModel | CompetitionRunningTeamModel) =>
          String(item.id)
        }
        renderItem={({
          item
        }: ListRenderItemInfo<
          CompetitionModel | CompetitionRunningTeamModel
        >) => (
          <TouchableOpacity onPress={() => onShowCompetition(item)}>
            {isRunningTeamCompetition(item) ? (
              <RunnerProfileCompetitionListItem
                participants={item.participationsPerTeam.map(
                  (rt) => rt.runningTeam.name
                )}
                competitionType={item.type}
                myPlacement={
                  item.participationsPerTeam
                    .map((t) => t.participations)
                    .flat()
                    .sort(
                      (
                        a: CompetitionParticipationFinished,
                        b: CompetitionParticipationFinished
                      ) => biggerDistanceFirst(a, b)
                    )
                    .findIndex((p) => p.runner.id === runner.id) + 1
                }
              />
            ) : (
              <RunnerProfileCompetitionListItem
                participants={item.participations.map((p) => p.runner.name)}
                competitionType={item.type}
                myPlacement={
                  item.participations
                    .sort(
                      (
                        a: CompetitionParticipationFinished,
                        b: CompetitionParticipationFinished
                      ) => biggerDistanceFirst(a, b)
                    )
                    .findIndex((p) => p.runner.id === runner.id) + 1
                }
              />
            )}
          </TouchableOpacity>
        )}
        ItemSeparatorComponent={Divider}
      />
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
    paddingTop: 10,
    backgroundColor: CustomColors.dark600
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  title: { fontSize: 16, marginLeft: 20 }
});
