import CustomColors from 'app/shared/helpers/CustomColors';
import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-paper';

interface Props {
  participants: string[];
  competitionType: string;
  myPlacement: number;
}

const Badge: FC = ({ children }) => (
  <View style={styles.badge}>
    <Text>{children}</Text>
  </View>
);

export const RunnerProfileCompetitionListItem = ({
  participants,
  myPlacement,
  competitionType
}: Props) => (
  <View style={styles.container}>
    <Badge>{myPlacement}</Badge>
    <View style={{ marginLeft: 20 }}>
      <Text>{competitionType}</Text>
      <Text style={styles.description}>{participants.join(', ')}</Text>
    </View>
  </View>
);

const styles = StyleSheet.create({
  badge: {
    backgroundColor: CustomColors.dark300,
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50
  },
  container: {
    flexDirection: 'row',
    paddingVertical: 10,
    alignItems: 'center'
  },
  description: {
    color: CustomColors.secondaryText,
    fontSize: 12
  }
});
