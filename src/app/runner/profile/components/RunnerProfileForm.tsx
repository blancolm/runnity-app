import React from 'react';
import { View } from 'react-native';
import {
  Button,
  FormikDateInput,
  FormikImage,
  FormikRadioGroup,
  FormikTextInput
} from '@vadiun/react-native-components';
import { Field, Formik } from 'formik';
import moment from 'moment';
import { RadioButton, Text } from 'react-native-paper';
import * as Yup from 'yup';
import CustomColors from 'app/shared/helpers/CustomColors';
import { ProfileFormType } from '../../models/ProfileFormType';

const Schema = Yup.object().shape({
  name: Yup.string().required('El nombre es requerido'),
  birthdate: Yup.mixed().required('La fecha de nacimiento es requerida'),
  weight: Yup.string().required('El peso es requerido'),
  sex: Yup.string().required('El sexo es requerido'),
  photo: Yup.mixed().required('La imagen es requerida')
});

const defaultInitialValues = {
  birthdate: moment(),
  weight: '',
  sex: 'male',
  name: '',
  photo: ''
};

interface Props {
  initialValues?: ProfileFormType;
  onSubmit: (x: ProfileFormType) => Promise<void>;
}

export const RunnerProfileForm = ({
  initialValues = defaultInitialValues,
  onSubmit
}: Props) => {
  return (
    <Formik<ProfileFormType>
      initialValues={initialValues}
      validationSchema={Schema}
      onSubmit={onSubmit}
    >
      {({ submitForm, isSubmitting }) => (
        <View style={{ flex: 1 }}>
          <View style={{ alignItems: 'center' }}>
            <Field
              name="photo"
              source="camera"
              component={FormikImage}
              rounded
              iconPosition="bottomRight"
              imagePickerOptions={{
                quality: 0.7
              }}
              size={{ width: 150, height: 150 }}
            />
          </View>
          <Field
            name="name"
            label="Nombre"
            mode="flat"
            component={FormikTextInput}
          />
          <Field
            name="weight"
            label="Peso [kg.]"
            mode="flat"
            keyboardType="number-pad"
            component={FormikTextInput}
          />

          <Field
            name="birthdate"
            label="Fecha nacimiento"
            mode="flat"
            component={FormikDateInput}
          />
          <Text style={{ color: CustomColors.secondaryText }}>Sexo</Text>
          <Field
            name="sex"
            label="Género"
            mode="flat"
            component={FormikRadioGroup}
          >
            <RadioButton.Item label="Masculino" value="male" />
            <RadioButton.Item label="Femenino" value="female" />
            <RadioButton.Item label="Otro" value="other" />
          </Field>

          <View style={{ marginTop: 'auto' }}>
            <Button
              onPress={submitForm}
              mode="contained"
              style={{ padding: 5 }}
              loading={isSubmitting}
              disabled={isSubmitting}
            >
              Continuar
            </Button>
          </View>
        </View>
      )}
    </Formik>
  );
};
