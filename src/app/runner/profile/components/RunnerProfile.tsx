import { CompetitionRunningTeamModel } from 'app/competition/team/models/CompetitionRunningTeamModel';
import React from 'react';
import { TrainingModel } from 'app/training/models/TrainingModel';
import { CompetitionModel } from 'app/competition/main/models/Competition';
import { StyleSheet, View } from 'react-native';
import RunnerModel from '../../models/Runner';
import { RunnerProfileHeader } from './RunnerProfileHeader';
import RunnerProfileTrainingChart from './RunnerProfileTrainingChart';
import { RunnerProfileResults } from './RunnerProfileResults';
import { RunnerProfileCompetitions } from './RunnerProfileCompetitions';

interface Props {
  runner: RunnerModel;
  trainings: TrainingModel[];
  competitions: (CompetitionModel | CompetitionRunningTeamModel)[];
  onShowTrainingDetails: () => void;
  onShowAllCompetitions: () => void;
  onShowCompetition: (
    comp: CompetitionModel | CompetitionRunningTeamModel
  ) => void;
  onLogout: () => void;
  onEdit: () => void;
}

export const RunnerProfile = (props: Props) => {
  const {
    runner,
    onShowTrainingDetails,
    onShowAllCompetitions,
    onShowCompetition
  } = props;
  return (
    <View style={styles.container}>
      <RunnerProfileHeader
        runner={runner}
        onLogout={props.onLogout}
        onEdit={props.onEdit}
      />
      <RunnerProfileResults runner={runner} />
      <RunnerProfileTrainingChart
        trainings={props.trainings}
        onShowTrainingDetails={onShowTrainingDetails}
      />
      <RunnerProfileCompetitions
        onShowAllCompetitions={onShowAllCompetitions}
        onShowCompetition={onShowCompetition}
        competitions={props.competitions}
        runner={props.runner}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
