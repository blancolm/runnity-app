import { Button } from '@vadiun/react-native-components';
import CustomColors from 'app/shared/helpers/CustomColors';
import TrainingChart from 'app/training/components/TrainingChart';
import { TrainingModel } from 'app/training/models/TrainingModel';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'react-native-paper';

interface Props {
  onShowTrainingDetails: () => void;
  trainings: TrainingModel[];
}

const RunnerProfileTrainingChart = ({
  onShowTrainingDetails,
  trainings
}: Props) => (
  <View style={styles.container}>
    <View style={styles.row}>
      <Text style={styles.title}>Mis entrenamientos</Text>
      <Button onPress={onShowTrainingDetails}>Ver todos</Button>
    </View>
    <TrainingChart trainings={trainings} />
  </View>
);

export default RunnerProfileTrainingChart;

const styles = StyleSheet.create({
  container: {
    marginVertical: 20,
    paddingTop: 10,
    backgroundColor: CustomColors.dark600
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10
  },
  title: { fontSize: 16, marginLeft: 20 }
});
