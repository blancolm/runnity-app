// @ts-ignore
import { PRODUCTION } from 'react-native-dotenv';

const LOCAL_IP = '192.168.0.5';
const develop = {
  backEnd: `https://runnity-forge2.fit-club.com.ar/api/`,
  localIp: LOCAL_IP,
  production: false
};

const production = {
  backEnd: `https://runnity-forge2.fit-club.com.ar/api/`,
  localIp: LOCAL_IP,
  production: true
};

const environment = PRODUCTION === 'true' ? production : develop;

export default environment;
